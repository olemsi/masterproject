/**
 * Main Files
 * - stdio_usb_example.c: the example application.
 * - conf_board.h: board configuration
 * - conf_clock.h: board configuration
 * - stdio_usb.h: Common USB CDC Standard I/O Implementation
 * - read.c : System implementation function used by standard library
 * - write.c : System implementation function used by standard library
 *
 */
#include <asf.h>
#include <string.h>
#include <math.h>

#define ENCODER_TO_MM 0.785

bool enc_rightHigh, enc_rightLow, enc_leftHigh, enc_leftLow;
int rightValue = 0;
int leftValue = 0;

static void CB_StopMotors(void)
{
	// User code to execute when the overflow occurs here
	LED_Toggle(LED0);
	TCC0.CCA = 0;
	TCC0.CCB = 0;
	rightValue = 0;
	leftValue = 0;
}

void RobotRotator(int in_dir, int in_speed) {
	int Speed = in_speed;
	
	if(in_dir > 0) { // Clockwize rotation
		PORTC.OUT |= (1<<3); // Left in forward dir
		PORTC.OUT &= ~(1<<2); // Right in reverse dir
	} else if (in_dir < 0) { // Counter Clockwize rotation
		PORTC.OUT &= ~(1<<3);
		PORTC.OUT |= (1<<2);
	}
	
	TCC0.CCA = abs(Speed);
	TCC0.CCB = abs(Speed);
}

void RobotDriver(int in_angle, int in_speed) {
	int Angle = in_angle; 
	int Speed = in_speed; 
	uint16_t v_Speed = 0;
	uint16_t h_Speed = 0;
	
	if(abs(Angle) <= 90) { // Forward
		PORTC.OUT |= (1<<2);
		PORTC.OUT |= (1<<3);
		//printf("Forward direction |");
		if(Angle < 0) {
			h_Speed = Speed;
			v_Speed = Speed + (Speed / 90) * Angle;
		}
		else if(Angle > 0) {
			v_Speed = Speed;
			h_Speed = Speed - (Speed / 90) * Angle;
		} else {
			v_Speed = Speed;
			h_Speed = Speed;
		}
	}
	long b;
	if (abs(Angle) > 110) { // Backwards
		PORTC.OUT &= ~(1<<3);
		PORTC.OUT &= ~(1<<2);
		//printf("Backwards direction |");
		if(Angle < -110 && abs(Angle) < 180) {
			h_Speed = Speed;
			v_Speed = (Speed / 90) * (abs(Angle) - 90);
			} else if (Angle > 110 && Angle < 180) {
			v_Speed = Speed;
			h_Speed = (Speed / 90) * (Angle - 90);
			} else if (abs(Angle) == 180) {
			v_Speed = Speed;
			h_Speed = Speed;
		}
	}
	
	if (abs(Angle) > 90 && abs(Angle) <= 110) { // Deadband
		v_Speed = 0;
		h_Speed = 0;
	}
	
	
	TCC0.CCA = h_Speed;
	TCC0.CCB = v_Speed;
}

void distanceDriver(float targetMM, bool forward) {
	
	int speed = 310;
	int angleAdjust = 0;
	float currentRight = (float)rightValue * ENCODER_TO_MM;
	float currentLeft = (float)leftValue * ENCODER_TO_MM;
	int i_currentRight = (int) currentRight;
	int i_currentLeft = (int) currentLeft;
	
	angleAdjust = i_currentRight - i_currentLeft;
	
	if((targetMM > currentRight || targetMM > currentLeft) && forward) {
		printf("MOVING: %i | %i | %i \n\r", (int)targetMM, i_currentRight, i_currentLeft);
		RobotDriver(angleAdjust, speed);
	}
	else if((targetMM < currentRight || targetMM < currentLeft) && !forward) {
		printf("MOVING\n\r");
		RobotDriver(180 + angleAdjust, speed);
	} else {
		TCC0.CCA = 0;
		TCC0.CCB = 0;
		printf("DONE");
	}
}

int main (void)
{

	sysclk_init();
	board_init();
	
	PMIC_CTRL = 0x07;
	pmic_init();
	
	tc_enable(&TCF0);
	tc_set_overflow_interrupt_callback(&TCF0, CB_StopMotors);
	tc_set_wgm(&TCF0, TC_WG_NORMAL);
	tc_write_period(&TCF0, 11719); // 1 sec = 23438 counts by using 2 as system prescaler and 1024 as timer prescaler (down below) (11719 = 0.5 sec)
	tc_set_overflow_interrupt_level(&TCF0, TC_INT_LVL_LO);
	// Enable interrupts
	cpu_irq_enable();
	// Running at 48MHz with prescaler = 2 (see config/conf_clock.h). 1 sec = 24 000 000 counts. Since 65536 = max value of 16-bit timer we use scaler DIV1024: 24 000 000 / 1024 ~= 23438 counts per second.
	tc_write_clock_source(&TCF0, TC_CLKSEL_DIV1024_gc);
	

	// Initialize interrupt vector table support.
	irq_initialize_vectors();

	
	/* Call a local utility routine to initialize C-Library Standard I/O over
	 * a USB CDC protocol. Tunable parameters in a conf_usb.h file must be
	 * supplied to configure the USB device correctly.
	 */
	stdio_usb_init();

	//set port C 0..3 to output, PC0 and PC1 for pwm and PC2 and PC3 for directional
	PORTC.DIR = 0b00001111;
	PORTB.DIR &= 0b11110000; // Setting Pin0 - Pin 3 to inputs
	
	sei(); // Enabling interrupts
	
	// Configuring interrupts
	PORTB.INT0MASK = 0b00000011;
	PORTB.INT1MASK = 0b00001100;
	
	 // Enable low level interrupts
	PORTB_INTCTRL = 0b00000101;
	
	enc_rightHigh = (PORTB.IN & PIN0_bm); // Initial pin-status ####
	enc_rightLow = (PORTB.IN & PIN1_bm);
	enc_leftHigh = (PORTB.IN & PIN2_bm);
	enc_leftLow = (PORTB.IN & PIN3_bm);

	TCC0.CTRLA |=(PIN2_bm); //enables compare, with 8 as a prescaler
	TCC0.CTRLB |=(PIN5_bm)|(PIN4_bm)|(PIN1_bm)|(PIN0_bm);
	
	TCC0.PER = 2000; //TCC0.PER in CCA and CCB is 100% pwm, 0 is 0% pwm
	TCC0.CCB = 0;
	TCC0.CCA = 0;
	
	//	[angle & speed]  +-180 deg. Speed == Value from 0 to 2000.
	
	char received[20], angle[20], speed[20];
	int i;
	char *token;
	bool first = true;
	
	while (true) {
		int result = scanf("%19s",&received); // get input characters
		
		tc_restart(&TCF0);
		LED_On(LED0);
		
		token = strtok(received, "|\n");
		if(strcmp(token, "D") == 0){
			printf("Direct\n\r");
			rightValue = 0;
			leftValue = 0;
			token = strtok(NULL, "|\n");
			i = 0;
			int angle, speed, rotation = 0;
			while(token != NULL) {
				if(i == 0) angle = atoi(token);
				if(i == 1) speed = atoi(token);
				if(i == 2) rotation = atoi(token);
				token = strtok(NULL, "|\n");
				i++;
			}
			if(rotation == 0) {
				RobotDriver(angle, speed);
				} else {
				RobotRotator(rotation, speed);
			}
		} else if(strcmp(token, "T") == 0) {
			token = strtok(NULL, "|\n");
			float distance = atof(token);
			token = strtok(NULL, "|\n");
			int direction = atoi(token);
			distanceDriver(distance, direction);
		}
	}
}

// Interrupt Service Routines
ISR(PORTB_INT0_vect){
	bool rightHigh_new = (PORTB.IN & PIN0_bm); // 400 Pulses per revolution. Diameter = 100mm -> 1 rev = (2 * pi * 50) mm = (100 * pi) mm = 314 mm
	bool rightLow_new = (PORTB.IN & PIN1_bm);
	if (enc_rightHigh && enc_rightLow ){// 1 1
		if (rightHigh_new && !rightLow_new){// 1 1 -> 1 0
			rightValue--;
		}
		else if(!rightHigh_new && rightLow_new){// 1 1 -> 0 1
			rightValue++;
		}
	}
	else if(enc_rightHigh && !enc_rightLow){ // 1 0
		if (!rightHigh_new && !rightLow_new){// 1 0 -> 0 0
			rightValue--;
		}
		else if(rightHigh_new && rightLow_new){// 1 0 -> 1 1
			rightValue++;
		}
	}
	else if(!enc_rightHigh && enc_rightLow){// 0 1
		if (rightHigh_new && rightLow_new){// 0 1 -> 1 1
			rightValue--;
		}
		else if(!rightHigh_new && !rightLow_new){// 0 1 -> 0 0
			rightValue++;
		}
	}
	else{//hHig oh hLow both false // 0 0
		if (!rightHigh_new && rightLow_new){// 0 0 -> 0 1
			rightValue--;
		}
		else if(rightHigh_new && !rightLow_new){// 0 0 -> 1 0
			rightValue++;
		}
	}
	
	enc_rightLow = rightHigh_new;
	enc_rightLow = rightLow_new;
}

ISR(PORTB_INT1_vect){
	bool leftHigh_new = (PORTB.IN & PIN2_bm);
	bool leftLow_new = (PORTB.IN & PIN3_bm);
	if (enc_leftHigh && enc_leftLow ){// 1 1
		if (leftHigh_new && !leftLow_new){// 1 1 -> 1 0
			leftValue++;
		}
		else if(!leftHigh_new && leftLow_new){// 1 1 -> 0 1
			leftValue--;
		}
	}
	else if(enc_leftHigh && !enc_leftLow){ // 1 0
		if (!leftHigh_new && !leftLow_new){// 1 0 -> 0 0
			leftValue++;
		}
		else if(leftHigh_new && leftLow_new){// 1 0 -> 1 1
			leftValue--;
		}
	}
	else if(!enc_leftHigh && enc_leftLow){// 0 1
		if (leftHigh_new && leftLow_new){// 0 1 -> 1 1
			leftValue++;
		}
		else if(!leftHigh_new && !leftLow_new){// 0 1 -> 0 0
			leftValue--;
		}
	}
	else{//hHig oh hLow both false // 0 0
		if (!leftHigh_new && leftLow_new){// 0 0 -> 0 1
			leftValue++;
		}
		else if(leftHigh_new && !leftLow_new){// 0 0 -> 1 0
			leftValue--;
		}
	}
	
	enc_leftLow = leftHigh_new;
	enc_leftLow = leftLow_new;
}
