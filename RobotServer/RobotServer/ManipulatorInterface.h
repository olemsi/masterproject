#include <mutex>
#include <condition_variable>
#include <thread>

class ManipulatorInterface {
public:
	
	bool Init(bool initHome = false);

	bool SetPosition(RemoteSetPoints rSP, char ch_Action);

	bool StopManual();

	bool ReadAnalog(unsigned char &v, int ioNum);

	bool Shutdown();

private:
	thread manipulatorThread;
	void GotoPoint();
	
	bool running = true;
};

void cb_PositionError(int * test);
void cb_InitEnd(ConfigData *pTheConfigData);       // see Initialization function in USBC.H file
void cb_ErrorMessage(ErrorInfo *pTheErrorInfo);    // see Initialization function in USBC.H file
void cb_AxisControl(UINT * uiBitmap);
void cb_MotionStart(UCHAR *ucStartCode);
void cb_MotionEnd(UCHAR *ucEndCode);
void cb_HomingNotif(UCHAR * ucNotifCode);
void cb_XYZValues(RobotData XYZPos);
void cb_WatchAnalogInp(unsigned char *tst);