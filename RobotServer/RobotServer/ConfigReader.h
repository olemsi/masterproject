#include <map>
#include <string>
#include <fstream>
#include <iostream>

class ConfigReader
{
public:
	
	void Clear();

	
	void WriteDefault();

	
	bool Load(const string& File);

	
	bool Contains(const string& key) const;

	
	bool Get(const string& key, string& value) const;
	bool Get(const string& key, int&    value) const;
	bool Get(const string& key, unsigned short& value) const;
	bool Get(const string& key, long&   value) const;
	bool Get(const string& key, double& value) const;
	bool Get(const string& key, bool&   value) const;

private:
	
	map<string, string> data;
	static string Trim(const string& str);
};