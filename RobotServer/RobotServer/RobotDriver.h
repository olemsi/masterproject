#ifndef ROBOTDRIVER_H // Header guard
#define ROBOTDRIVER_H

#include "SerialHandler.h"

class RobotDriver {
public:

	bool init(const char *COM, DWORD Baud, BYTE Databits, BYTE Stopbits, BYTE Parity);
	void moveRobot(float angle, float speed, float rotation);
	void moveToTargetRobot(float distance, bool direction);

	string getResponse();

	void Shutdown();

private:
	SerialHandler SH;
};

#endif