#include "stdafx.h"
#include "ManipulatorInterface.h"
#include <string>

bool InitSuccess = false; bool isHome = false;
bool axis_motionDone = false, gripper_motionDone = false;
bool newSP = false;
std::condition_variable cv_Init, cv_axis_motionDone, cv_gripper_motionDone, cv_NewSP;
std::mutex mtx_Init, mtx_axisMotion, mtx_gripperMotion, mtx_SPLock;

bool ManipulatorInterface::Init(bool initHome) {

	if (!Initialization(INIT_MODE_ONLINE, ER4USB_SYSTEM_TYPE, (CallBackFun)&cb_InitEnd, (CallBackFun)&cb_ErrorMessage)) {
		return false;
	}
	
	// Waiting for callback (max 5 seconds), to verify successful initialization
	std::unique_lock<std::mutex> lk(mtx_Init);
	while (!InitSuccess) {
		if (cv_Init.wait_for(lk, std::chrono::seconds(5)) == std::cv_status::timeout) return false;
	}

	if (!Control('&', TRUE)) return false;
	if (!DefineVector('A', "DIRECT", 20)) {
		return false;
	}
	if (!DefineVector('A', "RECORD", 200)) {
		return false;
	}
	if (!DefineVector('A', "HOMEPOS", 10)) {
		return false;
	}
	WatchMotion((CallBackFun)cb_MotionEnd, (CallBackFun)cb_MotionStart);
	ShowPositErr((CallBackFun)&cb_PositionError);
	WatchAnalogInp((CallBackFun)cb_WatchAnalogInp);

	if (initHome) {
		if (!Home('A', (CallBackFun)&cb_HomingNotif))
		{
			return false;
		}
		Here("HOMEPOS", 1, ABS_JOINT);
		isHome = true;
	}
	
	manipulatorThread = thread(&ManipulatorInterface::GotoPoint, this);
	return true;

}


char * movementType;
short gripperSP[201] {0}; // [0] = for direct usage. [1 - 200] = for recorded points
int pointNum = 0;
bool ManipulatorInterface::SetPosition(RemoteSetPoints rSP, char ch_Action) {
	
	long coordArray[8] {0};
	coordArray[0] = rSP[ROBOT_BASE] * -1000.0f;
	coordArray[1] = rSP[ROBOT_SHOULDER] * 1000.0f;
	coordArray[2] = rSP[ROBOT_ELBOW] * 1000.0f;
	coordArray[3] = rSP[ROBOT_PITCH] * -1000.0f;
	coordArray[4] = rSP[ROBOT_ROLL] * 1000.0f;
	short jawPos = (short)rSP[ROBOT_JAW];
	std::unique_lock<std::mutex> lkSP(mtx_SPLock);
	switch (ch_Action)
	{
	case 'A':
		pointNum++;
		if (pointNum > 200) return false;
		gripperSP[pointNum] = jawPos;
		if (!SetJoints("RECORD", pointNum, &coordArray[0], 5, ABS_JOINT)) return false;
		break;
	case 'R':
		if (pointNum > 0) {
			if (!DeletePoint("RECORD", pointNum)) return false;
			gripperSP[pointNum] = 0;
			pointNum--;
		}
		break;
	case 'D':
		if (!SetJoints("DIRECT", 1, &coordArray[0], 5, ABS_JOINT)) return false;
		movementType = "DIRECT";
		if (jawPos > 70) jawPos = 70;
		gripperSP[0] = jawPos;
		newSP = true;
		cv_NewSP.notify_one();
		break;
	case 'E':
		for (int i = 1; i <= pointNum; i++)
		{
			if (!DeletePoint("RECORD", i)) return false;
			gripperSP[i] = 0;
		}
		pointNum = 0;
		break;
	case 'X':
		if (IsPointExist("RECORD", 1)) {
			movementType = "RECORD";
			newSP = true;
			cv_NewSP.notify_one();
		}
		break;
	case 'C':
		Stop('&');
	case 'H':
		if (IsPointExist("HOMEPOS", 1) && !isHome) {
			movementType = "HOME";
			newSP = true;
			cv_NewSP.notify_one();
		}
	default:
		break;
	}

	return true;
}


void ManipulatorInterface::GotoPoint() {
	short old_Jaw;
	short old_Jaw_Perc;
	GetJaw(&old_Jaw_Perc, &old_Jaw);

	while (running) {
		bool directMode = false; bool homePosition = false;
		int numPoints = 0;
		printf("waiting\n");
		std::unique_lock<std::mutex> lkSP(mtx_SPLock);
		cv_NewSP.wait(lkSP, [&]() {return newSP || !running; }); // wait() unlocks the mutex. Mutex is locked again when not waiting.
		newSP = false;
		if (movementType == "DIRECT") {
			directMode = true;
		}
		else if (movementType == "HOME") {
			homePosition = true;
		}
		else {
			numPoints = pointNum;
		}
		lkSP.unlock();
		
		
		if (directMode) {
			if (gripperSP[0] > (old_Jaw + 1) || gripperSP[0] < (old_Jaw - 1)) {
				std::unique_lock<std::mutex> lkGrip(mtx_gripperMotion);
				bool jawRun = JawMetric(gripperSP[0]);
				cv_gripper_motionDone.wait(lkGrip, [&](){return gripper_motionDone || !running || !jawRun; });
				gripper_motionDone = false;
				old_Jaw = gripperSP[0];
			}
			std::unique_lock<std::mutex> lkAxis(mtx_axisMotion);
			if (!MoveJoint("DIRECT", 1)) throw std::exception("Movement Error");
			isHome = false;
			cv_axis_motionDone.wait(lkAxis, [&](){return axis_motionDone || !running; });
			axis_motionDone = false;
		}
		else if (homePosition) {
			std::unique_lock<std::mutex> lkAxis(mtx_axisMotion);
			if (!MoveJoint("HOMEPOS", 1)) throw std::exception("Movement Error");
			cv_axis_motionDone.wait(lkAxis, [&](){return axis_motionDone || !running; });
			axis_motionDone = false;
			isHome = true;
		}
		else {
			for (int i = 1; i <= numPoints; i++)
			{
				if (gripperSP[i] != old_Jaw) {
					std::unique_lock<std::mutex> lkGrip(mtx_gripperMotion);
					bool jawRun = JawMetric(gripperSP[i]);
					cv_gripper_motionDone.wait(lkGrip, [&](){return gripper_motionDone || !running || !jawRun; });
					gripper_motionDone = false;
					old_Jaw = gripperSP[i];
				}

				std::unique_lock<std::mutex> lkAxis(mtx_axisMotion);
				if (!MoveJoint("RECORD", i)) throw std::exception("Movement Error");
				isHome = false;
				cv_axis_motionDone.wait(lkAxis, [&](){return axis_motionDone || !running; });
				axis_motionDone = false;
			}
		}
	}
}

bool ManipulatorInterface::Shutdown() {
	
	// Stop any movement
	Stop('&');

	// Disable Control
	Control('&', FALSE);
	
	// Close Connection
	if (IsOnLineOk()) CloseUSBC();
	running = false;
	cv_axis_motionDone.notify_all();
	cv_NewSP.notify_all();
	if(manipulatorThread.joinable()) manipulatorThread.join();
	return true;
}

bool ManipulatorInterface::ReadAnalog(unsigned char &v, int ioNum) {
		
	if (!GetAnalogInput(ioNum, &v)) return false;
	
	return true;
}

// Callbacks
void cb_WatchAnalogInp(unsigned char *tst) {
	return;
}

void cb_PositionError(int * test) {

	INT *sdf = test;
	//printf("Position error: %i, %i, %i, %i, %i\n", test[0], test[1], test[2], test[3], test[4], test[5]);
}


void cb_InitEnd(ConfigData *pTheConfigData)
{
	// Is called when initialization is finished and successful
	{
		std::lock_guard<std::mutex> lk(mtx_Init);
		InitSuccess = true;
	}
	cv_Init.notify_one();

}


void cb_AxisControl(UINT * uiBitmap)
{
	UINT con = *uiBitmap;

	if (con != 255) {
		// Control not on for all axis
	}
	else {
		// Control on for all axis
	}

}

void cb_MotionStart(UCHAR *ucStartCode) {
	UCHAR ucCode = *ucStartCode;
	printf("Motion Start: %c\n", ucCode);

}


void cb_MotionEnd(UCHAR *ucEndCode)
{
	UCHAR ucCode = *ucEndCode;
	printf("Motion Done: %c\n", ucCode);
	if (ucCode == 'A') {
		axis_motionDone = true;
		cv_axis_motionDone.notify_one();
	}
	else if (ucCode == 'G'){
		gripper_motionDone = true;
		cv_gripper_motionDone.notify_one();
	}
}

void cb_HomingNotif(UCHAR * ucNotifCode) {

	UCHAR ucCode = *ucNotifCode;

	if (ucCode == 0xff) {
		printf("Homing started\n");
	}
	else if (ucCode == 0x40) {
		// Homing complete
		printf("Homing Complete\n");

	}
	else {
		// 1..8: Axis 1..8 is being homed
		printf("Homing Axis: %i\n", ucCode);
	}


}

void cb_XYZValues(RobotData XYZPos)
{
	long x = XYZPos[0];
	long y = XYZPos[1];
	long z = XYZPos[2];
	long pitch = XYZPos[3];
	long roll = XYZPos[4];
	//... do something with the values
}

void cb_ErrorMessage(ErrorInfo *pTheErrorInfo)
{
	string sError = "";

	switch (pTheErrorInfo->lNumber)
	{
	case ERR_EMERG_ON:
		printf("WM_EMERG_ON");
		break;

	case ERR_EMERG_OFF:
		printf("EMERG_OFF");
		break;

	case ERR_HT:
		sError = to_string((pTheErrorInfo->lNumber)) + "Time for homing elapsed";
		break;

	case ERR_HOME_ABORTED:
		sError = to_string((pTheErrorInfo->lNumber)) + "Homing aborted by user";
		break;

	case ERR_INVPAR:
		sError = to_string((pTheErrorInfo->lNumber)) + "Invalid parameter in file";
		break;

	case ERR_NOT_RESPOND:
		sError = to_string((pTheErrorInfo->lNumber)) + "Controller is not responding";
		break;

	case ERR_TRCK:
		printf("Position error during the motion is too great or impact has occurred.\n");

		break;

	case ERR_COFF:
		sError = to_string((pTheErrorInfo->lNumber)) + "Control disabled";
		break;

	case ERR_ROBOT_HM_ND:
		sError = to_string((pTheErrorInfo->lNumber)) + "Robot has not been homed";
		break;

	case KIN_ALPHA:
		sError = to_string((pTheErrorInfo->lNumber)) + "Position is not in the Cartesian workspace.\n( Kinematics: alpha error )";
		break;

	case KIN_BETA:
		sError = to_string((pTheErrorInfo->lNumber)) + "Position is not in the Cartesian workspace.\n( Kinematics: beta error )";
		break;

	case ERR_XYZ_INV:
		sError = to_string((pTheErrorInfo->lNumber)) + "Position is not in the Cartesian workspace";
		break;

	case ERR_JNT_INV:
		sError = to_string((pTheErrorInfo->lNumber)) + "Position is not in the Cartesian workspace";
		break;

	case  ERR_ENC_INV:
		sError = to_string((pTheErrorInfo->lNumber)) + "Position is not in the encoder workspace";
		break;

	case  ERR_MOVP:
		sError = to_string((pTheErrorInfo->lNumber)) + "Motion in progress";
		break;

	case  ERR_SPEED:
		sError = to_string((pTheErrorInfo->lNumber)) + "Excessive speed required";
		break;

	case  ERR_ACCL:
		sError = to_string((pTheErrorInfo->lNumber)) + "Excessive acceleration required";
		break;

	case  ERR_MDUR:
		sError = to_string((pTheErrorInfo->lNumber)) + "Invalid movement duration";
		break;

	case  ERR_TIME_PAR:
		sError = to_string((pTheErrorInfo->lNumber)) + "Invalid time duration for motion";
		break;

	case  ERR_VELO_PAR:
		sError = to_string((pTheErrorInfo->lNumber)) + "Invalid velocity";
		break;

	case  ERR_A_PAR:
		sError = to_string((pTheErrorInfo->lNumber)) + "Invalid acceleration parameter";
		break;

	case  ERR_AA_PAR:
		sError = to_string((pTheErrorInfo->lNumber)) + "Invalid parameter for increase of acceleration";
		break;

	case  ERR_SKIN:
		sError = to_string((pTheErrorInfo->lNumber)) + "Cannot execute movement along a specified path";
		break;

	case  ERR_GRP_INV:
		sError = to_string((pTheErrorInfo->lNumber)) + "Invalid group";
		break;

	case  KIN_CFG:
		sError = to_string((pTheErrorInfo->lNumber)) + "Kinematics: invalid configuration parameters";
		break;

	case  ERR_B_INV:
		sError = to_string((pTheErrorInfo->lNumber)) + "Position is not in the peripheral workspace";
		break;

	case  ERR_CCHANGE:
		sError = to_string((pTheErrorInfo->lNumber)) + "Robot cannot execute movement due to angle of robot arm at axis 3";
		break;

	case  TP_TEACH:
		printf("TEACH");
		break;

	case  TP_AUTO:
		printf("AUTO");
		break;

	case  ERR_AXTYPE:
		sError = to_string((pTheErrorInfo->lNumber)) + "Invalid axis type. Axis No: " + to_string(pTheErrorInfo->cAxis);
		break;

	default:
		if (pTheErrorInfo->cAxis == 0)
		{
			sError = to_string((pTheErrorInfo->lNumber)) + "Default error; ";
		}
		else
		{
			sError = to_string(pTheErrorInfo->lNumber) + "Axis No: " + pTheErrorInfo->cAxis + "   " + (char *)pTheErrorInfo->cOptional;
		}
		break;
	};
	
	printf("ERROR HAS OCCURRED: %s\n", sError.c_str());
}