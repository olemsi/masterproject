#include "stdafx.h"
#include "PanTiltUnit.h"


using namespace std;

float old_PanPos = 0;
float old_TiltPos = 0;


bool PanTiltUnit::init(const char *COM, DWORD Baud, BYTE Databits, BYTE Stopbits, BYTE Parity) {
	
	bool result = SH.init(COM, Baud, Databits, Stopbits, Parity);

	SH.writeData("pp0\n\rtp0\n\r");

	return result;

}

void PanTiltUnit::setTargetDirection(float pan, float tilt) {

	string msg;

	if (pan > (old_PanPos + 0.01f) || pan < (old_PanPos - 0.01f) || tilt > (old_TiltPos + 0.01f) || tilt < (old_TiltPos - 0.01f)) { // 0.01 rad ~ 0.5�
		
		// Converting angles in radians to position steps
		int targetPan = (int)((pan * (180.0f / 3.14159265f)) / 0.0128582f); 
		int targetTilt = (int)((tilt * (180.0f / 3.14159265f)) / 0.0128582f);
		
		string msg = "pp" + to_string(targetPan) + "\n\r" + "tp" + to_string(targetTilt) + "\n\r";
		SH.writeData(msg);
		old_PanPos = pan;
		old_TiltPos = tilt;
	}
}

string PanTiltUnit::getResponse() {

	return  SH.readData();

}

void PanTiltUnit::Shutdown() {

	SH.Shutdown();
}