#ifndef SERIALHANDLER_H // Header guard
#define SERIALHANDLER_H

#include <stdio.h>
#include <thread>
#include <string>
#include <iostream>
#include <mutex>
#include <condition_variable>

class SerialHandler {
public:

	SerialHandler() {};

	bool init(const char *COM, DWORD Baud, BYTE Databits, BYTE Stopbits, BYTE Parity);
	
	void writeData(string value);
	string readData();

	void Shutdown();

private:
	// Variables
	thread comThread;
	mutex mtx_Read;
	mutex mtx_Write;
	condition_variable cv_writeMsgAvail;
	
	HANDLE hComm;

	bool running;
	bool newData = false;
	bool dataAvail = false;
	string writeMsg;
	string readMsg;

	// Methods
	void DevicePollerThread(HANDLE hComm);
};

#endif