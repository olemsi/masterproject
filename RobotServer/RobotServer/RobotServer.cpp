// RobotArm.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include "RobotServer.h"
#include "ManipulatorInterface.h"
#include "PanTiltUnit.h"
#include "RobotDriver.h"
#include "ConfigReader.h"
#include "Kinect_Interface.h"

#pragma comment(lib, "Ws2_32.lib")

#ifdef _DEBUG
#define new DEBUG_NEW
#endif


// The one and only application object


CWinApp theApp;
thread senderThread;
thread recordModeThread;
// Config Parameters


bool transmitting = true;
bool transmitKinect = false;
bool transmitKinectColor = false;
bool recordModeActive = false;
int iResult = 0;

PanTiltUnit PTU;
RobotDriver RDriver;
ManipulatorInterface RobotArm;
ConfigReader cfg;
KinectController Kinect;

bool initWinsock() {

	WSADATA wsaData;
	
	// Initialize Winsock
	iResult = WSAStartup(MAKEWORD(2, 2), &wsaData);
	if (iResult != NO_ERROR) {
		wprintf(L"WSAStartup failed with error %d\n", iResult);
		return false;
	}

	return true;
}

bool createSocket(SOCKET &sock) {
	sockaddr_in RecvAddr;

	// Create a socket
	sock = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP);
	if (sock == INVALID_SOCKET) {
		wprintf(L"socket failed with error %d\n", WSAGetLastError());
		return false;
	}
	
	return true;
}

bool createSocket(SOCKET &sock, unsigned short Port) {
	sockaddr_in RecvAddr;

	// Create a socket
	sock = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP);
	if (sock == INVALID_SOCKET) {
		wprintf(L"socket failed with error %d\n", WSAGetLastError());
		return false;
	}
	
	// Bind the socket to any address and the specified port to receive datagrams.
	RecvAddr.sin_family = AF_INET;
	RecvAddr.sin_port = htons(Port);
	RecvAddr.sin_addr.s_addr = htonl(INADDR_ANY);

	iResult = ::bind(sock, (SOCKADDR *)& RecvAddr, sizeof(RecvAddr));
	if (iResult != 0) {
		wprintf(L"bind failed with error %d\n", WSAGetLastError());
		return false;
	}
	
	return true;
}

// Threaded function for maintaining a steady flow of setpoints during straight driving,
// avoiding the watchdog to trigger
void recordModeLoop(RemoteSetPoints received) {
	if (!recordModeActive) {
		string response = "init";
		recordModeActive = true;
		RobotArm.SetPosition(received, 'H');
		while (response != "DONE" && recordModeActive) { // Forcing a movement of 0.7 meter forward before activating robot arm.
			RDriver.moveToTargetRobot(700.0f, 1.0f);
			Sleep(10);
			response = RDriver.getResponse();
			printf("MCU: %s\n", response.c_str());
		}
		if (recordModeActive) RobotArm.SetPosition(received, 'X');
		else RobotArm.SetPosition(received, 'C');
	}
	recordModeActive = false;
}

// The threaded function for sending data to the client
void senderLoop(SOCKET senderSocket, sockaddr_in SenderAddr, unsigned short port) {

	sockaddr_in RecipientAddr = SenderAddr;
	
	RecipientAddr.sin_port = htons(port);
	int iSendResult, iRecvResult;
	unsigned char batteryLevel;
	int SenderAddrSize = sizeof(RecipientAddr);

	const size_t BUFSIZE = 4096;
	
	DWORD tv = 10000;
	if (setsockopt(senderSocket, SOL_SOCKET, SO_RCVTIMEO, (char *)&tv, sizeof(tv)) < 0) {
		perror("Error");
	}

	while (transmitting) {
		char buffer[BUFSIZE] { 0 };

		if (transmitKinect){
			Kinect.getImageData();

			USHORT *us_buf = (USHORT*)buffer;
			buffer[0] = 'K';
			int i = 0;
			while (i < KINECT_HEIGHT && transmitting)
			{
				buffer[1] = i / 100;
				buffer[2] = (i % 100) / 10;
				buffer[3] = (i % 100) % 10;

				for (int j = 0; j < KINECT_WIDTH; ++j) { // Storing current row data in buffer
					us_buf[j * 3 + 2] = Kinect.vb[i][j]; // Depth value
					buffer[j * 6 + 6] = Kinect.cb[i][j * 3]; // R
					buffer[j * 6 + 7] = Kinect.cb[i][j * 3 + 1]; // G
					buffer[j * 6 + 8] = Kinect.cb[i][j * 3 + 2]; // B
				}

				iSendResult = sendto(senderSocket, buffer, sizeof(buffer), 0, (SOCKADDR *)&RecipientAddr, SenderAddrSize);
				if (iSendResult == SOCKET_ERROR) {
					printf("send failed with error: %d\n", WSAGetLastError());
					closesocket(senderSocket);
					WSACleanup();
					return;
				}
				
				i++;
			}
		}
		else if (transmitKinectColor) {
			Kinect.getImageData();

			buffer[0] = 'C';
			int i = 0;
			while (i < KINECT_HEIGHT && transmitting)
			{
				buffer[1] = i / 100;
				buffer[2] = (i % 100) / 10;
				buffer[3] = (i % 100) % 10;
				// Sending two rows per buffer to save bandwidth / overhead
				for (int j = 0; j < KINECT_WIDTH; ++j) { // First row
					buffer[j * 3 + 4] = Kinect.cb[i][j * 3]; // R
					buffer[j * 3 + 5] = Kinect.cb[i][j * 3 + 1]; // G
					buffer[j * 3 + 6] = Kinect.cb[i][j * 3 + 2]; // B
				}
				for (int j = 0; j < KINECT_WIDTH; ++j) { // Second row
					buffer[1924 + j * 3] = Kinect.cb[i+1][j * 3];
					buffer[1924 + j * 3 + 1] = Kinect.cb[i + 1][j * 3 + 1];
					buffer[1924 + j * 3 + 2] = Kinect.cb[i + 1][j * 3 + 2];
				}

				iSendResult = sendto(senderSocket, buffer, sizeof(buffer), 0, (SOCKADDR *)&RecipientAddr, SenderAddrSize);
				if (iSendResult == SOCKET_ERROR) {
					printf("send failed with error: %d\n", WSAGetLastError());
					closesocket(senderSocket);
					WSACleanup();
					return;
				}

				i+=2;
			}
		}


		if (!RobotArm.ReadAnalog(batteryLevel, 0)) printf("Readvoltage failed");

		buffer[0] = 'B';
		buffer[1] = batteryLevel;
		iSendResult = sendto(senderSocket, buffer, sizeof(char) * 2, 0, (SOCKADDR *)&RecipientAddr, SenderAddrSize);
		if (iSendResult == SOCKET_ERROR) {
			printf("send failed with error: %d\n", WSAGetLastError());
			closesocket(senderSocket);
			WSACleanup();
			return;
		}
	}
}


int RecvMsgInterpret(char* charBuffer) {

	RemoteSetPoints received = { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };

	float* f_buf = (float*)charBuffer;

	//std::string activeSPs = "";
	bool activePTU = false, activeDriver = false;
	char robotCmd = charBuffer[0];
	if (charBuffer[1] == 'Y') activePTU = true;
	if (charBuffer[2] == 'Y') activeDriver = true;
	if (charBuffer[3] == 'D') {
		transmitKinect = true;
		transmitKinectColor = false;
	}
	else if (charBuffer[3] == 'C') {
		transmitKinectColor = true;
		transmitKinect = false;
	}
	else {
		transmitKinect = false;
		transmitKinectColor = false;
	}

	received[ROBOT_BASE] = f_buf[1];
	received[ROBOT_SHOULDER] = f_buf[2];
	received[ROBOT_ELBOW] = f_buf[3];
	received[ROBOT_ROLL] = f_buf[4];
	received[ROBOT_PITCH] = f_buf[5];
	received[ROBOT_JAW] = f_buf[6];
	received[PTU_P] = f_buf[7];
	received[PTU_T] = f_buf[8];
	received[DRIVER_ANGLE] = f_buf[9];
	received[DRIVER_SPEED] = f_buf[10];
	received[DRIVER_ROTATION] = f_buf[11];

	if (robotCmd != 'N') {
		if (robotCmd == 'X'){
			// Detatching thread for straight driving, allowing it to run on its own
			thread(&recordModeLoop, received).detach();
		}
		else{
			if (robotCmd == 'C') recordModeActive = false;
			RobotArm.SetPosition(received, robotCmd);
		}
	}

	if (activePTU)
	{
		PTU.setTargetDirection(received[PTU_P], received[PTU_T]);
	}

	if (activeDriver && !recordModeActive)
	{
		RDriver.moveRobot(received[DRIVER_ANGLE], received[DRIVER_SPEED], received[DRIVER_ROTATION]);
	}
	
	return 0;
}

int main()
{
	bool initError = false; string errorMsg = "";
	unsigned short ReceivePort = 2222, SendPort = 2223;
	string PTU_COM;	int PTU_BAUD;	int PTU_DATABITS;	int PTU_STOPBITS;	int PTU_PARITY;
	string MCU_COM;	int MCU_BAUD;	int MCU_DATABITS;	int MCU_STOPBITS;	int MCU_PARITY;

	// Reading contents of config file
	cfg.Load("Config.cfg");
	if (!(cfg.Get("ReceivePort", ReceivePort) && cfg.Get("SendPort", SendPort) &&
		cfg.Get("PTU_COM", PTU_COM) && cfg.Get("PTU_BAUD", PTU_BAUD) && cfg.Get("PTU_DATABITS", PTU_DATABITS) && cfg.Get("PTU_STOPBITS", PTU_STOPBITS) && cfg.Get("PTU_PARITY", PTU_PARITY) &&
		cfg.Get("MCU_COM", MCU_COM) && cfg.Get("MCU_BAUD", MCU_BAUD) &&	cfg.Get("MCU_DATABITS", MCU_DATABITS) && cfg.Get("MCU_STOPBITS", MCU_STOPBITS) && cfg.Get("MCU_PARITY", MCU_PARITY)))
	{
		printf("Missing parameter or file. Writing default");
		cfg.WriteDefault();

		return 1;
	}
	
	if (!RDriver.init(MCU_COM.c_str(), MCU_BAUD, MCU_DATABITS, MCU_STOPBITS, MCU_PARITY)) errorMsg = errorMsg + "| MCU initialization failure |";
	if (!PTU.init(PTU_COM.c_str(), PTU_BAUD, PTU_DATABITS, PTU_STOPBITS, PTU_PARITY)) errorMsg = errorMsg + "| PTU initialization failure |";
	bool home = true;
	if (!RobotArm.Init(home)) errorMsg = "| Robot Manipulator initialization failure |";
	if (!Kinect.initKinect()) errorMsg = errorMsg + "| Kinect initialization failure |";
	if (home) {

		Sleep(100);
		// Setting startup position
		RemoteSetPoints received = { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };
		received[ROBOT_BASE] = 0.0f;
		received[ROBOT_SHOULDER] = -100.0f;
		received[ROBOT_ELBOW] = 110.0f;
		received[ROBOT_PITCH] = 0.0f;
		received[ROBOT_ROLL] = 0.0f;
		received[ROBOT_JAW] = 0.0f;
		RobotArm.SetPosition(received, 'D');
	}
	SOCKET ReceiverSocket;
	SOCKET SenderSocket;
	if (!initWinsock()) errorMsg = errorMsg + "| Winsock initialization failure |";
	if (!createSocket(ReceiverSocket, ReceivePort)) errorMsg = errorMsg + "| Failed to create Receiversocket |";
	if (!createSocket(SenderSocket)) errorMsg = errorMsg + "| Failed to create Sendersocket |";
	
	if (errorMsg == "") {

		char RecvBuf[64];
		int iRecvResult;
		sockaddr_in SenderAddr;
		int SenderAddrSize = sizeof(SenderAddr);

		bool firstReceive = true;
		printf("Running\n");
		do {
			// Calling the recvfrom function to receive datagrams
			// on the bound socket.
			
			iRecvResult = recvfrom(ReceiverSocket, RecvBuf, sizeof(RecvBuf), 0, (SOCKADDR *)&SenderAddr, &SenderAddrSize);
			if (iRecvResult == 0)
				printf("Connection closing...\n");
			else  if (iRecvResult == SOCKET_ERROR){
				printf("recv failed with error: %d\n", WSAGetLastError());
				closesocket(ReceiverSocket);
				WSACleanup();
				return 1;
			}

			RecvMsgInterpret(&RecvBuf[0]);

			if (firstReceive) {
				senderThread = thread(&senderLoop, SenderSocket, SenderAddr, SendPort);
				firstReceive = false;
			}



		} while (iRecvResult > 0);
		transmitting = false;
		if (senderThread.joinable()) senderThread.join();
		// Close the socket when finished receiving datagrams
		wprintf(L"Finished receiving. Closing socket.\n");
	}
	// Clean up and exit.
	wprintf(L"Exiting.\n");
	closesocket(ReceiverSocket);
	WSACleanup();
	recordModeActive = false;
	PTU.Shutdown();
	RobotArm.Shutdown();
	RDriver.Shutdown();

	printf("Error: %s", errorMsg.c_str());
	Sleep(3000);
	return 1;
}

