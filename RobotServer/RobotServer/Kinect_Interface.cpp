#include "stdafx.h"
#include "Kinect_Interface.h"



bool KinectController::initKinect() {
	// Get a working kinect sensor
	
	vb = new USHORT*[KINECT_HEIGHT];
	for (int i = 0; i < KINECT_HEIGHT; ++i)
		vb[i] = new USHORT[(KINECT_WIDTH + 1)];

	cb = new unsigned char*[KINECT_HEIGHT];
	for (int i = 0; i < KINECT_HEIGHT; ++i)
		cb[i] = new unsigned char[(KINECT_WIDTH + 1) * 3];

	int numSensors;
	if (NuiGetSensorCount(&numSensors) < 0 || numSensors < 1) return false;
	HRESULT ht = NuiCreateSensorByIndex(0, &sensor);
	if (ht < 0) return false;

	// Initialize sensor
	sensor->NuiInitialize(NUI_INITIALIZE_FLAG_USES_DEPTH | NUI_INITIALIZE_FLAG_USES_COLOR);
	sensor->NuiImageStreamOpen(NUI_IMAGE_TYPE_DEPTH, // Depth camera
		NUI_IMAGE_RESOLUTION_640x480,                // Image resolution
		0,        // Image stream flags
		2,        // Number of frames to buffer
		NULL,     // Event handle
		&depthStream);
	sensor->NuiImageStreamOpen(NUI_IMAGE_TYPE_COLOR, // RGB camera
		NUI_IMAGE_RESOLUTION_640x480,                // Resolution
		0,      // Image stream flags
		2,      // Number of frames to buffer
		NULL,   // Event handle
		&rgbStream);
	
	return sensor;
}

void KinectController::getImageData() {

	NUI_IMAGE_FRAME depthimageFrame;
	NUI_LOCKED_RECT depthLockedRect;
	if (sensor->NuiImageStreamGetNextFrame(depthStream, 100, &depthimageFrame) < 0) return;
	INuiFrameTexture* depthtexture = depthimageFrame.pFrameTexture;

	NUI_IMAGE_FRAME rgbimageFrame;
	NUI_LOCKED_RECT rgbLockedRect;
	if (sensor->NuiImageStreamGetNextFrame(rgbStream, 100, &rgbimageFrame) < 0) return;
	INuiFrameTexture* rgbtexture = rgbimageFrame.pFrameTexture;

	depthtexture->LockRect(0, &depthLockedRect, NULL, 0);
	rgbtexture->LockRect(0, &rgbLockedRect, NULL, 0);
	if (depthLockedRect.Pitch != 0 && rgbLockedRect.Pitch != 0) {
		const USHORT* curr = (const USHORT*)depthLockedRect.pBits;
		const BYTE* start = (const BYTE*)rgbLockedRect.pBits;
		int k = 0;
		
		for (int j = 0; j < KINECT_HEIGHT; ++j) {
			int h = 0;
			for (int i = 0; i < KINECT_WIDTH; ++i) {
				// Get depth of pixel in millimeters
				USHORT depth = NuiDepthPixelToDepth(*curr++);
				
				vb[j][i] = depth << 3;
				// Store the index into the color array corresponding to this pixel
				long xPixel, yPixel;
				NuiImageGetColorPixelCoordinatesFromDepthPixelAtResolution(
					NUI_IMAGE_RESOLUTION_640x480, NUI_IMAGE_RESOLUTION_640x480, NULL,
					i, j, depth << 3, &xPixel, &yPixel);

				if (xPixel < 0 || yPixel < 0 || xPixel > KINECT_WIDTH || yPixel > KINECT_HEIGHT) {
					cb[j][h] = 0;
					cb[j][h + 1] = 0;
					cb[j][h + 2] = 0;
					xPixel = i;
					yPixel = j;
				}
				else {
					const BYTE* curr = start + (xPixel + KINECT_WIDTH * yPixel) * 4;
					cb[j][h] = curr[2];
					cb[j][h + 1] = curr[1];
					cb[j][h + 2] = curr[0];
				}
				k++;
				h += 3;
			}
		}
	}
	depthtexture->UnlockRect(0);
	sensor->NuiImageStreamReleaseFrame(depthStream, &depthimageFrame);
	rgbtexture->UnlockRect(0);
	sensor->NuiImageStreamReleaseFrame(rgbStream, &rgbimageFrame);
}

void KinectController::Shutdown() {
	sensor->NuiShutdown();
	sensor->Release();

	for (int i = 0; i < KINECT_HEIGHT; i++) delete[] vb[i];
	delete[] vb;

	for (int i = 0; i < KINECT_HEIGHT; i++) delete[] cb[i];
	delete[] cb;
}