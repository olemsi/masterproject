#include "stdafx.h"
#include "SerialHandler.h"

using namespace std;


void SerialHandler::writeData(string msg) {
	{
		lock_guard<mutex> lkWrite(mtx_Write);
		writeMsg = msg;
		newData = true;
	}
	cv_writeMsgAvail.notify_one();
	
}


string SerialHandler::readData() {
	string returnVal = "";

	if (!dataAvail) return "";

	{
		lock_guard<mutex> lkRead(mtx_Read);
		returnVal = readMsg;
		dataAvail = false;
	}
	
	return returnVal;
}


void SerialHandler::DevicePollerThread(HANDLE hComm) {

	char charAr[80];
	DWORD dwRead, dwWritten;
	string msg, strRec;
	
	while (running) {
		char szBuffer[200];
		unique_lock<mutex> lkWrite(mtx_Write); // Wait for signal that data is available to send
		cv_writeMsgAvail.wait(lkWrite, [&](){return newData || !running; });
		msg = writeMsg;
		newData = false;
		lkWrite.unlock();
		if (running) {
			strcpy_s(charAr, msg.c_str());
			WriteFile(hComm, &charAr[0], strlen(charAr), &dwWritten, NULL);

			ReadFile(hComm, szBuffer, sizeof(szBuffer), &dwRead, NULL);

			szBuffer[dwRead] = 0;

			{
				lock_guard<mutex> lkRead(mtx_Read);
				readMsg = string(szBuffer);
				dataAvail = true;
			}
		}
	}
	CloseHandle(hComm);

}


bool SerialHandler::init(const char *COM, DWORD Baud, BYTE Databits, BYTE Stopbits, BYTE Parity) {
	try {
		running = true;
		
		DCB dcb = { 0 };
		
		OVERLAPPED ovlr = { 0 }, ovlw = { 0 };
		COMMTIMEOUTS cto;

		// Creating handle
		hComm = CreateFileA(COM, GENERIC_READ | GENERIC_WRITE,
			0, NULL, OPEN_EXISTING,
			0, NULL);

		if (hComm == INVALID_HANDLE_VALUE) {
			printf("Unable to open COM port: %s", COM);
			return false;
		}

		// Get the state of the device and modify it
		dcb.DCBlength = sizeof(dcb);
		GetCommState(hComm, &dcb);
		dcb.BaudRate = Baud;
		dcb.ByteSize = Databits;
		dcb.StopBits = Stopbits;
		dcb.Parity = Parity;
		SetCommState(hComm, &dcb);
		cto.ReadIntervalTimeout = 3;
		cto.ReadTotalTimeoutConstant = 3;
		cto.ReadTotalTimeoutMultiplier = 3;
		cto.WriteTotalTimeoutConstant = 0;
		cto.WriteTotalTimeoutMultiplier = 0;

		SetCommTimeouts(hComm, &cto);

		comThread = thread(&SerialHandler::DevicePollerThread, this, hComm);
	}
	catch (int e) {
		printf("Failed to initialize serial communication to: %s, code %i", COM, e);
		return false;
	}

	return true;
}

void SerialHandler::Shutdown() {
	running = false;
	cv_writeMsgAvail.notify_all();
	if (comThread.joinable()){
		comThread.join();
	}
}