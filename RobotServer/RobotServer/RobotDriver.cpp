#include "stdafx.h"
#include "RobotDriver.h"

using namespace std;

float old_Angle = 0, old_Speed = 0, old_Rotation = 0;

bool RobotDriver::init(const char *COM, DWORD Baud, BYTE Databits, BYTE Stopbits, BYTE Parity) {
	
	return SH.init(COM, Baud, Databits, Stopbits, Parity);
	
}

void RobotDriver::moveRobot(float angle, float speed, float rotation) { // Rotation > 0 : Clockwize rotation
	string msg = "D|0|0|0";
	int angleMsg = (int) angle;
	int speedMsg = (int) speed;
	int rotMsg = (int)rotation;

	
	if (rotation != 0.0f) {
		msg = "D|" + to_string(angleMsg) + "|" + to_string(speedMsg) + "|" + to_string(rotMsg) + "\n";
	}
	else {
		msg = "D|" + to_string(angleMsg) + "|" + to_string(speedMsg) + "\n";
	}

	SH.writeData(msg);
	
}

void RobotDriver::moveToTargetRobot(float distance, bool direction) {
	string msg = "T|0|0";
	int distanceMsg = (int)distance;
	int directionMsg = (int)direction;

	
	msg = "T|" + to_string(distanceMsg) + "|" + to_string(directionMsg) + "\n";

	SH.writeData(msg);
}

string RobotDriver::getResponse() {

	return SH.readData();

}

void RobotDriver::Shutdown() {

	SH.Shutdown();
}