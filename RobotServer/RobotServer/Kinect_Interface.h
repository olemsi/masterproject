#include <Windows.h>
#include <NuiApi.h>
#include <NuiImageCamera.h>
#include <NuiSensor.h>
#include <vector>
#define KINECT_WIDTH 640
#define KINECT_HEIGHT 480

class KinectController {
public:
	USHORT **vb;
	unsigned char **cb;
	bool initKinect();
	void getImageData();
	
	void Shutdown();


private:
	HANDLE depthStream;
	HANDLE rgbStream;
	INuiSensor* sensor;
};