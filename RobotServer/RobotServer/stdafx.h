// stdafx.h : include file for standard system include files,
// or project specific include files that are used frequently, but
// are changed infrequently
//

#pragma once

#include "targetver.h"
#include <stdio.h>
#include <tchar.h>
#define _ATL_CSTRING_EXPLICIT_CONSTRUCTORS      // some CString constructors will be explicit
#define _AFX_NO_MFC_CONTROLS_IN_DIALOGS         // remove support for MFC controls in dialogs

#ifndef VC_EXTRALEAN
#define VC_EXTRALEAN            // Exclude rarely-used stuff from Windows headers
#endif

#include <afx.h>
#include <afxwin.h>         // MFC core and standard components
#include <afxext.h>         // MFC extensions
#ifndef _AFX_NO_OLE_SUPPORT
#include <afxdtctl.h>           // MFC support for Internet Explorer 4 Common Controls
#endif
#ifndef _AFX_NO_AFXCMN_SUPPORT
#include <afxcmn.h>                     // MFC support for Windows Common Controls
#endif // _AFX_NO_AFXCMN_SUPPORT

// TODO: reference additional headers your program requires here
#include <WinSock2.h>
#include <WS2tcpip.h>

#include "usbc\error.h"
#include "usbc\usbcdef.h"
#include "usbc\usbc.h"
#include "usbc\extern.h"

#include <iostream>

enum MessageType {
	INCL_ROBOT,
	INCL_PTU,
	INCL_DRIVER
};

enum SetPointData
{
	ROBOT_BASE,
	ROBOT_SHOULDER,
	ROBOT_ELBOW,
	ROBOT_ROLL,
	ROBOT_PITCH,
	ROBOT_JAW,
	PTU_P,
	PTU_T,
	DRIVER_ANGLE,
	DRIVER_SPEED,
	DRIVER_ROTATION
};

typedef float RemoteSetPoints[13];
