#ifndef PANTILTUNIT_H // Header guard
#define PANTILTUNIT_H
#include "SerialHandler.h"

class PanTiltUnit {
public:

	bool init(const char *COM, DWORD Baud, BYTE Databits, BYTE Stopbits, BYTE Parity);
	void setTargetDirection(float pan, float tilt);

	string getResponse();

	void Shutdown();

private:
	SerialHandler SH;
};

#endif