#include "LeapMotion_Interface.h"
#include <thread>


bool LeapMotionController::initLeap() {

	
	leap_Controller.setPolicy(Controller::POLICY_OPTIMIZE_HMD);
	int i = 0;
	while (!leap_Controller.isConnected())
	{
		if (i >= 10) return false;
		std::this_thread::sleep_for(std::chrono::milliseconds(100));
		i++;
	}

	return true;
}


bool LeapMotionController::getHandPositions(float &X, float &Y, float &Z, float &roll, float &pitch, float &yaw) {
		
	Hand ctrlHand = leap_Controller.frame().hands().leftmost();
	
	if (!ctrlHand.isValid() || !ctrlHand.isLeft()) return false; // No hands found
	
	// Alternative approach when using Scorbots MoveLinear function
	//FingerList fingers = ctrlHand.fingers();

	//Finger thumb_Finger = fingers[0];
	//Finger index_Finger = fingers[1];

	//Vector thumbPos = thumb_Finger.tipPosition(); // [x1, y1, z1]
	//Vector indexPos = index_Finger.tipPosition(); // [x2, y2, z2]
	// Distance vector: [|x1 - x2|, |y1 - y2|, |z1 - z2|] = [x3, y3, z3]

	// float gripperDistance = thumbPos.distanceTo(indexPos); // Nesten litt for enkelt...


	// Length between fingertips in mm: |sqrt(x3^2 + y3^2 + z3^2)|   == Setpoint for gripper

	// [(x1 + x2)/2, (y1 + y2)/2, (z1 + z2)/2] = Setpoint for robot position

	// Setpoints for gripper angles
	//Vector pitchAngle = ctrlHand.direction.pitch(); // Pitch / Yaw - Direction from center of palm to fingers
	//Vector rollAngle = ctrlHand.palmNormal.roll(); // Roll
	if (ctrlHand.confidence() > 0.6f) // Must be at least 60% confident that the hand position is correct
	{
		Y = ctrlHand.palmPosition().y;
		X = ctrlHand.palmPosition().x;
		Z = -ctrlHand.palmPosition().z;
		pitch = -ctrlHand.direction().pitch() * RAD_TO_DEG;
		roll = (ctrlHand.palmNormal().roll() * RAD_TO_DEG) + 90.0f; // Unused, due to tracking difficulties
		float pinchDistance = ctrlHand.pinchDistance() - 20.0f;
		if (pinchDistance <= 5.0f) pinchDistance = 0.0f;
		else if (pinchDistance > 70.0f) pinchDistance = 70.0f;
		yaw = pinchDistance;
		
		return true;
	}
	
	return false;
	
}


void LeapMotionController::Shutdown() {

}

// Excluded in the final version, but may be used to normalize coordinates according to the interaction box of the device.
Vector LeapMotionController::LeapToWorld(Vector leapPoint, InteractionBox interactionBox) {

	Vector normalizedCoord = interactionBox.normalizePoint(leapPoint, true);

	normalizedCoord -= Vector(0.5, 0, 0.5); // Center in the middle of the nearest plane of the interactionbox
	
	return normalizedCoord;


}