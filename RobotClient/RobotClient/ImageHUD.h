#include <opencv2\core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <string>
#include <time.h>

using namespace cv;
using namespace std;

#define HUD_ENVIRONMENT 1
#define HUD_MAIN 2
#define HUD_ROBOT 3


// HUD Adjustment functions
void incrementHorizontalOffset();
void decrementHorizontalOffset();
void incrementVerticalOffset();
void decrementVerticalOffset();



void paintMain(Mat &img, int eye, int fpsLeft, int fpsRight, float angle, bool menuActiveL, bool menuActiveR);

void paintEnvironmental(Mat &img, int eye, float angle, bool menuActiveL, bool menuActiveH);

void paintRobotic(Mat &img, int eye, float angle, bool menuActiveL, bool menuActiveR);