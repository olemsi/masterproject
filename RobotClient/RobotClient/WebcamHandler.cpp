#include "WebcamHandler.h"

cv::Mat image;

void WebcamHandler::setResult(const Mat &newFrame) {
	lock_guard<mutex> guard(lock);
	frame = newFrame;
	hasFrame = true;
	
}

bool WebcamHandler::getResult(Mat &out) {
	if (!hasFrame) {
		return false;
	}
	lock_guard<mutex> guard(lock);
	out = frame;
	hasFrame = false;

	return true;
}



float WebcamHandler::startCapture(string adr) {
	
	if (!videoCapture.open(cv::String(adr))) {
		// Error opening video
		return 0;
	}

	if (!videoCapture.isOpened())  // if not success, exit program
	{
		//cout << "Cannot open the video cam" << endl;
		return 0;
	}
	for (int i = 0; i < 10 && !videoCapture.read(frame); i++) {
		std::this_thread::sleep_for(std::chrono::milliseconds(10));
	}
	if (!videoCapture.read(frame)) {
		//cerr << "Unable to get first frame from Left Camera";
	}

	float aspectRatio = (float)frame.cols / (float)frame.rows;
	camThread = thread(&WebcamHandler::captureLoop, this);
	
	return aspectRatio;
	//return 1.0f;
}



void WebcamHandler::stopCapture() {
	running = false;
	camThread.join();
	videoCapture.release();
}

void WebcamHandler::captureLoop() {
	Mat newFrame;
	
	while (running) {
		videoCapture.read(newFrame);
		setResult(newFrame);
	}
}