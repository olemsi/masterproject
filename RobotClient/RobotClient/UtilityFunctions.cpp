#include "UtilityFunctions.h"

float scaleValue(float val, float minX, float maxX, float minY, float maxY) {

	if (minX != maxX) {
		return (((maxY - minY) * (val - minX)) / (maxX - minX)) + minY;
	}
	else {
		return 0;
	}


}