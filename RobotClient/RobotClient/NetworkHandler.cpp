//#ifndef UNICODE
//#define UNICODE
//#endif

#include "NetworkHandler.h"
char *SendBuf = NULL;


SOCKET SendSocket = INVALID_SOCKET;
SOCKET RecvSocket = INVALID_SOCKET;
sockaddr_in SendAddr;
sockaddr_in ReceiveAddr;

int NetworkHandler::init(const char* IP, unsigned short sendPort, unsigned short receivePort)
{
	KinectColors = new unsigned char[640 * 480 * 3];
	
	KinectVB.resize(640 * 480);
	int iResult;
	WSADATA wsaData;

	// Initialize Winsock
	iResult = WSAStartup(MAKEWORD(2, 2), &wsaData);
	if (iResult != NO_ERROR) {
		wprintf(L"WSAStartup failed with error: %d\n", iResult);
		return 1;
	}

	// Create a socket for sending data
	SendSocket = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP);
	if (SendSocket == INVALID_SOCKET) {
		wprintf(L"socket failed with error: %ld\n", WSAGetLastError());
		WSACleanup();
		return 1;
	}

	SendAddr.sin_family = AF_INET;
	SendAddr.sin_port = htons(sendPort);
	inet_pton(AF_INET, IP, &(SendAddr.sin_addr));

	// Create socket for receiving data
	RecvSocket = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP);
	if (RecvSocket == INVALID_SOCKET) {
		wprintf(L"socket failed with error: %ld\n", WSAGetLastError());
		WSACleanup();
		return 1;
	}

	DWORD tv = 100; // Adding timeout to receiver
	if (setsockopt(RecvSocket, SOL_SOCKET, SO_RCVTIMEO, (char *)&tv, sizeof(tv)) < 0) {
		perror("Error");
	}
	// Set up the RecvAddr structure with the IP address of
	// the receiver and the specified port number.
	ReceiveAddr.sin_family = AF_INET;
	ReceiveAddr.sin_port = htons(receivePort);
	ReceiveAddr.sin_addr.s_addr = htonl(INADDR_ANY);

	iResult = ::bind(RecvSocket, (SOCKADDR *)& ReceiveAddr, sizeof(ReceiveAddr));
	if (iResult != 0) {
		int err = WSAGetLastError();
		wprintf(L"bind failed with error %d\n", WSAGetLastError());
		return false;
	}
	th_dataReceiver = thread(&NetworkHandler::dataReceiverLoop, this);
	return 0;
}

bool NetworkHandler::sendSetPoints(RemoteSetPoints SP, char cmdRobot, bool activePTU, bool activeDriver, char requestKinect) {
	
	char outputBuffer[64] { 0 };
	float* f_buf = (float*)outputBuffer;

	outputBuffer[0] = cmdRobot; // First byte
	if (activePTU) outputBuffer[1] = 'Y'; else outputBuffer[1] = 'N';
	if (activeDriver) outputBuffer[2] = 'Y'; else outputBuffer[2] = 'N';
	outputBuffer[3] = requestKinect; // D = Full Depth & color image, C = Color image only

	f_buf[1] = SP[ROBOT_BASE]; // Float occupies 4 bytes, hence f_buf[1] = outputBuffer[4] .. outputBuffer[7]
	f_buf[2] = SP[ROBOT_SHOULDER];
	f_buf[3] = SP[ROBOT_ELBOW];
	f_buf[4] = SP[ROBOT_ROLL];
	f_buf[5] = SP[ROBOT_PITCH];
	f_buf[6] = SP[ROBOT_JAW];
	f_buf[7] = SP[PTU_P];
	f_buf[8] = SP[PTU_T];
	f_buf[9] = SP[DRIVER_ANGLE];
	f_buf[10] = SP[DRIVER_SPEED];
	f_buf[11] = SP[DRIVER_ROTATION];

	// Send a datagram to the receiver
	int iResult = sendto(SendSocket,
		outputBuffer, sizeof(outputBuffer), 0, (SOCKADDR *)& SendAddr, sizeof(SendAddr));
	if (iResult == SOCKET_ERROR) {
		wprintf(L"sendto failed with error: %d\n", WSAGetLastError());
		return false;
	}

	return true;
}

void NetworkHandler::dataReceiverLoop(){ 

	
	sockaddr_in SenderAddr;
	int SenderAddrSize = sizeof(SenderAddr);

	std::vector<Vertex> tempVb;
	tempVb.resize(640 * 480);
	bitset<480> rowsReceived;
	bitset<480> rowsReceived_Color;

	
	int iRecvResult, iSendResult;
	int RecvAddressSize = sizeof(ReceiveAddr);
	
	while (running){
		char RecvBuf[4096] { 0 };
		
		USHORT *us_buf = (USHORT*)RecvBuf;

		iRecvResult = recvfrom(RecvSocket, RecvBuf, sizeof(RecvBuf), 0, (SOCKADDR *)&SenderAddr, &SenderAddrSize);
		if (iRecvResult == SOCKET_ERROR){
			printf("recv failed with error: %d\n", WSAGetLastError());
			setBatteryLevel(255);
		}
		else {
			if (RecvBuf[0] == 'K') {
				int row = RecvBuf[1] * 100 + RecvBuf[2] * 10 + RecvBuf[3];
				for (int i = 0; i < 640; ++i) {
					USHORT depthVal = us_buf[i * 3 + 2];
					
					float z_value = static_cast<FLOAT>(depthVal >> 3) / 1000.0f; // Storing current row data in buffer
					float x_value = ((float)i - 640.0f / 2.0f) * (320.0f / 640.0f) * 0.00350100012f * z_value;
					float y_value = -((float)row - 480.0f / 2.0f) * (240.0f / 480.0f) * 0.00350100012f * z_value;
					Vertex vert = { -x_value, y_value, -z_value + 0.06f * abs(cos((((float) i) / 639.0f) * 3.14159264f)), (BYTE)RecvBuf[i * 6 + 6] / 255.0f, (BYTE)RecvBuf[i * 6 + 7] / 255.0f, (BYTE)RecvBuf[i * 6 + 8] / 255.0f, 1.0f, 0.5f, 0.5f };
					tempVb[row * 639 + i] = vert;
				}
				rowsReceived[row] = true;
			
			}
			else if (RecvBuf[0] == 'C') { 
				int row = RecvBuf[1] * 100 + RecvBuf[2] * 10 + RecvBuf[3];
				{
					lock_guard<mutex> guard(lock_KinectColor);
					for (int i = 0; i < 640; ++i) {

						KinectColors[row * 640 * 3 + i * 3] = RecvBuf[i * 3 + 6]; // B
						KinectColors[row * 640 * 3 + i * 3 + 1] = RecvBuf[i * 3 + 5]; // G
						KinectColors[row * 640 * 3 + i * 3 + 2] = RecvBuf[i * 3 + 4]; // R
						
					}
					for (int i = 0; i < 640; ++i) {

						KinectColors[(row + 1) * 640 * 3 + i * 3] = RecvBuf[1924 + i * 3 + 2]; // B
						KinectColors[(row + 1) * 640 * 3 + i * 3 + 1] = RecvBuf[1924 + i * 3 + 1]; // G
						KinectColors[(row + 1) * 640 * 3 + i * 3 + 2] = RecvBuf[1924 + i * 3]; // R
						
					}
				}
				rowsReceived_Color[row] = true;
				rowsReceived_Color[row+1] = true;

			}
			else if (RecvBuf[0] == 'B')
			{
				setBatteryLevel(RecvBuf[1]); // Battery
			}
		}

		if (rowsReceived.all()) {
			setKinectData(tempVb);
			rowsReceived.reset();
		}
		if (rowsReceived_Color.all()) {
			
			newVal_KinectColor = true;
			rowsReceived_Color.reset();
		}
		
	}
}


int NetworkHandler::shutDown() {

	// Closing the socket.
	wprintf(L"Finished sending. Closing socket.\n");
	int iResult = sendto(SendSocket,
		0, 0, 0, (SOCKADDR *)& SendAddr, sizeof(SendAddr));

	running = false;
	th_dataReceiver.join();
	iResult = closesocket(SendSocket);
	if (iResult == SOCKET_ERROR) {
		wprintf(L"closesocket failed with error: %d\n", WSAGetLastError());
		WSACleanup();
		return 1;
	}
	iResult = closesocket(RecvSocket);
	if (iResult == SOCKET_ERROR) {
		wprintf(L"closesocket failed with error: %d\n", WSAGetLastError());
		WSACleanup();
		return 1;
	}
	// Clean up and quit.
	WSACleanup();

	//for (int i = 0; i < KINECT_HEIGHT * KINECT_WIDTH * 4; i++) delete KinectColors;
	delete[] KinectColors;
	
	return 0;
}

void NetworkHandler::setBatteryLevel(unsigned char val) {

	lock_guard<mutex> guard(lock_Battery);
	recData = (float)val;
	newVal_Batt = true;
}

bool NetworkHandler::getBatteryLevel(float &val) {

	if (!newVal_Batt) {
		return false;
	}
	lock_guard<std::mutex> guard(lock_Battery);
	newVal_Batt = false;
	val = recData;

	return true;

}

void NetworkHandler::setKinectData(std::vector<Vertex> &vertexBuffer) {

	lock_guard<mutex> guard(lock_Kinect);
	KinectVB = vertexBuffer;
	newVal_Kinect = true;
}

bool NetworkHandler::getKinectData(std::vector<Vertex> &vertexBuffer) {

	if (!newVal_Kinect) {
		return false;
	}
	lock_guard<std::mutex> guard(lock_Kinect);
	newVal_Kinect = false;
	vertexBuffer = KinectVB;

	return true;

}

bool NetworkHandler::getKinectColor(cv::Mat &img) {

	if (!newVal_KinectColor) {
		return false;
	}
	lock_guard<std::mutex> guard(lock_KinectColor);
	newVal_KinectColor = false;
	
	// Turning the color values into a format usable as texture
	cv::Mat image = cv::Mat(480, 640, CV_8UC3, KinectColors, CV_AUTOSTEP);
	img = image;
	
	
	return true;

}