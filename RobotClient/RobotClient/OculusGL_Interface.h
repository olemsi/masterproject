#include <OVR_CAPI_GL.h>
#include <Extras\OVR_Math.h>

#include <Windows.h>

#include <GL\glew.h>
#include <GL\wglew.h>
#include <gl\GLU.h>

#include <glm\glm.hpp>
#include <glm\gtc\matrix_transform.hpp>
#include <glm\gtc\type_ptr.hpp>


#include <opencv2\core.hpp>
#include <opencv2\highgui\highgui.hpp>
#include <opencv2\imgproc\imgproc.hpp>

#include <time.h>
#include <map>
#include <ft2build.h>
#include FT_FREETYPE_H

#include "UtilityFunctions.h"





struct headAngle {
	float yaw;
	float pitch;
	float roll;
};

struct windowSize {
	int width;
	int height;
};



struct Character {
	GLuint TextureID;   // ID handle of the glyph texture
	glm::ivec2 Size;    // Size of glyph
	glm::ivec2 Bearing;  // Offset from baseline to left/top of glyph
	GLuint Advance;    // Horizontal offset to advance to next glyph
};

struct ShapeAttribs {
	float height;
	float width;
	float depth = 0.0f;
	float radius;
	float opaqueness = 1.0f;
	float zoomTex = 0.0f;
	glm::vec3 color = { 1.0f, 1.0f, 1.0f };
	glm::mat4 translationMatrix = glm::mat4();
	glm::mat4 rotationMatrix = glm::mat4();
};


struct Model {
	std::vector<Vertex> vertices;
	std::vector<GLuint> indices;
	std::string sName;

	glm::mat4 modelMatrix;

	GLuint vbo; // Vertex Buffer Object
	GLuint ibo; // Index Buffer Object
	GLuint texture;
	bool isVisible = true;
	Model(){};

	// Kinect point cloud model constructor
	Model(std::vector<Vertex> vertexArray, glm::mat4 mMatrix, std::string name) {
		vertices = vertexArray;
		modelMatrix = mMatrix;
		sName = name;

		glGenBuffers(1, &vbo);
		glBindBuffer(GL_ARRAY_BUFFER, vbo);
		glBufferData(GL_ARRAY_BUFFER, vertices.size() * sizeof(Vertex), &vertices[0], GL_DYNAMIC_DRAW);
		glGenBuffers(1, &ibo);

		glGenTextures(1, &texture);
		glBindTexture(GL_TEXTURE_2D, texture);

		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
		const GLbyte noTexture = 0xff;
		glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, 1, 1, 0, GL_BGR, GL_UNSIGNED_BYTE, &noTexture);
	}

	// Character model constructor
	Model(std::vector<Vertex> vertexArray, std::vector<GLuint> indexArray, glm::mat4 mMatrix, GLuint tex, std::string name) {
		vertices = vertexArray;
		indices = indexArray;
		modelMatrix = mMatrix;
		texture = tex;
		sName = name;

		glGenBuffers(1, &vbo);
		glBindBuffer(GL_ARRAY_BUFFER, vbo);
		glBufferData(GL_ARRAY_BUFFER, vertices.size() * sizeof(Vertex), &vertices[0], GL_STATIC_DRAW);

		glGenBuffers(1, &ibo);
		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, ibo);
		glBufferData(GL_ELEMENT_ARRAY_BUFFER, indices.size() * sizeof(GLuint), &indices[0], GL_STATIC_DRAW);



	}

	// 3D model constructor
	Model(std::vector<Vertex> vertexArray, std::vector<GLuint> indexArray, glm::mat4 mMatrix, cv::Mat tex, std::string name, bool visible = true) {
		vertices = vertexArray;
		indices = indexArray;
		modelMatrix = mMatrix;
		isVisible = visible;
		sName = name;

		glGenBuffers(1, &vbo);
		glBindBuffer(GL_ARRAY_BUFFER, vbo);
		glBufferData(GL_ARRAY_BUFFER, vertices.size() * sizeof(Vertex), &vertices[0], GL_DYNAMIC_DRAW);

		glGenBuffers(1, &ibo);
		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, ibo);
		glBufferData(GL_ELEMENT_ARRAY_BUFFER, indices.size() * sizeof(GLuint), &indices[0], GL_DYNAMIC_DRAW);

		
		glGenTextures(1, &texture);
		glBindTexture(GL_TEXTURE_2D, texture);

		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
		if (!tex.empty()) {
			glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, tex.size().width, tex.size().height, 0, GL_BGR, GL_UNSIGNED_BYTE, tex.data);
		}
		else {
			const GLbyte noTexture = 0xff;
			glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, 1, 1, 0, GL_BGR, GL_UNSIGNED_BYTE, &noTexture);
		}
		
	}

	void updateVertices(std::vector<Vertex> v) {
		vertices = v;
		glBindBuffer(GL_ARRAY_BUFFER, vbo);
		glBufferSubData(GL_ARRAY_BUFFER, 0, vertices.size() * sizeof(Vertex), &vertices[0]);
	}

	void updateTexture(cv::Mat tex) {
		glBindTexture(GL_TEXTURE_2D, texture);
		glTexSubImage2D(GL_TEXTURE_2D, 0, 0, 0, tex.size().width, tex.size().height, GL_BGR, GL_UNSIGNED_BYTE, tex.data);
	}

	void updateTexture(GLuint texID) {
		texture = texID;
	}

	void updateTexture(unsigned char* data, int width, int height) {
		glBindTexture(GL_TEXTURE_2D, texture);
		glTexSubImage2D(GL_TEXTURE_2D, 0, 0, 0, width, height, GL_BGR, GL_UNSIGNED_BYTE, data);
	}

	std::string getName() {
		return sName;
	}

	void setVisible(bool visible) {
		isVisible = visible;
	}

	void freeModel(bool deleteTexture = false) { // When model is destroyed, its occupied graphics memory should be released.
		if (deleteTexture) {
			glDeleteTextures(1, &texture);
		}
		glDeleteBuffers(1, &vbo);
		glDeleteBuffers(1, &ibo);
	}
};

struct ModelCollection {
	
	std::vector<Model> modelList;
	std::string sName;

	ModelCollection();

	ModelCollection(std::vector<Model> mdlList, std::string name)
	{
		sName = name;
		modelList = mdlList;
	}

	std::string getName() {
		return sName;
	}

};

class OculusRift_Interface {
public:
	// Variables
	ovrHmd HMD;
	ovrEyeRenderDesc EyeRenderDescription[2];
	ovrSizei idealTextureSize_Left;
	ovrSizei idealTextureSize_Right;
	int idealWindowWidth;
	int idealWindowHeight;
	

	// Constructor
	OculusRift_Interface();

	// Methods
	bool initRift();
	headAngle getHeadAngle();
	void convertLeapToOculus(float &x, float &y, float &z);
	void RecenterHeadPose();
	void SubmitToRift(ovrSwapTextureSet *textureSet_Left, ovrSwapTextureSet *textureSet_Right);
	void Shutdown();

};



static glm::vec3 posTopLeft = glm::vec3(-35.0f, 26.0f, 1.0f);
static glm::vec3 posMidLeft = glm::vec3(-35.0f, 0.0f, 1.0f);
static glm::vec3 posBotLeft = glm::vec3(-35.0f, -28.0f, 1.0f);

static glm::vec3 posTopRight = glm::vec3(35.0f, 26.0f, 1.0f);
static glm::vec3 posMidRight = glm::vec3(35.0f, 0.0f, 1.0f);
static glm::vec3 posBotRight = glm::vec3(35.0f, -28.0f, 1.0f);

static glm::vec3 posTopMid = glm::vec3(0.0f, 26.0f, 1.0f);
static glm::vec3 posBotMid = glm::vec3(0.0f, -28.0f, 1.0f);
static glm::vec3 posCenter = glm::vec3(0.0f, 0.0f, 1.0f);

static glm::vec3 posAlarms = glm::vec3(0.0f, 20.0f, 1.0f);

static glm::vec3 txtColor = glm::vec3(1.0f, 0.5f, 0.0f);

static float textScale = 0.04f;

class OpenGL_Handler {
public:
	// Variables
	ovrSwapTextureSet* Oculus_TextureSet_Left;
	ovrSwapTextureSet* Oculus_TextureSet_Right;
	ovrSwapTextureSet* Oculus_TextureSet_HUD;

	// Constructor
	OpenGL_Handler();

	// Methods
	bool initRendering(bool RiftIsPresent, OculusRift_Interface ovr,
		cv::Mat CamPicture_Left, cv::Mat CamPicture_Right, const char* fontDir);

	void updateRobotModel(float baseAngle, float shoulderAngle, float elbowAngle, float wristRollAngle, float wristPitchAngle, float fingersDistance, float opaqueness);
	void updateCompassModel(float angle);
	void updateSpeedArrow(float speed, float angle, float rotation);
	void updateBatteryLevel(float capacity);
	void updateTextModels(std::string name, std::string text, glm::vec3 txtPos, glm::vec3 color = txtColor);
	void updateClockModel();
	void updateKinectModel(std::vector<Vertex> kinectData, float angle);
	bool removeModelCollection(std::string listName);
	bool removeModel(std::string listName, std::string mdlName);


	void Render_RiftPresent(OculusRift_Interface ovr);
	void Render_RiftMissing(HDC hDC, windowSize windowRect);
	void mirrorToScreen(HDC hDC);
	void updateCameraTexture(int eye, cv::Mat CamPicture, bool zoom = false, bool hideCameras = false);
	void updateKinectTexture(cv::Mat img, bool zoom, bool hideTexture = true);
	void shutdown(bool RiftIsPresent, ovrHmd HMD);


private:
	GLuint getVertexShader(const char* src);
	GLuint getFragmentShader(const char* src);
	GLuint getShaderProgram(GLuint, GLuint);
	void setupMirrorTexture(ovrHmd hmd, int windowWidth, int windowHeight);
	std::vector<Model> addSurfaceModels(bool RiftIsPresent, cv::Mat &CamPicture_Left, cv::Mat &CamPicture_Right);
	void getKinectData();

	bool initFreetype(const char* fontPath);
	std::map<GLchar, Character> Characters;
	std::vector<ModelCollection> modelCollection;
	
	// 3D Modeling functions
	void addBatteryModel(std::vector<Model> *mdlList, std::string name, float capacity, glm::mat4 modelMatrix = glm::mat4(), cv::Mat tex = cv::Mat());
	void addArrowModel(std::vector<Model> *mdlList, std::string name, float angle, glm::mat4 modelMatrix = glm::mat4(), cv::Mat tex = cv::Mat());
	void addRobotModel(std::vector<Model> *mdlList, std::string name, float baseAngle, float shoulderAngle, float elbowAngle, float wristRollAngle, float wristPitchAngle, float fingersDistance, float opaqueness = 0.5f, glm::mat4 modelMatrix = glm::mat4(), cv::Mat tex = cv::Mat());
	void addClockModel(std::vector<Model> *mdlList, std::string name, tm time, glm::mat4 modelMatrix = glm::mat4(), cv::Mat tex = cv::Mat());
	void addSpeedoMeter(std::vector<ModelCollection> &modelCollection, std::string name, float speed, float angle, float rotation, glm::mat4 modelMatrix = glm::mat4(), cv::Mat tex = cv::Mat());

	void addShape_Cylinder(std::vector<Vertex> &vertexBuffer, std::vector<GLuint> &indexBuffer, ShapeAttribs shape);
	void addShape_Cube(std::vector<Vertex> &vertexBuffer, std::vector<GLuint> &indexBuffer, ShapeAttribs shape);
	void addShape_TriangularPrism(std::vector<Vertex> &vertexBuffer, std::vector<GLuint> &indexBuffer, ShapeAttribs shape);

	void addShape_Plane(std::vector<Vertex> &vertexBuffer, std::vector<GLuint> &indexBuffer, ShapeAttribs shape, bool mirrorTexture = false);
	void addShape_Circle(std::vector<Vertex> &vertexBuffer, std::vector<GLuint> &indexBuffer, ShapeAttribs shape);

	void addTextModels(std::vector<ModelCollection> &modelCollection, std::string text, float scale, glm::vec3 textPosition, glm::vec3 color, std::string name, bool background = false, float xRotation = 0.0f);

	std::vector<ModelCollection> mainHud(bool RiftIsPresent, cv::Mat &CamPicture_Left, cv::Mat &CamPicture_Right);
	
	std::vector<Model>* findModelList(std::vector<ModelCollection> &mdlCollection, std::string name);
	Model* findModel(std::vector<Model> &mdlList, std::string name);

	cv::Mat texSpeedo = cv::imread("speedoTexture.jpg");
	cv::Mat texSpeedoCW = cv::imread("speedoTextureCW.jpg");
	cv::Mat texSpeedoCCW = cv::imread("speedoTextureCCW.jpg");
};