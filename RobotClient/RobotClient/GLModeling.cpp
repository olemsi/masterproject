#include "OculusGL_Interface.h"

// Adds a cube shape to the supplied vertex buffer and index buffer, according to spesifications in the shape attribute structure.
// Shape attributes implemented: Width, Height, Depth, Opaqueness, Color, Translation Matrix, Rotation Matrix
void OpenGL_Handler::addShape_Cube(std::vector<Vertex> &vertexBuffer, std::vector<GLuint> &indexBuffer, ShapeAttribs shape) {

	float halfWidth = shape.width / 2.0f;
	float halfHeight = shape.height / 2.0f;
	float halfDepth = shape.depth / 2.0f;
	float opaqueness = shape.opaqueness;
	glm::vec3 color = shape.color;

	int indexOffset = vertexBuffer.size(); // Compensating indices in case of an already populated vertex buffer

	// Front plane
	vertexBuffer.push_back({ -halfWidth, -halfHeight, halfDepth, color.r, color.g, color.b, opaqueness, 0.0f, 0.0f }); // Bottom left
	vertexBuffer.push_back({ halfWidth, -halfHeight, halfDepth, color.r, color.g, color.b, opaqueness, 1.0f, 0.0f });	// Bottom right
	vertexBuffer.push_back({ halfWidth, halfHeight, halfDepth, color.r, color.g, color.b, opaqueness, 1.0f, 1.0f });	// Top right
	vertexBuffer.push_back({ -halfWidth, halfHeight, halfDepth, color.r, color.g, color.b, opaqueness, 0.0f, 1.0f });	// Top left - x, y, z, r, g, b, a, u, v
	// Back plane
	vertexBuffer.push_back({ -halfWidth, -halfHeight, -halfDepth, color.r, color.g, color.b, opaqueness, 0.0f, 0.0f }); // Bottom left
	vertexBuffer.push_back({ halfWidth, -halfHeight, -halfDepth, color.r, color.g, color.b, opaqueness, 1.0f, 0.0f });	// Bottom right
	vertexBuffer.push_back({ halfWidth, halfHeight, -halfDepth, color.r, color.g, color.b, opaqueness, 1.0f, 1.0f });	// Top right
	vertexBuffer.push_back({ -halfWidth, halfHeight, -halfDepth, color.r, color.g, color.b, opaqueness, 0.0f, 1.0f });	// Top left - x, y, z, r, g, b, a, u, v
	// Left plane
	vertexBuffer.push_back({ -halfWidth, -halfHeight, -halfDepth, color.r, color.g, color.b, opaqueness, 0.0f, 0.0f }); // Bottom left
	vertexBuffer.push_back({ -halfWidth, -halfHeight, halfDepth, color.r, color.g, color.b, opaqueness, 1.0f, 0.0f });	// Top left - x, y, z, r, g, b, a, u, v
	vertexBuffer.push_back({ -halfWidth, halfHeight, halfDepth, color.r, color.g, color.b, opaqueness, 1.0f, 1.0f });	// Top right
	vertexBuffer.push_back({ -halfWidth, halfHeight, -halfDepth, color.r, color.g, color.b, opaqueness, 0.0f, 1.0f });	// Bottom right
	// Right plane
	vertexBuffer.push_back({ halfWidth, -halfHeight, halfDepth, color.r, color.g, color.b, opaqueness, 0.0f, 0.0f });	// Top left - x, y, z, r, g, b, a, u, v
	vertexBuffer.push_back({ halfWidth, -halfHeight, -halfDepth, color.r, color.g, color.b, opaqueness, 1.0f, 0.0f }); // Bottom left
	vertexBuffer.push_back({ halfWidth, halfHeight, -halfDepth, color.r, color.g, color.b, opaqueness, 1.0f, 1.0f });	// Bottom right
	vertexBuffer.push_back({ halfWidth, halfHeight, halfDepth, color.r, color.g, color.b, opaqueness, 0.0f, 1.0f });	// Top right
	// Top plane
	vertexBuffer.push_back({ -halfWidth, halfHeight, halfDepth, color.r, color.g, color.b, opaqueness, 0.0f, 0.0f });	// Top left - x, y, z, r, g, b, a, u, v
	vertexBuffer.push_back({ halfWidth, halfHeight, halfDepth, color.r, color.g, color.b, opaqueness, 1.0f, 0.0f });	// Top right
	vertexBuffer.push_back({ halfWidth, halfHeight, -halfDepth, color.r, color.g, color.b, opaqueness, 1.0f, 1.0f });	// Bottom right
	vertexBuffer.push_back({ -halfWidth, halfHeight, -halfDepth, color.r, color.g, color.b, opaqueness, 0.0f, 1.0f }); // Bottom left
	// Bottom plane
	vertexBuffer.push_back({ -halfWidth, -halfHeight, -halfDepth, color.r, color.g, color.b, opaqueness, 0.0f, 0.0f }); // Bottom left
	vertexBuffer.push_back({ halfWidth, -halfHeight, -halfDepth, color.r, color.g, color.b, opaqueness, 1.0f, 0.0f });	// Bottom right
	vertexBuffer.push_back({ halfWidth, -halfHeight, halfDepth, color.r, color.g, color.b, opaqueness, 1.0f, 1.0f });	// Top right
	vertexBuffer.push_back({ -halfWidth, -halfHeight, halfDepth, color.r, color.g, color.b, opaqueness, 0.0f, 1.0f });	// Top left - x, y, z, r, g, b, a, u, v

	// Translating according to local shape matrices
	for (std::vector<Vertex>::iterator v = vertexBuffer.begin(); v != vertexBuffer.end(); ++v)
	{
		Vertex vert = *v;
		glm::vec4 newPos = shape.translationMatrix * shape.rotationMatrix * glm::vec4(vert.X, vert.Y, vert.Z, 1.0f);
		vert.X = newPos.x;
		vert.Y = newPos.y;
		vert.Z = newPos.z;
		*v = vert;
	}

	int v = 0;
	for (int i = 0; i < 6; i++) // 6 planes with 6 vertices needed
	{
		indexBuffer.push_back(0 + v + indexOffset);
		indexBuffer.push_back(1 + v + indexOffset);
		indexBuffer.push_back(2 + v + indexOffset);
		indexBuffer.push_back(2 + v + indexOffset);
		indexBuffer.push_back(3 + v + indexOffset);
		indexBuffer.push_back(0 + v + indexOffset);
		v += 4; // 4 vertices used per plane
	}
}

// Adds a cylinder shape to the supplied vertex buffer and index buffer, according to spesifications in the shape attribute structure.
// Shape attributes available: Depth, Radius, Opaqueness, Color, Translation Matrix, Rotation Matrix
void OpenGL_Handler::addShape_Cylinder(std::vector<Vertex> &vertexBuffer, std::vector<GLuint> &indexBuffer, ShapeAttribs shape) {

	int indexOffset = vertexBuffer.size();

	std::vector<Vertex> vb_FrontCircle, vb_BackCircle;
	std::vector<GLuint> ib_FrontCircle, ib_BackCircle;

	ShapeAttribs circleShape = shape;
	circleShape.depth = shape.depth / 2.0f;

	addShape_Circle(vertexBuffer, ib_FrontCircle, circleShape); // Adding the front-facing circle
	
	circleShape.depth *= -1.0f;
	addShape_Circle(vertexBuffer, ib_BackCircle, circleShape); // Adding the backfacing circle
		
	indexBuffer.insert(indexBuffer.end(), ib_FrontCircle.begin(), ib_FrontCircle.end());
	indexBuffer.insert(indexBuffer.end(), ib_BackCircle.begin(), ib_BackCircle.end());
	
	// Adding surfaces between the two circles
	for (int i = 0; i < ib_FrontCircle.size(); i+=3) {
		indexBuffer.push_back(ib_FrontCircle[1 + i]);
		indexBuffer.push_back(ib_BackCircle[1 + i]);
		indexBuffer.push_back(ib_FrontCircle[2 + i]);
		indexBuffer.push_back(ib_FrontCircle[2 + i]);
		indexBuffer.push_back(ib_BackCircle[1 + i]);
		indexBuffer.push_back(ib_BackCircle[2 + i]);
	}
}

// Adds a triangular prism shape to the supplied vertex buffer and index buffer, according to spesifications in the shape attribute structure.
// Shape attributes available: Width, Height, Depth, Opaqueness, Color, Translation Matrix, Rotation Matrix
void OpenGL_Handler::addShape_TriangularPrism(std::vector<Vertex> &vertexBuffer, std::vector<GLuint> &indexBuffer, ShapeAttribs shape) {
	int indexOffset = vertexBuffer.size();

	float halfWidth = shape.width / 2.0f;
	float halfHeight = shape.height / 2.0f;
	float halfDepth = shape.depth / 2.0f;
	float opaqueness = shape.opaqueness;
	glm::vec3 color = shape.color;

	// Front Plane
	vertexBuffer.push_back({ 0.0f, halfHeight, halfDepth, color.r, color.g, color.b, opaqueness, 0.0f, 0.0f }); // Top
	vertexBuffer.push_back({ halfWidth, -halfHeight, halfDepth, color.r, color.g, color.b, opaqueness, 1.0f, 1.0f });	// Bottom right
	vertexBuffer.push_back({ -halfWidth, -halfHeight, halfDepth, color.r, color.g, color.b, opaqueness, 0.0f, 1.0f }); // Bottom left
	// Back plane
	vertexBuffer.push_back({ 0.0f, halfHeight, -halfDepth, color.r, color.g, color.b, opaqueness, 0.0f, 0.0f }); // Top
	vertexBuffer.push_back({ halfWidth, -halfHeight, -halfDepth, color.r, color.g, color.b, opaqueness, 1.0f, 1.0f });	// Bottom right
	vertexBuffer.push_back({ -halfWidth, -halfHeight, -halfDepth, color.r, color.g, color.b, opaqueness, 0.0f, 1.0f }); // Bottom left

	for (std::vector<Vertex>::iterator v = vertexBuffer.begin(); v != vertexBuffer.end(); ++v)
	{
		Vertex vert = *v;
		glm::vec4 newPos = shape.translationMatrix * shape.rotationMatrix * glm::vec4(vert.X, vert.Y, vert.Z, 1.0f);
		vert.X = newPos.x;
		vert.Y = newPos.y;
		vert.Z = newPos.z;
		*v = vert;
	}

	// Front
	indexBuffer.push_back(0 + indexOffset);
	indexBuffer.push_back(1 + indexOffset);
	indexBuffer.push_back(2 + indexOffset);
	// Back
	indexBuffer.push_back(3 + indexOffset);
	indexBuffer.push_back(4 + indexOffset);
	indexBuffer.push_back(5 + indexOffset);

	// Left side
	indexBuffer.push_back(2 + indexOffset);
	indexBuffer.push_back(5 + indexOffset);
	indexBuffer.push_back(3 + indexOffset);
	indexBuffer.push_back(3 + indexOffset);
	indexBuffer.push_back(0 + indexOffset);
	indexBuffer.push_back(2 + indexOffset);
	 
	// Right Side
	indexBuffer.push_back(1 + indexOffset);
	indexBuffer.push_back(0 + indexOffset);
	indexBuffer.push_back(3 + indexOffset);
	indexBuffer.push_back(3 + indexOffset);
	indexBuffer.push_back(4 + indexOffset);
	indexBuffer.push_back(1 + indexOffset);

	// Bottom
	indexBuffer.push_back(2 + indexOffset);
	indexBuffer.push_back(1 + indexOffset);
	indexBuffer.push_back(4 + indexOffset);
	indexBuffer.push_back(4 + indexOffset);
	indexBuffer.push_back(5 + indexOffset);
	indexBuffer.push_back(2 + indexOffset);
}

// Adds a 2D plane shape to the supplied vertex buffer and index buffer, according to spesifications in the shape attribute structure.
// Shape attributes available: Width, Height, Depth, Opaqueness, Color, ZoomTex
void OpenGL_Handler::addShape_Plane(std::vector<Vertex> &vertexBuffer, std::vector<GLuint> &indexBuffer, ShapeAttribs shape, bool mirrorTexture) {
	int indexOffset = vertexBuffer.size();

	float halfWidth = shape.width / 2.0f;
	float halfHeight = shape.height / 2.0f;
	float depth = shape.depth;
	float opaqueness = shape.opaqueness;
	glm::vec3 color = shape.color;
	if (!mirrorTexture) {
		vertexBuffer.push_back({ -halfWidth, halfHeight, depth, color.r, color.g, color.b, opaqueness, 0.0f + shape.zoomTex, 0.0f + shape.zoomTex });	// Top left - x, y, z, r, g, b, a, u, v
		vertexBuffer.push_back({ halfWidth, halfHeight, depth, color.r, color.g, color.b, opaqueness, 1.0f - shape.zoomTex, 0.0f + shape.zoomTex });	// Top right
		vertexBuffer.push_back({ halfWidth, -halfHeight, depth, color.r, color.g, color.b, opaqueness, 1.0f - shape.zoomTex, 1.0f - shape.zoomTex });	// Bottom right
		vertexBuffer.push_back({ -halfWidth, -halfHeight, depth, color.r, color.g, color.b, opaqueness, 0.0f + shape.zoomTex, 1.0f - shape.zoomTex }); // Bottom left
	}
	else {
		vertexBuffer.push_back({ -halfWidth, halfHeight, depth, color.r, color.g, color.b, opaqueness, 1.0f - shape.zoomTex, 0.0f + shape.zoomTex });	// Top left - x, y, z, r, g, b, a, u, v
		vertexBuffer.push_back({ halfWidth, halfHeight, depth, color.r, color.g, color.b, opaqueness, 0.0f + shape.zoomTex, 0.0f + shape.zoomTex });	// Top right
		vertexBuffer.push_back({ halfWidth, -halfHeight, depth, color.r, color.g, color.b, opaqueness, 0.0f + shape.zoomTex, 1.0f - shape.zoomTex });	// Bottom right
		vertexBuffer.push_back({ -halfWidth, -halfHeight, depth, color.r, color.g, color.b, opaqueness, 1.0f - shape.zoomTex, 1.0f - shape.zoomTex }); // Bottom left
	}
	indexBuffer.push_back(0 + indexOffset);
	indexBuffer.push_back(1 + indexOffset);
	indexBuffer.push_back(2 + indexOffset);
	indexBuffer.push_back(2 + indexOffset);
	indexBuffer.push_back(3 + indexOffset);
	indexBuffer.push_back(0 + indexOffset);

}

// Adds a circle shape to the supplied vertex buffer and index buffer, according to spesifications in the shape attribute structure.
// Shape attributes available: Depth, Radius, Opaqueness, Color, Translation Matrix, Rotation Matrix
void OpenGL_Handler::addShape_Circle(std::vector<Vertex> &vertexBuffer, std::vector<GLuint> &indexBuffer, ShapeAttribs shape) {
	
	glm::vec4 newPos;
	int indexOffset = vertexBuffer.size();

	float radius = shape.radius;
	float depth = shape.depth;
	float opaqueness = shape.opaqueness;
	glm::vec3 color = shape.color;

	Vertex centerVert = { 0.0f, 0.0f, depth, color.r, color.g, color.b, opaqueness, 0.5f, 0.5f };
	Vertex radiusVert = { radius, 0.0f, depth, color.r, color.g, color.b, opaqueness, 1.0f, 0.5f };

	vertexBuffer.push_back(centerVert);
	
	Vertex tmpVertex = radiusVert;
	for (float i = 0.0f; i <= 360.0f; i++)
	{
		float angleRad = glm::radians(i);
		newPos = glm::rotate(glm::mat4(), angleRad, glm::vec3(0.0f, 0.0f, 1.0f)) * glm::vec4(radiusVert.X, radiusVert.Y, radiusVert.Z, 1.0f);
		tmpVertex.X = newPos.x;
		tmpVertex.Y = newPos.y;
		tmpVertex.Z = newPos.z;
		tmpVertex.U = cos(angleRad) * 0.5f + 0.5f;
		tmpVertex.V = sin(angleRad) * -0.5f + 0.5f;
		
		vertexBuffer.push_back(tmpVertex);
	}

	for (int i = 0; i < 360; i++)
	{
		indexBuffer.push_back(0 + indexOffset);
		indexBuffer.push_back(1 + i + indexOffset);
		indexBuffer.push_back(2 + i + indexOffset);
	}
}

// Adds the surface models which display the webcamera images to the "3DModels" model list.
std::vector<Model> OpenGL_Handler::addSurfaceModels(bool RiftIsPresent, cv::Mat &CamPicture_Left, cv::Mat &CamPicture_Right) {

	std::vector<Model> mdlList;

	std::vector<Vertex> surfaceVertices;
	std::vector<GLuint> surfaceIndices;

	glm::mat4 modelMatrix;
	{
		ShapeAttribs shape;
		shape.width = 62.5f; // Maintaining aspect ratio of 1280x1024 images
		shape.height = 50.0f;
		shape.opaqueness = 1.0f;
		shape.zoomTex = 0.0;
		addShape_Plane(surfaceVertices, surfaceIndices, shape);
	}

	if (!RiftIsPresent) {
		modelMatrix = glm::translate(glm::mat4(), glm::vec3(-31.75f, 0.0f, 0.0f));
	}
	else {
		//modelMatrix = glm::translate(glm::mat4(), glm::vec3(ovr.EyeRenderDescription[ovrEye_Left].HmdToEyeViewOffset.x, 0.0f, 0.0f));
		modelMatrix = glm::mat4();
	}
	
	mdlList.push_back(Model(surfaceVertices, surfaceIndices, modelMatrix, CamPicture_Left, "mdlLeftSurface"));

	// Using the same plane for both eyes / models
	if (!RiftIsPresent) {
		modelMatrix = glm::translate(glm::mat4(), glm::vec3(31.75f, 0.0f, 0.0f));
	}
	else {
		//modelMatrix = glm::translate(glm::mat4(), glm::vec3(ovr.EyeRenderDescription[ovrEye_Right].HmdToEyeViewOffset.x, 0.0f, 0.0f));
		modelMatrix = glm::mat4();
	}

	mdlList.push_back(Model(surfaceVertices, surfaceIndices, modelMatrix, CamPicture_Right, "mdlRightSurface"));

	return mdlList;
}

// Adds a robot arm model to the supplied model list, where each joint angle is determined by supplied parameters.
/* Parameters:
	vector<Model> *mdlList - The model list where the model should reside
	string name - The name of the model
	float _Angle - The angles for each joint of the robot model
	float fingersDistance - The claw opening of the robot model
	float opaqueness - The translucency of the model. 0 - Invisible, 1 - Opaque
	glm::mat4 modelmatrix - Homogenouos transformation matrix describing any translation / rotation to be done to the final model
	cv::Mat tex - Texture to be used on the model
*/
void OpenGL_Handler::addRobotModel(std::vector<Model> *mdlList, std::string name, float baseAngle, float shoulderAngle, float elbowAngle, float wristRollAngle, float wristPitchAngle, float fingersDistance, 
	float opaqueness, glm::mat4 modelMatrix, cv::Mat tex) {

	Model *mdl = findModel(*mdlList, name);
	if (opaqueness == 0.0f && mdl != NULL) {
		mdl->setVisible(false);
		return;
	}

	std::vector<Vertex> vb_RobotModel;
	std::vector<GLuint> ib_RobotModel;
	{
		ShapeAttribs shape;
		shape.width = 65.0f;
		shape.height = 20.0f;
		shape.depth = 25.0f;
		shape.opaqueness = opaqueness;

		shape.translationMatrix = glm::translate(glm::mat4(), glm::vec3(0.0f, 0.0f, fingersDistance + 25.0f));
		addShape_Cube(vb_RobotModel, ib_RobotModel, shape); // Adding "left" finger

		shape.translationMatrix = glm::translate(glm::mat4(), glm::vec3(-75.0f, 0.0f, -(fingersDistance + 25.0f) / 2.0f));
		addShape_Cube(vb_RobotModel, ib_RobotModel, shape); // Adding "right" finger
	}
	{
		glm::mat4 rotationMatrix = glm::rotate(glm::mat4(), wristPitchAngle, glm::vec3(0.0f, 0.0f, -1.0f))
			* glm::rotate(glm::mat4(), wristRollAngle, glm::vec3(-1.0f, 0.0f, 0.0f));

		ShapeAttribs shape;
		shape.width = 85.0f;
		shape.height = 40.0f;
		shape.depth = 120.0f;
		shape.opaqueness = opaqueness;
		shape.translationMatrix = glm::translate(glm::mat4(), glm::vec3(-110.5f, 0.0f, 0.0f));
		shape.rotationMatrix = glm::translate(rotationMatrix, glm::vec3(-45.0f, 0.0f, 0.0f)); // Translating along its rotated axis

		addShape_Cube(vb_RobotModel, ib_RobotModel, shape); // Adding wrist
	}
	{
		ShapeAttribs shape;
		shape.width = 221.0f;
		shape.height = 65.0f;
		shape.depth = 130.0f;
		shape.opaqueness = opaqueness;
		shape.translationMatrix = glm::translate(glm::mat4(1.0f), glm::vec3(-110.5f, 0.0f, 0.0f));
		shape.rotationMatrix = glm::translate(glm::rotate(glm::mat4(), elbowAngle, glm::vec3(0.0f, 0.0f, -1.0f)), glm::vec3(-110.5f, 0.0f, 0.0f)); // Rotating and translating along its rotated axis


		addShape_Cube(vb_RobotModel, ib_RobotModel, shape); // Adding forearm	
	}
	{
		ShapeAttribs shape;
		shape.width = 221.0f;
		shape.height = 70.0f;
		shape.depth = 149.0f;
		shape.opaqueness = opaqueness;
		shape.translationMatrix = glm::translate(glm::mat4(1.0f), glm::vec3(-16.0f, 174.5f, 0.0f));
		shape.rotationMatrix = glm::translate(glm::rotate(glm::mat4(), shoulderAngle, glm::vec3(0.0f, 0.0f, -1.0f)), glm::vec3(-110.5f, 0.0f, 0.0f)); // Rotating and translating along its rotated axis

		addShape_Cube(vb_RobotModel, ib_RobotModel, shape); // Adding shoulder
	}
	// Rendering of the robots base
	//{
	//	ShapeAttribs shape;
	//	shape.width = 135.0f;
	//	shape.height = 349.0f;
	//	shape.depth = 135.0f;
	//	shape.opaqueness = opaqueness;
	//	shape.rotationMatrix = glm::rotate(glm::mat4(), baseAngle, glm::vec3(0.0f, 1.0f, 0.0f));


	//	addShape_Cube(vb_RobotModel, ib_RobotModel, shape); // Adding base
	//}

	for (std::vector<Vertex>::iterator v = vb_RobotModel.begin(); v != vb_RobotModel.end(); ++v)
	{
		Vertex vert = *v;
		glm::vec4 newPos = glm::rotate(glm::mat4(), baseAngle, glm::vec3(0.0f, -1.0f, 0.0f)) * glm::vec4(vert.X, vert.Y, vert.Z, 1.0f);
		vert.X = newPos.x;
		vert.Y = newPos.y;
		vert.Z = newPos.z;
		*v = vert;
	}
	
	if (mdl != NULL) {
		mdl->updateVertices(vb_RobotModel);
		mdl->setVisible(true);
	}
	else {
		mdlList->push_back(Model(vb_RobotModel, ib_RobotModel, modelMatrix, tex, name, opaqueness != 0.0f));
	}
}

// Adds a battery model to the supplied model list, at the location / pose determined by the model matrix.
/* Parameters:
	vector<Model> *mdlList - The model list where the model should reside
	string name - The name of the model
	float capacity - The remaining battery power (scaled between 0 - 1)
	glm::mat4 modelmatrix - Homogenouos transformation matrix describing any translation / rotation to be done to the final model
	cv::Mat tex - Texture to be used on the model
*/
void OpenGL_Handler::addBatteryModel(std::vector<Model> *mdlList, std::string name, float capacity, glm::mat4 modelMatrix, cv::Mat tex) {

	std::vector<Vertex> vb_Battery;
	std::vector<GLuint> ib_Battery;

	float _Capacity = capacity;

	glm::vec3 capIndColor;
	if (_Capacity >= 0.7f) {
		capIndColor = glm::vec3(0.0f, 1.0f, 0.0f);
	}
	else if (_Capacity < 0.7f && _Capacity > 0.4f) {
		capIndColor = glm::vec3(1.0f, 0.4f, 0.0f);
	}
	else {
		capIndColor = glm::vec3(1.0f, 0.0f, 0.0f);
		if (_Capacity < 0.0f) _Capacity = 0.0f;
	}
	

	{
		ShapeAttribs shape;
		shape.width = 1.0f;
		shape.height = 3.0f;
		shape.depth = 2.0f;
		shape.color = glm::vec3{ 0.7f, 0.7f, 0.7f };
		shape.translationMatrix = glm::translate(glm::mat4(1.0f), glm::vec3(5.5f, 0.0f, 0.0f));

		addShape_Cube(vb_Battery, ib_Battery, shape);
	}

	{
		ShapeAttribs shape;
		shape.width = 10.0f;
		shape.height = 5.0f;
		shape.depth = 2.0f;
		shape.color = glm::vec3{ 0.7f, 0.7f, 0.7f };
		shape.translationMatrix = glm::translate(glm::mat4(1.0f), glm::vec3(0.0f, 0.0f, -1.5f));

		addShape_Cube(vb_Battery, ib_Battery, shape);
	}
	{
		std::vector<Vertex> vb_dynamicBlock;
		std::vector<GLuint> ib_dynamicBlock;
		ShapeAttribs shape;
		shape.width = 8.0f * _Capacity;
		shape.height = 4.0f;
		shape.depth = 1.0f;
		shape.color = capIndColor;
		shape.translationMatrix = glm::translate(glm::mat4(), glm::vec3(-4.0f * (1.0f - _Capacity), 0.0f, 0.0f));

		addShape_Cube(vb_dynamicBlock, ib_dynamicBlock, shape);

		// In order to just manipulate the one block of the model, it must be created and translated in its own vectors
		int ib_offset = vb_Battery.size();
		vb_Battery.insert(vb_Battery.end(), vb_dynamicBlock.begin(), vb_dynamicBlock.end());

		for (int i = 0; i < ib_dynamicBlock.size(); i++)
		{
			ib_dynamicBlock[i] += ib_offset;
		}
		ib_Battery.insert(ib_Battery.end(), ib_dynamicBlock.begin(), ib_dynamicBlock.end());
	}
	
	Model *mdl = findModel(*mdlList, name);
	if (mdl != NULL) {
		mdl->updateVertices(vb_Battery);
	}
	else {
		mdlList->push_back(Model(vb_Battery, ib_Battery, modelMatrix, tex, name));
	}
	
}

// Adds an arrow model to the supplied model list, at the location / pose determined by the model matrix.
/* Parameters:
	vector<Model> *mdlList - The model list where the model should reside
	string name - The name of the model
	float angle - The angle in which to point the arrow (in radians)
	glm::mat4 modelmatrix - Homogenouos transformation matrix describing any translation / rotation to be done to the final model
	cv::Mat tex - Texture to be used on the model
*/
void OpenGL_Handler::addArrowModel(std::vector<Model> *mdlList, std::string name, float angle, glm::mat4 modelMatrix, cv::Mat tex) {

	std::vector<Vertex> vb_Arrow;
	std::vector<GLuint> ib_Arrow;

	{
		ShapeAttribs shape;
		shape.width = 3.0f;
		shape.height = 3.0f;
		shape.depth = 1.0f;
		shape.translationMatrix = glm::translate(glm::mat4(), glm::vec3(0.0f, 5.0f, 0.0f));
		addShape_TriangularPrism(vb_Arrow, ib_Arrow, shape);
	}

	{
		ShapeAttribs shape;
		shape.width = 2.0f;
		shape.height = 7.0f;
		shape.depth = 1.0f;
		shape.rotationMatrix = glm::rotate(glm::mat4(), angle, glm::vec3(0.0f, 0.0f, 1.0f)) * glm::translate(glm::mat4(), glm::vec3(0.0f, -1.5f, 0.0f)); // Translating to center before rotation
		addShape_Cube(vb_Arrow, ib_Arrow, shape);
	}

	Model *mdl = findModel(*mdlList, name);
	if (mdl != NULL) {
		mdl->updateVertices(vb_Arrow);
		if (modelMatrix != glm::mat4()) mdl->modelMatrix = modelMatrix;
	}
	
	else {
		mdlList->push_back(Model(vb_Arrow, ib_Arrow, modelMatrix, tex, name));
	}
}

// Adds a model containing an hour and minute arrow to the supplied model list, at the location / pose determined by the model matrix.
/* Parameters:
vector<Model> *mdlList - The model list where the model should reside
string name - The name of the model
tm time - Time struct containing desired hour and minute representation
glm::mat4 modelmatrix - Homogenouos transformation matrix describing any translation / rotation to be done to the final model
cv::Mat tex - Texture to be used on the model
*/
void OpenGL_Handler::addClockModel(std::vector<Model> *mdlList, std::string name, tm time, glm::mat4 modelMatrix, cv::Mat tex) {

	std::vector<Vertex> vb_Clock;
	std::vector<GLuint> ib_Clock;

	{	// Hour counter
		ShapeAttribs shape;
		shape.width = 1.0f;
		shape.height = 5.0f;
		shape.depth = 1.0f;
		shape.color = glm::vec3(0.0f, 0.0f, 0.0f);
		shape.translationMatrix = glm::translate(glm::mat4(), glm::vec3(0.0f, -3.5f, 0.0f));
		addShape_Cube(vb_Clock, ib_Clock, shape);
	}
	{
		ShapeAttribs shape;
		shape.width = 1.0f;
		shape.height = 2.0f;
		shape.depth = 1.0f;
		shape.color = glm::vec3(0.0f, 0.0f, 0.0f); // Formula: glm::radians((hour * 360 / 12) + (minutes / 60 * (360 / 12))) := 360 / 12 = 30 and 30/60 = 0.5
		shape.rotationMatrix = glm::rotate(glm::mat4(), glm::radians((float)(time.tm_hour * 30) + ((float)(time.tm_min) * 0.5f)), glm::vec3(0.0f, 0.0f, -1.0f)) *glm::translate(glm::mat4(), glm::vec3(0.0f, 6.0f, 0.0f));
		addShape_TriangularPrism(vb_Clock, ib_Clock, shape);
		
	}

	{	// Minute counter
		std::vector<Vertex> vb_clkMinute;
		std::vector<GLuint> ib_clkMinute;
		ShapeAttribs shape;
		shape.width = 1.0f;
		shape.height = 8.0f;
		shape.depth = 1.0f;
		shape.color = glm::vec3(0.0f, 0.0f, 0.0f);
		shape.translationMatrix = glm::translate(glm::mat4(), glm::vec3(0.0f, -5.0f, 0.0f));
		addShape_Cube(vb_clkMinute, ib_clkMinute, shape);
	
		shape.width = 1.0f;
		shape.height = 2.0f;
		shape.depth = 1.0f;
		shape.translationMatrix = glm::mat4();
		shape.rotationMatrix = glm::rotate(glm::mat4(), glm::radians((float)(time.tm_min * 6)), glm::vec3(0.0f, 0.0f, -1.0f)) * glm::translate(glm::mat4(), glm::vec3(0.0f, 9.0f, 0.0f));
		addShape_TriangularPrism(vb_clkMinute, ib_clkMinute, shape);

		int ib_offset = vb_Clock.size();

		for (int i = 0; i < ib_clkMinute.size(); i++)
		{
			ib_clkMinute[i] += ib_offset;
		}

		vb_Clock.insert(vb_Clock.end(), vb_clkMinute.begin(), vb_clkMinute.end());
		ib_Clock.insert(ib_Clock.end(), ib_clkMinute.begin(), ib_clkMinute.end());
	}	

	Model *mdl = findModel(*mdlList, name);
	if (mdl != NULL) {
		mdl->updateVertices(vb_Clock);
		if (modelMatrix != glm::mat4()) mdl->modelMatrix = modelMatrix;
	}

	else {
		mdlList->push_back(Model(vb_Clock, ib_Clock, modelMatrix, tex, name));
	}
}

// Adds a speedometer model and speed indicating text to the supplied model collection, at the location / pose determined by the model matrix.
/* Parameters:
vector<ModelCollection> &modelCollection - The model collection to which the speedometer should be added
string name - The name of the 3D model
float speed - The speed to be displayed by text
float angle - The angle in which to point the direction arrow (in degrees)
float rotation - Whether the robot is rotating or not. 0: No rotation, 1: clockwise rotation, -1: Counter clockwise rotation
glm::mat4 modelmatrix - Homogenouos transformation matrix describing any translation / rotation to be done to the final model
cv::Mat tex - Texture to be used on the model
*/
void OpenGL_Handler::addSpeedoMeter(std::vector<ModelCollection> &modelCollection, std::string name, float speed, float angle, float rotation, glm::mat4 modelMatrix, cv::Mat tex) {
	
	std::vector<Vertex> vb_speedArrow;
	std::vector<GLuint> ib_speedArrow;

	
	glm::vec4 txtPosition = modelMatrix * glm::vec4(posCenter.x, posCenter.y - 2.0f, posCenter.z, 1.0f);
	addTextModels(modelCollection, std::to_string(((int)speed) / 10), 0.05f, glm::vec3(txtPosition.x, txtPosition.y, txtPosition.z), glm::vec3(0.0f, 0.0f, 0.0f), "s_" + name, false, 90.0f);

	if (rotation == 0)
	{
		ShapeAttribs shape;
		shape.width = 1.0f;
		shape.height = 6.0f;
		shape.depth = 1.0f;
		shape.color = glm::vec3(0.0f, 0.0f, 1.0f);
		shape.translationMatrix = glm::translate(glm::mat4(), glm::vec3(0.0f, -4.0f, 0.0f));
		addShape_Cube(vb_speedArrow, ib_speedArrow, shape);

		shape.width = 1.0f;
		shape.height = 2.0f;
		shape.depth = 1.0f;
		shape.translationMatrix = glm::mat4();
		shape.rotationMatrix = glm::rotate(glm::mat4(), glm::radians(angle), glm::vec3(0.0f, 0.0f, -1.0f)) * glm::translate(glm::mat4(), glm::vec3(0.0f, 10.0f, 0.0f));
		addShape_TriangularPrism(vb_speedArrow, ib_speedArrow, shape);

		std::vector<Model> *mdlList = findModelList(modelCollection, "3DModels");
		if (mdlList == NULL) {
			return;
		}
		else {
			Model *mdl = findModel(*mdlList, name);
			if (mdl == NULL) {
				mdlList->push_back(Model(vb_speedArrow, ib_speedArrow, modelMatrix, tex, name));
			}
			else {
				mdl->updateVertices(vb_speedArrow);
				mdl->setVisible(true);
			}
			Model *mdlBackgr = findModel(*mdlList, "mdlSpeedometerBkg");
			mdlBackgr->updateTexture(texSpeedo);
		}
	}
	else if (rotation == -1) {

		std::vector<Model> *mdlList = findModelList(modelCollection, "3DModels");
		if (mdlList == NULL) {
			return;
		} else {
			Model *mdl = findModel(*mdlList, "mdlSpeedometerBkg");
				mdl->updateTexture(texSpeedoCCW);
			Model *mdlArrow = findModel(*mdlList, name);
			mdlArrow->setVisible(false);
		}
	}
	else if (rotation == 1) {

		std::vector<Model> *mdlList = findModelList(modelCollection, "3DModels");
		if (mdlList == NULL) {
			return;
		}
		else {
			Model *mdl = findModel(*mdlList, "mdlSpeedometerBkg");
			mdl->updateTexture(texSpeedoCW);
			Model *mdlArrow = findModel(*mdlList, name);
			mdlArrow->setVisible(false);
		}
	}
}

// Adds a model list containing all characters (needed to write the specified string) as individual models, and adds / updates the model collection with the new model list.
/* Parameters:
vector<ModelCollection> &modelCollection - Address of the model collection to which the model list should be added.
string text - The string to be rendered
float scale - Scale of the text
glm::vec3 textPosition - An [X, Y, Z] vector describing the position of the text.
						If the text is located in the right half of the screen it is aligned to the right, if it is in the center of the screen it is centered around this point.
glm::vec3 color - The color of the text string
string name - The name of the model list containing the string
bool background - Whether the string should be rendered on a black background or not
float xRotation - The angle each character should be rotated around the x axis of the screen (in degrees).
*/
void OpenGL_Handler::addTextModels(std::vector<ModelCollection> &modelCollection, std::string text, float scale, glm::vec3 textPosition, glm::vec3 color, std::string name, bool background, float xRotation) {
	glm::vec3 position;

	position.x = textPosition.x;
	position.y = textPosition.y;
	position.z = textPosition.z;

	std::vector<Model> charList;
	std::string::iterator c;
	enum Alignment
	{
		LEFT,
		CENTER,
		RIGHT
	};
	int txtAlign = LEFT;

	if (position.x > 0.0f) txtAlign = RIGHT;
	else if (position.x == 0.0f) txtAlign = CENTER;
	else txtAlign = LEFT;

	glm::mat4 modelMatrix = glm::mat4();
	bool extended = false;
	int listSize = 0;

	std::vector<Model> *mdlList = findModelList(modelCollection, name);
	if (!(mdlList == NULL)) listSize = mdlList->size();

	Character ch;
	float cursor = 0.0f;
	int i = 0;
	for (c = text.begin(); c != text.end(); c++) {
		std::vector<Vertex> vbo;
		std::vector<GLuint> ibo;

		ch = Characters[*c];

		float xPosOffset = (float)ch.Bearing.x * scale;
		float yPosOffset = ((float)ch.Bearing.y - (float)ch.Size.y) * scale;

		float Width = ch.Size.x * scale;
		float Height = ch.Size.y * scale;

		modelMatrix = glm::translate(glm::mat4(), glm::vec3(cursor + xPosOffset, yPosOffset, 0.0f));

		vbo.push_back({ 0.0f, 0.0f, 0.0f, color.x, color.y, color.z, 0.8f, 0.0f, 1.0f }); // Bottom left
		vbo.push_back({ Width, 0.0f, 0.0f, color.x, color.y, color.z, 0.8f, 1.0f, 1.0f });	// Bottom right
		vbo.push_back({ Width, Height, 0.0f, color.x, color.y, color.z, 0.8f, 1.0f, 0.0f });	// Top right
		vbo.push_back({ 0.0f, Height, 0.0f, color.x, color.y, color.z, 0.8f, 0.0f, 0.0f });	// Top left - x, y, z, r, g, b, a, u, v

		ibo.push_back(0);
		ibo.push_back(1);
		ibo.push_back(2);
		ibo.push_back(2);
		ibo.push_back(3);
		ibo.push_back(0);
		if (listSize > 0) { // There is already text in the spesified area (with the same name)
			if (mdlList->size() > i) {
				mdlList->at(i).updateVertices(vbo);
				mdlList->at(i).updateTexture(ch.TextureID);
				mdlList->at(i).modelMatrix = modelMatrix;
				mdlList->at(i).sName = "s" + text + "_Char_" + std::to_string(i);
			}
			else { // The new text is larger than what is already there
				mdlList->push_back(Model(vbo, ibo, modelMatrix, ch.TextureID, "s" + text + "_Char_" + std::to_string(i)));
				extended = true;
			}
		}
		else { // There exists no text at the spesific location name
			charList.push_back(Model(vbo, ibo, modelMatrix, ch.TextureID, "s" + text + "_Char_" + std::to_string(i)));
		}
		
		cursor += ((float)(ch.Advance >> 6)) * scale;
		i++;
	}

	if (!extended && (listSize > 0)) { // The new text is smaller than what was already there
		
		while ((listSize = mdlList->size()) > i){
			mdlList->at(listSize - 1).freeModel();
			mdlList->pop_back();
		}
	}

	glm::vec3 txtPos;
	if (txtAlign == CENTER) txtPos = { position.x - (cursor / 2.0f), position.y, position.z };
	else if (txtAlign == RIGHT) txtPos = { position.x - cursor, position.y, position.z }; // Aligning text to the right, in the right half of the screen.
	else txtPos = position;

	if (background) {
		std::vector<Vertex> bkgVertices;
		std::vector<GLuint> bkgIndices;

		ShapeAttribs shape;
		shape.width = cursor;
		shape.height = 3.5f;
		shape.depth = -0.01f;
		shape.color = glm::vec3(0.0f, 0.0f, 0.0f);
		glm::mat4 backgroundMatrix = glm::translate(glm::mat4(), glm::vec3(shape.width / 2.0f, 1.0f, 0.0f));

		addShape_Plane(bkgVertices, bkgIndices, shape);

		if (listSize > 0) {
			Model *bkgModel = NULL;
			bkgModel = findModel(*mdlList, "background");
			if (bkgModel != NULL) bkgModel->updateVertices(bkgVertices);
			else {
				mdlList->push_back(Model(bkgVertices, bkgIndices, backgroundMatrix, cv::Mat(), "background"));
				std::iter_swap(mdlList->begin(), mdlList->rbegin()); // Making sure the background is drawn first (is first in queue), to avoid depth-testing issues with alpha blended characters
			}
		}
		else {
			charList.push_back(Model(bkgVertices, bkgIndices, backgroundMatrix, cv::Mat(), "background"));
			std::iter_swap(charList.begin(), charList.rbegin()); // Making sure the background is drawn first (is first in queue)
		}
	}

	if (listSize > 0) {
		for (std::vector < Model >::iterator mdl = mdlList->begin(); mdl != mdlList->end(); ++mdl)
		{
			mdl->modelMatrix *= glm::translate(glm::mat4(), txtPos) *glm::rotate(glm::mat4(), glm::radians(xRotation), glm::vec3(-1.0f, 0.0f, 0.0f));
		}
	}
	else {
		for (std::vector < Model >::iterator i = charList.begin(); i != charList.end(); ++i)
		{
			i->modelMatrix *= glm::translate(glm::mat4(), txtPos) *glm::rotate(glm::mat4(), glm::radians(xRotation), glm::vec3(-1.0f, 0.0f, 0.0f));
		}
		modelCollection.push_back(ModelCollection(charList, name));
	}
}