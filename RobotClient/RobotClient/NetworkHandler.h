#ifndef NETWORKHANDLER_H
#define NETWORKHANDLER_H

#include <winsock2.h>
#include <Ws2tcpip.h>
#include <stdio.h>
#include <string>
#include <thread>
#include <mutex>
#include <vector>
#include <bitset>
#include <opencv2\core.hpp>
#include "UtilityFunctions.h"

using namespace std;

enum MessageType {
	INCL_ROBOT,
	INCL_PTU,
	INCL_DRIVER
};

enum SetPointData
{
	ROBOT_BASE,
	ROBOT_SHOULDER,
	ROBOT_ELBOW,
	ROBOT_ROLL,
	ROBOT_PITCH,
	ROBOT_JAW,
	PTU_P,
	PTU_T,
	DRIVER_ANGLE,
	DRIVER_SPEED,
	DRIVER_ROTATION
};

typedef float RemoteSetPoints[11];

class NetworkHandler {
private:
	thread th_dataReceiver;
	bool running = true;
	
	mutex lock_Battery;
	bool newVal_Batt = false;
	float recData = 255.0f;

	mutex lock_Kinect;
	bool newVal_Kinect = false;
	std::vector<Vertex> KinectVB;

	mutex lock_KinectColor;
	bool newVal_KinectColor = false;
	unsigned char* KinectColors;
	
	void dataReceiverLoop();
	void setBatteryLevel(unsigned char value);
	void setKinectData(std::vector<Vertex> &vertexBuffer);

public:
	int init(const char* IP, unsigned short sendPort = 2222, unsigned short receivePort = 2223);
	bool sendSetPoints(RemoteSetPoints SP, char cmdRobot, bool activePTU, bool activeDriver, char requestKinect);
	
	
	bool getBatteryLevel(float &val);
	bool getKinectData(std::vector<Vertex> &vertexBuffer);
	bool getKinectColor(cv::Mat &img);
	int shutDown();
};
#endif