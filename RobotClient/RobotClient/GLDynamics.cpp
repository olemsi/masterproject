#include "OculusGL_Interface.h"

// Updates the rendered robot model based on supplied joint angles. Opaqueness determines the transparency of the model.
void OpenGL_Handler::updateRobotModel(float baseAngle, float shoulderAngle, float elbowAngle, float wristRollAngle, float wristPitchAngle, float fingersDistance, float opaqueness) {
	
	std::vector<Model> *mdlList = findModelList(modelCollection, "3DModels");;
	
	addRobotModel(mdlList, "mdlRobot", glm::radians(baseAngle), -glm::radians(shoulderAngle), -glm::radians(elbowAngle), glm::radians(wristRollAngle), glm::radians(wristPitchAngle), fingersDistance, opaqueness);

}

// Updates the rendered compass model, rotating the needle as determined by the supplied angle.
void OpenGL_Handler::updateCompassModel(float angle) {

	std::vector<Model> *mdlList = findModelList(modelCollection, "3DModels");
	addArrowModel(mdlList, "mdlCompassArrow", angle);
}

// Updates the driving info model based on supplied parameters.
void OpenGL_Handler::updateSpeedArrow(float speed, float angle, float rotation) {

	glm::mat4 modelMatrix = glm::translate(glm::mat4(), glm::vec3(0.0, -30.0f, 10.0f)) * glm::rotate(glm::mat4(), glm::radians(90.0f), glm::vec3(-1.0f, 0.0f, 0.0f));;
	addSpeedoMeter(modelCollection, "mdlSpeedArrow", speed, angle, rotation, modelMatrix);

}

// Updates the battery level indicator
void OpenGL_Handler::updateBatteryLevel(float capacity) {

	std::vector<Model> *mdlList = findModelList(modelCollection, "3DModels");
	addBatteryModel(mdlList, "mdlBattery", capacity);
}

// Updates existing, or adds new, text strings to be rendered.
void OpenGL_Handler::updateTextModels(std::string name, std::string text, glm::vec3 textPosition, glm::vec3 color) {

	std::string txt = text;

	if (txt == "") txt = " ";
	addTextModels(modelCollection, txt, textScale, textPosition, color, name, true);
}

// Updates the clock model to display the current system time
void OpenGL_Handler::updateClockModel() {

	time_t rawTime = time(0);
	tm timeInfo;
	localtime_s(&timeInfo, &rawTime);
	std::vector<Model> *mdlList = findModelList(modelCollection, "3DModels");
	addClockModel(mdlList, "mdlClockPointers", timeInfo);

}

// Updates the Kinect point cloud model using the supplied vertex buffer and eventual rotation angle
void OpenGL_Handler::updateKinectModel(std::vector<Vertex> vertices, float angle) {

	std::vector<Model> *mdlList = findModelList(modelCollection, "Kinect");
	
	glm::mat4 modelMatrix = glm::translate(glm::mat4(), glm::vec3(0.00f, 0.0f, 55.00f)) * glm::rotate(glm::mat4(), glm::radians(angle), glm::vec3(0.0f, 1.0f, 0.0f));
	// Da minimumsdistansen til Kinect ser ut for � v�re 80 cm, trekker jeg den her 65 cm n�rmere roboten. (Roboten st�r 0.5 meter inn i bildet, kinect modelleres fra 15cm bak kamera).
	if (!(mdlList == NULL)) {
		Model *mdl = findModel(*mdlList, "mdlKinect");
		if (mdl != NULL) {
			mdl->updateVertices(vertices);
			mdl->modelMatrix = modelMatrix;
		} else{
			mdlList->push_back(Model(vertices, modelMatrix, "mdlKinect"));
		}
	}
	else {
		std::vector<Model> newMdls;
		newMdls.push_back(Model(vertices, modelMatrix, "mdlKinect"));
		modelCollection.push_back(ModelCollection(newMdls, "Kinect"));
	}
}

// Updates a camera image model. The parameters decide which eye texture to update, with what image, if the images are to be zoomed, or if the image are to be hidden.
void OpenGL_Handler::updateCameraTexture(int eye, cv::Mat CamPicture, bool zoom, bool hideCameras) {

	std::vector<Model> *mdlList = findModelList(modelCollection, "Surfaces");

	std::vector<Vertex> surfaceVertices;
	std::vector<GLuint> surfaceIndices;
	if (!hideCameras)
	{
		ShapeAttribs shape;
		shape.width = 66.0f;
		shape.height = 50.0f;
		if (zoom) shape.zoomTex = 0.25f;
		else shape.zoomTex = 0.0f;
		addShape_Plane(surfaceVertices, surfaceIndices, shape);
	}

	Model *leftMdl, *rightMdl;
	if (!(mdlList == NULL)) {
		leftMdl = findModel(*mdlList, "mdlLeftSurface");
		if (leftMdl != NULL) {
			if (hideCameras) leftMdl->setVisible(false);
			else {
				leftMdl->setVisible(true);
				leftMdl->updateVertices(surfaceVertices);
				if (eye == ovrEye_Left) {
					leftMdl->updateTexture(CamPicture);
				}
			}
		}
	}
		
	if (!(mdlList == NULL)) {
		rightMdl = findModel(*mdlList, "mdlRightSurface");
		if (rightMdl != NULL) {
			if (hideCameras) rightMdl->setVisible(false);
			else {
				rightMdl->setVisible(true);
				rightMdl->updateVertices(surfaceVertices);
				if (eye == ovrEye_Right) {
					rightMdl->updateTexture(CamPicture);
				}
			}
			
		}
	}
}

// Updates existing, or adds new, model on which to display the supplied kinect RGB image.
void OpenGL_Handler::updateKinectTexture(cv::Mat img, bool zoom, bool hideTexture) {

	std::vector<Model> *mdlList = findModelList(modelCollection, "Surfaces");

	std::vector<Vertex> surfaceVertices;
	std::vector<GLuint> surfaceIndices;
	
	{
		ShapeAttribs shape;
		shape.width = 66.0f;
		shape.height = 50.0f;
		shape.depth = 0.0f;
		if (!hideTexture) shape.opaqueness = 1.0f;
		else shape.opaqueness = 0.0f;
		if (zoom) shape.zoomTex = 0.25f;
		else shape.zoomTex = 0.0f;
		addShape_Plane(surfaceVertices, surfaceIndices, shape, true);
	}

	Model *mdl;
	if (!(mdlList == NULL)) {
		mdl = findModel(*mdlList, "mdlKinectSurface");
		if (mdl != NULL) {
			mdl->updateVertices(surfaceVertices);
			mdl->updateTexture(img);
		}
		else {
			mdlList->push_back(Model(surfaceVertices, surfaceIndices, glm::mat4(), img, "mdlKinectSurface"));
		}
	}
}