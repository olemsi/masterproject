#ifndef UTILITYFUNCTIONS_H
#define UTILITYFUNCTIONS_H

#define KINECT_WIDTH 640
#define KINECT_HEIGHT 480

struct Vertex {
	float X, Y, Z, R, G, B, A, U, V;
};


/*
	Scales the value val from being between the range [minX, minY] to
	the new range [minY, maxY]
*/
float scaleValue(float val, float minX, float maxX, float minY, float maxY);
#endif