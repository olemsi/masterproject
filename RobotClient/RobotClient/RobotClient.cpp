// OculusTest_Project.cpp : Defines the entry point for the application.
// Ca 3700 lines of code total

#include <SDKDDKVer.h>
#define WIN32_LEAN_AND_MEAN
#include <Windows.h>
#include <Windowsx.h>
#include <mmsystem.h>
#include <stdlib.h>
#include <memory.h>
#include <tchar.h>
#include "resource.h"

#include <time.h>

#include "ConfigReader.h"
#include "NetworkHandler.h"
#include "WebcamHandler.h"
#include "OculusGL_Interface.h"
#include "UtilityFunctions.h"
#include "LeapMotion_Interface.h"

#define MAX_LOADSTRING 100

// Global Variables:
HINSTANCE hInst;								// current instance
TCHAR szTitle[MAX_LOADSTRING];					// The title bar text
TCHAR szWindowClass[MAX_LOADSTRING];			// the main window class name

ConfigReader cfg;
OculusRift_Interface OculusRift;
OpenGL_Handler oglHandler;
WebcamHandler camHandler_Left, camHandler_Right;
NetworkHandler netHandle;
LeapMotionController LeapMotion;

// Config Parameters
string IP_LeftCam;
string IP_RightCam;
string IP_Server;
string Port_LeftCam;
string Port_RightCam;
unsigned short Port_ServerSend;
unsigned short Port_ServerReceive;
string fontDir;


bool run = true;
bool RiftIsPresent = false;
bool LeapIsPresent = false;

int counterLeft = 0;
int counterRight = 0;
int fpsLeft = 0;
int fpsRight = 0;

char cmdRobot = 'N';
char kinectRequest = 'N'; // D = Depth & Color image, C = Color image only
bool trackHead = false;
bool trackDriver = false;
bool zoom = false;

float joy_DriveSpeed = 0.0f;
float joy_DriveAngle = 0.0f;
float joy_DriveRotation = 0.0f;

float robot_maxDriveSpeed = 0;
bool manipulator_recordMode = false;
bool manipulator_directMode = false;
bool hideCameras = false;

float mouseTestX = 0.0f;
float mouseTestY = 0.0f;
bool monoVision = false;
bool dummy = false;

HDC hDC;

windowSize winSize;

HGLRC context;

// Forward declarations of functions included in this code module:
ATOM				MyRegisterClass(HINSTANCE hInstance);
BOOL				InitInstance(HINSTANCE, int);
LRESULT CALLBACK	WndProc(HWND, UINT, WPARAM, LPARAM);
HGLRC				createFakeGLContext(HWND Window, HDC hDC);
HGLRC				createGLContext(HWND Window, HDC hDC);
int					initJoystick();
bool				getJointAngles(glm::vec4 xyzCoords, float &a_base, float &a_shoulder, float &a_elbow);
bool				compensateHeadMovement(glm::vec4 &leapVector, glm::vec4 &destVector, headAngle headRot);

int APIENTRY _tWinMain(_In_ HINSTANCE hInstance,
	_In_opt_ HINSTANCE hPrevInstance,
	_In_ LPTSTR    lpCmdLine,
	_In_ int       nCmdShow)
{
	UNREFERENCED_PARAMETER(hPrevInstance);
	UNREFERENCED_PARAMETER(lpCmdLine);

	MSG msg;


	// Initialize global strings
	LoadString(hInstance, IDS_APP_TITLE, szTitle, MAX_LOADSTRING); // Loading string from the String Table Resource
	LoadString(hInstance, IDC_ROBOTCLIENT, szWindowClass, MAX_LOADSTRING); // Loading File-menu from the Menu Resource
	MyRegisterClass(hInstance);

	cfg.Load("Config.cfg");
	if (!(cfg.Get("IP_LeftCam", IP_LeftCam) && cfg.Get("IP_RightCam", IP_RightCam) &&
		cfg.Get("Port_LeftCam", Port_LeftCam) && cfg.Get("Port_RightCam", Port_RightCam) &&
		cfg.Get("IP_Server", IP_Server) && cfg.Get("Port_ServerSend", Port_ServerSend) && cfg.Get("Port_ServerReceive", Port_ServerReceive) &&
		cfg.Get("Font", fontDir)))
	{
		printf("Missing parameter or file. Writing default");
		cfg.WriteDefault();

		return 1;
	}
	
	// Perform application initialization:
	
	if (camHandler_Left.startCapture("rtsp://" + IP_LeftCam + ":" + Port_LeftCam + "/video.h264") == 0) return 0;
	if (camHandler_Right.startCapture("rtsp://" + IP_RightCam + ":" + Port_RightCam + "/video.h264") == 0) return 0;
	
	
	if (netHandle.init(IP_Server.c_str(), Port_ServerSend, Port_ServerReceive) != 0)
	{
		return FALSE;
	}

	if (!InitInstance(hInstance, nCmdShow))
	{
		return FALSE;
	}
	
	cv::Mat img_Left, img_Right;
	// Making sure there are images available for buffer initialization
	while (!camHandler_Left.getResult(img_Left)) {
		std::this_thread::sleep_for(std::chrono::milliseconds(50));
	}
	while (!camHandler_Right.getResult(img_Right)) {
		std::this_thread::sleep_for(std::chrono::milliseconds(50));
	}

	if (OculusRift.initRift()) {
		RiftIsPresent = true;
	}
	if (!oglHandler.initRendering(RiftIsPresent, OculusRift, img_Left, img_Right, fontDir.c_str())) return 1;
	if (RiftIsPresent) {
		OculusRift.RecenterHeadPose();
	}
	if (LeapMotion.initLeap()) {
		LeapIsPresent = true;
	}
	else {
		oglHandler.updateTextModels("a_ManUnavail", "Manipulator unavailable", posAlarms, glm::vec3(1.0f, 0.0f, 0.0f));
		oglHandler.updateTextModels("a_NoLeap", "Missing Leap Motion", glm::vec3(posAlarms.x, posAlarms.y - 4.0f, posAlarms.z), glm::vec3(1.0f, 0.0f, 0.0f));
	}
	oglHandler.updateTextModels("armMode", "Arm: Disabled", posBotMid);
	oglHandler.updateTextModels("PTU_status", "PTU: Disabled", posBotLeft);
	cv::Mat croppedImg_Left, croppedImg_Right;
	headAngle camDest = { 0.0f, 0.0f, 0.0f };
	
	// Main message loop:
	bool handFound;
	char sentCmd = 'N';
	float battLevel = 0;
	cv::Mat kinectImg;
	char prevKinectRequest = 'N';

	while (run)
	{
		handFound = false;

		if (RiftIsPresent) camDest = OculusRift.getHeadAngle();

		RemoteSetPoints remSP = { 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f };
		
		oglHandler.updateRobotModel(remSP[ROBOT_BASE], remSP[ROBOT_SHOULDER], remSP[ROBOT_ELBOW], remSP[ROBOT_ROLL], remSP[ROBOT_PITCH], remSP[ROBOT_JAW], 0.0f);

		if (LeapIsPresent) {
			float X_leap, Y_leap, Z_leap, roll_leap, pitch_leap, jaw_leap;
			if (LeapMotion.getHandPositions(X_leap, Y_leap, Z_leap, roll_leap, pitch_leap, jaw_leap)) {
				handFound = true;
				
				glm::vec4 leapVector = { X_leap, Y_leap, Z_leap, 1.0f };
				glm::vec4 correctedVector;
				
				if (dummy) {
					compensateHeadMovement(leapVector, correctedVector, camDest);
					getJointAngles(correctedVector, remSP[ROBOT_BASE], remSP[ROBOT_SHOULDER], remSP[ROBOT_ELBOW]);
				}
				else {
					getJointAngles(leapVector, remSP[ROBOT_BASE], remSP[ROBOT_SHOULDER], remSP[ROBOT_ELBOW]);
				}
								
				remSP[ROBOT_ROLL] = 0.0f;
				remSP[ROBOT_PITCH] = pitch_leap;
				remSP[ROBOT_JAW] = jaw_leap;

				oglHandler.updateRobotModel(remSP[ROBOT_BASE], remSP[ROBOT_SHOULDER], remSP[ROBOT_ELBOW], remSP[ROBOT_ROLL], remSP[ROBOT_PITCH], remSP[ROBOT_JAW], 0.3f);

				oglHandler.updateTextModels("hand", "Hand Found", posTopLeft);
			}
			else {
				oglHandler.updateTextModels("hand", "Hand Gone", posTopLeft);
			}
			if (!handFound && manipulator_directMode) cmdRobot = 'N';
		}
		if (manipulator_recordMode) {
			
			std::vector<Vertex> tmpVB;
			if (netHandle.getKinectData(tmpVB))	oglHandler.updateKinectModel(tmpVB, 0.0f);
			
		}
		else if (manipulator_directMode && handFound) {
			
			cmdRobot = 'D';
		}
		oglHandler.updateCompassModel(camDest.pitch);
		if (trackHead) {
			
			// Angles in radians
			if (RiftIsPresent)
			{
				oglHandler.updateCompassModel(-camDest.pitch);
			}
			else {
				camDest = { 0.0f, mouseTestX, mouseTestY };
			}
			remSP[PTU_P] = camDest.pitch;
			if (camDest.roll < -0.311f) camDest.roll = -0.311f;
			remSP[PTU_T] = camDest.roll;
		}
		if (trackDriver)
		{
			oglHandler.updateSpeedArrow(joy_DriveSpeed, joy_DriveAngle, joy_DriveRotation);
			remSP[DRIVER_ANGLE] = joy_DriveAngle;
			remSP[DRIVER_SPEED] = joy_DriveSpeed;
			remSP[DRIVER_ROTATION] = joy_DriveRotation;
		}
		
		if (netHandle.sendSetPoints(remSP, cmdRobot, trackHead, trackDriver, kinectRequest)) {
			sentCmd = cmdRobot;
		}
			
		if (netHandle.getBatteryLevel(battLevel)) {
			if (battLevel != 255.0f) {
				oglHandler.updateBatteryLevel(scaleValue(battLevel, 102.5f, 124.8f, 0.0f, 1.0f)); // Min. measured voltage = 10.25 V. Full battery = 12.48 V
			}
		}

		
		if (netHandle.getKinectColor(kinectImg))
		{
			oglHandler.updateKinectTexture(kinectImg, zoom, !hideCameras && !manipulator_recordMode);
			prevKinectRequest = 'C';
		}
		if (prevKinectRequest == 'C' && kinectRequest != 'C') {
			oglHandler.updateKinectTexture(kinectImg, zoom, !hideCameras || manipulator_recordMode);
			prevKinectRequest = kinectRequest;
		}
		oglHandler.updateTextModels("i_FPS", std::to_string(fpsLeft) + "|" + std::to_string(fpsRight), posTopRight);
		
		if (camHandler_Left.getResult(img_Left)) {
			oglHandler.updateCameraTexture(ovrEye_Left, img_Left, zoom, hideCameras);
			counterLeft++;
		}

		if (camHandler_Right.getResult(img_Right)) {
			if (monoVision) img_Right = img_Left;
			oglHandler.updateCameraTexture(ovrEye_Right, img_Right, zoom, hideCameras);
			counterRight++;
		}
		
		if (RiftIsPresent)
		{
			oglHandler.Render_RiftPresent(OculusRift);
			OculusRift.SubmitToRift(oglHandler.Oculus_TextureSet_Left, oglHandler.Oculus_TextureSet_Right);
			oglHandler.mirrorToScreen(hDC);

		}
		else {
			oglHandler.Render_RiftMissing(hDC, winSize);
		}
		
		if (cmdRobot != 'D' && sentCmd == cmdRobot) {
			cmdRobot = 'N';
		}
		if (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE)) {
			TranslateMessage(&msg);
			DispatchMessage(&msg);
		}
		
	}
	UnregisterClass(szWindowClass, hInstance);

	oglHandler.shutdown(RiftIsPresent, OculusRift.HMD);
	if (RiftIsPresent) OculusRift.Shutdown();
	netHandle.shutDown();
	camHandler_Left.stopCapture();
	camHandler_Right.stopCapture();
	
	return (int)msg.wParam;
}


// Register the window class.
ATOM MyRegisterClass(HINSTANCE hInstance)
{
	WNDCLASSEX wcex; // Windowclass using unicode.

	wcex.cbSize = sizeof(WNDCLASSEX);

	wcex.style = CS_CLASSDC;
	wcex.lpfnWndProc = WndProc;
	wcex.cbClsExtra = 0;
	wcex.cbWndExtra = 0;
	wcex.hInstance = hInstance;
	wcex.hIcon = LoadIcon(hInstance, MAKEINTRESOURCE(IDI_ICON));
	wcex.hCursor = LoadCursor(NULL, IDC_ARROW);
	wcex.hbrBackground = NULL;
	wcex.lpszMenuName = NULL;
	wcex.lpszClassName = szWindowClass;
	wcex.hIconSm = LoadIcon(wcex.hInstance, MAKEINTRESOURCE(IDI_ICON));

	return RegisterClassEx(&wcex);
}

//
//   FUNCTION: InitInstance(HINSTANCE, int)
//
//   PURPOSE: Saves instance handle and creates main window
//
//   COMMENTS:
//
//        In this function, we save the instance handle in a global variable and
//        create and display the main program window.
//
BOOL InitInstance(HINSTANCE hInstance, int nCmdShow)
{
	HWND hWnd;

	hInst = hInstance; // Store instance handle in our global variable
	if (RiftIsPresent)
	{
		winSize = { OculusRift.idealWindowWidth, OculusRift.idealWindowHeight };
	}
	else {
		winSize = { 960, 540 };
	}
	RECT size = { 0, 0, winSize.width, winSize.height };
	// Setting up the window-frame attributes. Excluding WS_THICKFRAME to make the window un-rezisable.
	const DWORD wsStyle = WS_OVERLAPPEDWINDOW & ~WS_THICKFRAME;

	AdjustWindowRect(&size, wsStyle, FALSE); // Adjusting window size to compensate for borders
	hWnd = CreateWindow(
		szWindowClass,			// Classname
		szTitle,				// Windowtitle
		wsStyle | WS_VISIBLE,	// Windowstyle
		CW_USEDEFAULT,			// Initial horizontal windowposition (top left corner coordinate).
		CW_USEDEFAULT,			// Initial vertical position (top left corner).
		size.right - size.left,	// Windowwidth
		size.bottom - size.top,	// Windowheight
		NULL,					// Parent window
		NULL,					// Handle to the menu to be used with the window. NULL: use class-menu.
		hInstance,				// Handle to the instance of the module to be associated with the window.
		NULL);					// Optional pointers for data needed by the window

	if (!hWnd)
	{
		return FALSE;
	}


	hDC = GetDC(hWnd); // Getting device context of the window
	context = createFakeGLContext(hWnd, hDC);

	if (!wglMakeCurrent(hDC, context)) {
		wglDeleteContext(context);
		ReleaseDC(hWnd, hDC);
		return FALSE;
	}

	// Only possible to init wglew when a context is current.
	wglewInit();
	
	// We now have our function pointers - Delete current context, and create the one we are going to use, by using the extension functions:
	wglDeleteContext(context);
	context = createGLContext(hWnd, hDC);

	if (!context) {
		return FALSE;
	}

	if (!wglMakeCurrent(hDC, context)) {
		wglDeleteContext(context);
		ReleaseDC(hWnd, hDC);
		return FALSE;
	}
	SetTimer(hWnd, 1, 1000, NULL);
	ShowWindow(hWnd, SW_SHOWDEFAULT);


	return TRUE;
}

//
//  FUNCTION: WndProc(HWND, UINT, WPARAM, LPARAM)
//
//  PURPOSE:  Processes messages for the main window.
//
//  WM_COMMAND	- process the application menu
//  WM_PAINT	- Paint the main window
//  WM_DESTROY	- post a quit message and return
//
//
DWORD prevButton = 0xffff;
LRESULT CALLBACK WndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
	DWORD prevPOV = 1234;
	int wmId, wmEvent;
	PAINTSTRUCT ps;
	HDC hdc;
	float x_Pos, y_Pos;
	switch (message)
	{
	case WM_CREATE:
		if (joySetCapture(hWnd, JOYSTICKID1, NULL, FALSE)) // hWnd, Joystick ID, Polling frequency, Messages only when position change
		{
			MessageBeep(MB_ICONEXCLAMATION); // Error message when joystick is not found
			MessageBox(hWnd, "Couldn't capture the joystick.", NULL,
				MB_OK | MB_ICONEXCLAMATION);
			PostMessage(hWnd, WM_CLOSE, 0, 0L);
		}
		else {
			trackDriver = true;
		}
		break;
	case WM_MOUSEMOVE:
		
		mouseTestY = scaleValue((float)GET_Y_LPARAM(lParam), 0.0f, 540.0f, -458.0f, 458.0f);
		mouseTestX = scaleValue((float)GET_X_LPARAM(lParam), 0.0f, 960.0f, -2.0f, 2.0f);
		break;
	case MM_JOY1ZMOVE:
		
		robot_maxDriveSpeed = scaleValue((float)LOWORD(lParam), 0.0f, 65535.0f, 1000.0f, 0.0f);
		
		break;
	case MM_JOY1MOVE:                     // changed position
		JOYINFOEX joyinfoex;
		joyinfoex.dwFlags = JOY_RETURNPOV;
		joyGetPosEx(JOYSTICKID1, &joyinfoex);
		x_Pos = scaleValue((float)(LOWORD(lParam)), 0.0f, 65535.0f, -robot_maxDriveSpeed, robot_maxDriveSpeed);
		y_Pos = scaleValue((float)(HIWORD(lParam)), 0.0f, 65535.0f, robot_maxDriveSpeed, -robot_maxDriveSpeed);
		if (x_Pos != 0.0f) {
			joy_DriveAngle = atan2(x_Pos, y_Pos) * 180.0f / 3.14f;
		}
		else {
			joy_DriveAngle = 0.0f;
		}
		if (wParam == JOY_BUTTON1){
	
			float vecLen = sqrt((x_Pos * x_Pos) + (y_Pos * y_Pos));
			if (vecLen > robot_maxDriveSpeed)
			{
				joy_DriveSpeed = robot_maxDriveSpeed;
			}
			else if (vecLen < ((robot_maxDriveSpeed / 100.0f) * 10.0f)) { // Deadband of 10% around center
				joy_DriveSpeed = 0.0f;
			}
			else {
				joy_DriveSpeed = vecLen;
			}
			
		} else if (wParam == JOY_BUTTON2) { // Toggling rotation
			float x_Pos = scaleValue((float)(LOWORD(lParam)), 0.0f, 65535.0f, -robot_maxDriveSpeed, robot_maxDriveSpeed);

			if (x_Pos > 0) {
				joy_DriveRotation = 1.0f;
			}
			else if (x_Pos < 0) {
				joy_DriveRotation = -1.0f;
			}
			else {
				joy_DriveRotation = 0.0f;
			}
		

			float vecLen = abs(x_Pos);

			if (vecLen > robot_maxDriveSpeed)
			{
				joy_DriveSpeed = robot_maxDriveSpeed;
			}
			else if (vecLen < ((robot_maxDriveSpeed / 100.0f) * 10.0f)) { // Deadband of 10% around center
				joy_DriveSpeed = 0.0f;
			}
			else {
				joy_DriveSpeed = vecLen;
			}


		}
		else if (wParam == JOY_BUTTON3 && wParam != prevButton) {
			zoom = !zoom;
		}
		else if (wParam == JOY_BUTTON4 && wParam != prevButton) {
			OculusRift.RecenterHeadPose();
			trackHead = !trackHead;
			if (trackHead) oglHandler.updateTextModels("PTU_status", "PTU: Enabled", posBotLeft);
			else oglHandler.updateTextModels("PTU_status", "PTU: Disabled", posBotLeft);
		}
		else if (wParam == JOY_BUTTON5 && wParam != prevButton) {
			if (!manipulator_recordMode && LeapIsPresent) {
				OculusRift.RecenterHeadPose();
				manipulator_recordMode = true; // Query Kinect
				cmdRobot = 'E';
				hideCameras = true;
				manipulator_directMode = false;
				kinectRequest = 'D';
				oglHandler.updateTextModels("armMode", "Arm: Record", posBotMid);
			}
			else {
				manipulator_recordMode = false; // Remove Kinect model
				hideCameras = false;
				kinectRequest = 'N';
				oglHandler.removeModelCollection("Kinect");
				oglHandler.updateTextModels("armMode", "Arm: Disabled", posBotMid);
				oglHandler.updateRobotModel(0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f);
			}
		}
		else if (wParam == JOY_BUTTON6 && wParam != prevButton) {
			if (!manipulator_directMode && LeapIsPresent) {
				cmdRobot = 'D'; // Toggle Direct mode
				OculusRift.RecenterHeadPose();
				manipulator_directMode = true;
				manipulator_recordMode = false;
				oglHandler.updateTextModels("armMode", "Arm: Direct", posBotMid);
			}
			else {
				cmdRobot = 'N';
				manipulator_directMode = false;
				oglHandler.updateTextModels("armMode", "Arm: Disabled", posBotMid);
			}

		}
		else if (wParam == JOY_BUTTON7 && wParam != prevButton) {
			if (manipulator_recordMode)	cmdRobot = 'A'; // Add point
		}
		else if (wParam == JOY_BUTTON8 && wParam != prevButton) {
			if (manipulator_recordMode)	cmdRobot = 'R'; // Remove point
		}
		else if (wParam == JOY_BUTTON9 && wParam != prevButton) {
			
		}
		else if (wParam == JOY_BUTTON10 && wParam != prevButton) {
			if (manipulator_recordMode)	cmdRobot = 'E'; // Erase points
		}
		else if (wParam == JOY_BUTTON11 && wParam != prevButton) {
			if (manipulator_recordMode)	cmdRobot = 'X'; // Execute points
		}
		else if (wParam == JOY_BUTTON12 && wParam != prevButton) {
			if (manipulator_recordMode)	cmdRobot = 'C'; // Cancel operation
		}
		else if (joyinfoex.dwPOV == JOY_POVFORWARD) {
			if (!manipulator_recordMode) {
				hideCameras = false;
				kinectRequest = 'N';
			}
		}
		else if (joyinfoex.dwPOV == JOY_POVLEFT && joyinfoex.dwPOV != prevPOV) {
			monoVision = !monoVision;
		}
		else if (joyinfoex.dwPOV == JOY_POVRIGHT) {
			OculusRift.RecenterHeadPose();
		}
		else if (joyinfoex.dwPOV == JOY_POVBACKWARD) {
			if (!manipulator_recordMode) {
				kinectRequest = 'C';
				hideCameras = true;
			}
		}
		else {
			joy_DriveSpeed = 0.0f;
			joy_DriveRotation = 0.0f;
		}
		
		prevButton = wParam;
		prevPOV = joyinfoex.dwPOV;
		break;
	case WM_KEYUP:
		if (wParam == VK_SNAPSHOT && wParam != prevButton) {
			if (manipulator_recordMode)	cmdRobot = 'A'; // Add point
		}
		else if (wParam == VK_ADD && wParam != prevButton) {
			dummy = !dummy;
		}
		prevButton = wParam;
		break;
	case WM_TIMER:
		if (wParam == 1) {
			fpsLeft = counterLeft;
			fpsRight = counterRight;
			counterLeft = 0;
			counterRight = 0;
			oglHandler.updateClockModel();
		}
		break;
	case WM_DESTROY:
		run = false;
		if (context) {
			wglMakeCurrent(NULL, NULL);
			wglDeleteContext(context);
		}

		PostQuitMessage(0);
		break;
	default:
		return DefWindowProc(hWnd, message, wParam, lParam);
	}
	return 0;
}



/*
Creating an OpenGL context for the sole purpose of getting access to the two functions wglChoosePixelFormatARB and wglCreateContextAttribsARB functions.
*/
HGLRC createFakeGLContext(HWND Window, HDC hDC) {

	PIXELFORMATDESCRIPTOR pfd;

	memset(&pfd, 0, sizeof(pfd)); // Allocating memory

	// Defining our desired pixelformat
	pfd.nSize = sizeof(pfd);
	pfd.nVersion = 1; // Default version
	pfd.iPixelType = PFD_TYPE_RGBA; // RGBA color mode
	pfd.dwFlags = PFD_SUPPORT_OPENGL | PFD_DRAW_TO_WINDOW | PFD_DOUBLEBUFFER; // OpenGL support | Window drawing support | Double buffering support
	pfd.cColorBits = 32; // 32 bit color mode
	pfd.cDepthBits = 16; // 16 bit z-buffer size

	// Choosing the best matching pixel format based on given preferences. A number is returned by ChoosePixelFormat() representing the best matching pixel format available.
	int pf = ChoosePixelFormat(hDC, &pfd);

	if (!pf) // No matching pixel format found -> Release device context and kill program
	{
		ReleaseDC(Window, hDC);
		return false;
	}

	if (!SetPixelFormat(hDC, pf, &pfd)) // Unable to activate the pixelformat -> Release device context and kill program.
	{
		ReleaseDC(Window, hDC);
		return false;
	}

	return wglCreateContext(hDC); // Creating and returning an OpenGL context based on the updated windowcontext hDC.
}

HGLRC createGLContext(HWND Window, HDC hDC) {

	int iAttributes[] = {
		WGL_SUPPORT_OPENGL_ARB, GL_TRUE,
		WGL_COLOR_BITS_ARB, 32,
		WGL_DEPTH_BITS_ARB, 16,
		WGL_DOUBLE_BUFFER_ARB, GL_TRUE,
		WGL_FRAMEBUFFER_SRGB_CAPABLE_ARB, GL_TRUE,
		0, 0 };

	float fAttributes[] = { 0, 0 };

	int   pixelformat = 0;
	UINT  numFormats = 0;
	if (!__wglewChoosePixelFormatARB(hDC, iAttributes, fAttributes, 1, &pixelformat, &numFormats)) {
		ReleaseDC(Window, hDC);
		return false;
	}

	PIXELFORMATDESCRIPTOR pfd;
	memset(&pfd, 0, sizeof(pfd));

	if (!SetPixelFormat(hDC, pixelformat, &pfd))
	{
		ReleaseDC(Window, hDC);
		return false;
	}

	GLint attribs[16];
	int attribCount = 0;

	attribs[attribCount] = 0;

	return __wglewCreateContextAttribsARB(hDC, 0, attribs);
}

bool getJointAngles(glm::vec4 xyzCoords, float &a_base, float &a_shoulder, float &a_elbow) {

	// DH- Parameters
	float a1 = 16.0f;
	float d1 = 349.0f;
	float a2 = 221.0f;
	float a3 = 221.0f;

	float xMin = 250.0f;
	float zMin = -200.0f;

	float scaledXc = xyzCoords.y;	float scaledYc = xyzCoords.x;	float scaledZc = xyzCoords.z;
	
	float XcSquared = pow(scaledXc, 2); float YcSquared = pow(scaledYc, 2); float ZcSquared = pow(scaledZc, 2);

	float vLen = sqrt(XcSquared + YcSquared + ZcSquared); // Total length of the vector

	float tetha = acos(scaledXc / vLen); // The angle between the X-unitvector (robot forward) and the position vector.
	float gamma = atan2(scaledYc, scaledZc); // The angle of the position vector in the yz-plane
	float phi = acos(sqrt(XcSquared + YcSquared) / vLen); // Angle relative to the horizontal plane

	float maxR = a2 + a3 + a1 * cos(phi); // Max physically possible arm-stretch

	float xMax = maxR * cos(tetha); // Max physically possible X position (forward) at the current angle
	float yMax = maxR * sin(tetha) * sin(gamma); // Max physically possible Y position (left / right) at the current angle
	float zMax = maxR * sin(tetha) * cos(gamma); // Max physically possible Z position (up / down) at the current angle
	
	if (scaledXc > abs(xMax)) scaledXc = xMax;
	if (scaledXc < xMin) scaledXc = xMin; // Making sure the robot arm stays in front of its own base
	if (abs(scaledYc) > abs(yMax)) scaledYc = yMax;

	if (scaledZc > abs(zMax)) scaledZc = zMax;
	if (scaledZc < zMin) scaledZc = zMin; // Making sure the robot does not hit the floor

	float baseAngle = atan2(scaledYc, scaledXc);
	
	float D = (pow(scaledXc - a1 * cos(phi) * cos(baseAngle), 2) + pow(scaledYc - a1 * cos(phi) * sin(baseAngle), 2) + pow(scaledZc, 2) - pow(a2, 2) - pow(a3, 2))
		/ (2.0f * a2 * a3);
	if (D > 1.0f) D = 1.0f;
	float elbowAngle = atan2(sqrt(1.0f - D*D), D); // Elbow up / elbow down = - / + sqrt
	float shoulderAngle = atan2(-scaledZc, sqrt(pow(scaledXc - a1  * cos(phi) * cos(baseAngle), 2) + pow(scaledYc - a1  * cos(phi) * sin(baseAngle), 2))) - atan2(a3 * sin(elbowAngle), a2 + a3 * cos(elbowAngle));

	a_base = baseAngle * -RAD_TO_DEG;
	a_shoulder = shoulderAngle * RAD_TO_DEG;
	a_elbow = elbowAngle * RAD_TO_DEG;
	
	return true;
}

bool compensateHeadMovement(glm::vec4 &leapVector, glm::vec4 &destVector, headAngle headRot) {

	glm::mat4 toOculusSys = { 1.0f, 0.0f, 0.0f, 0.0f,
		0.0f, 0.0f, -1.0f, 0.0f,
		0.0f, 1.0f, 0.0f, 0.0f,
		0.0f, 0.0f, -180.0f, 1.0f };

	glm::mat4 toLeapSys = { 1.0f, 0.0f, 0.0f, 0,
		0.0f, 0.0f, 1.0f, 0.0f,
		0.0f, -1.0f, 0.0f, 0,
		0.0f, -180.0f, 0.0f, 1.0f };

	glm::vec4 vectorInOculus = toOculusSys * leapVector;

	glm::mat4 rotationMatrix = glm::rotate(glm::mat4(), headRot.roll, glm::vec3(1.0f, 0.0f, 0.0f)) * glm::rotate(glm::mat4(), -headRot.pitch, glm::vec3(0.0f, 1.0f, 0.0f));

	glm::vec4 rotatedVector = rotationMatrix * vectorInOculus;

	destVector = toLeapSys * rotatedVector;
	
	return true;
}