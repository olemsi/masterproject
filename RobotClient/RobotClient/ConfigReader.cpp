#include "ConfigReader.h"



// ----------------------------------
// method implementations
// ----------------------------------

void ConfigReader::Clear()
{
	data.clear();
}

bool ConfigReader::Load(const string& file)
{
	ifstream inFile(file.c_str());

	if (!inFile.good())
	{
		cout << "Cannot read configuration file " << file << endl;
		return false;
	}

	while (inFile.good() && !inFile.eof())
	{
		string line;
		getline(inFile, line);

		// Filter comments
		if (!line.empty())
		{
			int pos = line.find('#');

			if (pos != string::npos)
			{
				line = line.substr(0, pos);
			}
		}

		// split line into key and value
		if (!line.empty())
		{
			int pos = line.find('=');

			if (pos != string::npos)
			{
				string key = Trim(line.substr(0, pos));
				string value = Trim(line.substr(pos + 1));

				if (!key.empty() && !value.empty())
				{
					data[key] = value;
				}
			}
		}
	}

	return true;
}

bool ConfigReader::Contains(const string& key) const
{
	return data.find(key) != data.end();
}

bool ConfigReader::Get(const string& key, string& value) const
{
	map<string, string>::const_iterator iter = data.find(key);

	if (iter != data.end())
	{
		value = iter->second;
		return true;
	}
	else
	{
		return false;
	}
}

bool ConfigReader::Get(const string& key, int& value) const
{
	string str;

	if (Get(key, str))
	{
		value = atoi(str.c_str());
		return true;
	}
	else
	{
		return false;
	}
}

bool ConfigReader::Get(const string& key, unsigned short& value) const
{
	string str;

	if (Get(key, str))
	{
		value = (unsigned short)strtoul(str.c_str(), NULL, 0);
		return true;
	}
	else
	{
		return false;
	}
}


bool ConfigReader::Get(const string& key, long& value) const
{
	string str;

	if (Get(key, str))
	{
		value = atol(str.c_str());
		return true;
	}
	else
	{
		return false;
	}
}

bool ConfigReader::Get(const string& key, double& value) const
{
	string str;

	if (Get(key, str))
	{
		value = atof(str.c_str());
		return true;
	}
	else
	{
		return false;
	}
}

bool ConfigReader::Get(const string& key, bool& value) const
{
	string str;

	if (Get(key, str))
	{
		value = (str == "true");
		return true;
	}
	else
	{
		return false;
	}
}

string ConfigReader::Trim(const string& str)
{
	int first = str.find_first_not_of(" \t");

	if (first != string::npos)
	{
		int last = str.find_last_not_of(" \t");

		return str.substr(first, last - first + 1);
	}
	else
	{
		return "";
	}
}

void ConfigReader::WriteDefault() {

	ofstream cfgFile;
	cfgFile.open("Config.cfg");
	cfgFile << "#\n";
	cfgFile << "# RobotClient parameters\n";
	cfgFile << "#\n";
	cfgFile << " \n";
	cfgFile << "IP_LeftCam			= 192.168.0.100 # Note: All devices behind local router w/ port forwarding\n";
	cfgFile << "IP_RightCam			= 192.168.0.102\n";
	cfgFile << "IP_Server			= 192.168.0.103\n";
	cfgFile << "\n";
	cfgFile << "Port_LeftCam		= 2554\n";
	cfgFile << "Port_RightCam		= 2555\n";
	cfgFile << "Port_ServerSend		= 2222\n";
	cfgFile << "Port_ServerReceive	= 2223\n";
	cfgFile << "Font                = C:/Windows/Fonts/arial.ttf\n";
	cfgFile.close();

}