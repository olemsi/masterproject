
#include "Leap.h"
#include "LeapMath.h"
#include "NetworkHandler.h"

using namespace Leap;

class LeapMotionController {
public:
	bool initLeap();
	
	bool getHandPositions(float &X, float &Y, float &Z, float &roll, float &pitch, float &yaw);
	void Shutdown();

private:
	Controller leap_Controller;
	Vector LeapToWorld(Vector leapPoint, InteractionBox iBox);
};