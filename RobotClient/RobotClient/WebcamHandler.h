#ifndef WEBCAMHANDLER_H
#define WEBCAMHANDLER_H


#include <Windows.h>
#include <string>
#include <thread>
#include <mutex>
#include <opencv2/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <OVR_CAPI.h>
#include <iostream>

using namespace cv;
using namespace std;

class WebcamHandler {
private:
	bool running = true;
	VideoCapture videoCapture;
	thread camThread;
	mutex lock;
	Mat frame;
	bool hasFrame = false;
	
	void setResult(const Mat &newFrame);
	void captureLoop();

public:
	WebcamHandler() {}
	float startCapture(string adr);
	void stopCapture();
	bool getResult(cv::Mat &out);
};
#endif