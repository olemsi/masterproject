//#include "stdafx.h"
#include "OculusGL_Interface.h"

ovrHmd HMD;
ovrGraphicsLuid luid;

ovrPosef headPose;
ovrPosef EyeRenderPose[2];
ovrVector3f viewOffset[2];
OculusRift_Interface::OculusRift_Interface() {

}


bool OculusRift_Interface::initRift() {

	if (ovr_Initialize(nullptr) != ovrSuccess) {
		return false;
	}
	ovrResult result = ovr_Create(&HMD, &luid); // Tracking started by default
	
	if (!OVR_SUCCESS(result)) {
		// Rift not detected
		ovr_Shutdown();
		return false;
	}
	
	ovrHmdDesc hmdDesc = ovr_GetHmdDesc(HMD);

	EyeRenderDescription[ovrEye_Left] = ovr_GetRenderDesc(HMD, ovrEye_Left, hmdDesc.DefaultEyeFov[0]);
	EyeRenderDescription[ovrEye_Right] = ovr_GetRenderDesc(HMD, ovrEye_Right, hmdDesc.DefaultEyeFov[1]);

	idealTextureSize_Left = ovr_GetFovTextureSize(HMD, ovrEye_Left, hmdDesc.DefaultEyeFov[ovrEye_Left], 1);
	idealTextureSize_Right = ovr_GetFovTextureSize(HMD, ovrEye_Right, hmdDesc.DefaultEyeFov[ovrEye_Right], 1);
	
	idealWindowWidth = (long)hmdDesc.Resolution.w / 2;
	idealWindowHeight = (long)hmdDesc.Resolution.h / 2;
	
	return true;
}


headAngle OculusRift_Interface::getHeadAngle() {

	float p_Yaw = 0;
	float p_Pitch = 0;
	float p_Roll = 0;
		

	double ftiming = ovr_GetPredictedDisplayTime(HMD, 0);
	double sensorSampleTime = ovr_GetTimeInSeconds();
	ovrTrackingState hmdState = ovr_GetTrackingState(HMD, ftiming, ovrTrue);
	headPose = hmdState.HeadPose.ThePose;
	headAngle currentAngle;
	
	OVR::Quatf quaternion = headPose.Orientation;
	// Calculating Euler Angles from given quaternion - The Oculus SDK handles the singularities at the North and South pole
	// Using traditional formulas we are unable to pass +-90�, as the values returned will get mirrored over the unit circle
	quaternion.GetEulerAngles<OVR::Axis_Y, OVR::Axis_X, OVR::Axis_Z>(&p_Pitch, &p_Roll, &p_Yaw);

	currentAngle.pitch = p_Pitch;
	currentAngle.yaw = p_Yaw;
	currentAngle.roll = p_Roll;

	return currentAngle;
}

void OculusRift_Interface::RecenterHeadPose() {
	ovr_RecenterPose(HMD);
}

void OculusRift_Interface::SubmitToRift(ovrSwapTextureSet *textureSet_Left, ovrSwapTextureSet *textureSet_Right) {

	viewOffset[0] = EyeRenderDescription[ovrEye_Left].HmdToEyeViewOffset;
	viewOffset[1] = EyeRenderDescription[ovrEye_Right].HmdToEyeViewOffset;
	ovr_CalcEyePoses(headPose, viewOffset, EyeRenderPose);
	
	ovrViewScaleDesc viewScaleDesc;
	viewScaleDesc.HmdSpaceToWorldScaleInMeters = 1.0f;
	viewScaleDesc.HmdToEyeViewOffset[0] = viewOffset[0];
	viewScaleDesc.HmdToEyeViewOffset[1] = viewOffset[1];

	ovrLayerEyeFov layer;
	
	//layer.Header.Type = ovrLayerType_Direct;
	layer.Header.Type = ovrLayerType_EyeFov; // Fixes chromatic aberration, causes timewarp disturbance
	layer.Header.Flags = ovrLayerFlag_TextureOriginAtBottomLeft | ovrLayerFlag_HighQuality;

	layer.ColorTexture[ovrEye_Left] = textureSet_Left;
	layer.ColorTexture[ovrEye_Right] = textureSet_Right;

	ovrRecti viewPort_Left = { 0, 0, idealTextureSize_Left.w, idealTextureSize_Left.h };
	ovrRecti viewPort_Right = { 0, 0, idealTextureSize_Right.w, idealTextureSize_Right.h };
	
	layer.Viewport[ovrEye_Left] = viewPort_Left;
	layer.Viewport[ovrEye_Right] = viewPort_Right;

	layer.Fov[ovrEye_Left] = EyeRenderDescription[ovrEye_Left].Fov;
	layer.Fov[ovrEye_Right] = EyeRenderDescription[ovrEye_Right].Fov;

	layer.RenderPose[ovrEye_Left] = EyeRenderPose[ovrEye_Left];
	layer.RenderPose[ovrEye_Right] = EyeRenderPose[ovrEye_Right];

	layer.SensorSampleTime = ovr_GetTimeInSeconds();
	

	ovrLayerHeader *layerHeader = &layer.Header;
	ovrResult result = ovr_SubmitFrame(HMD, 0, &viewScaleDesc, &layerHeader, 1);
}

void OculusRift_Interface::Shutdown() {

	ovr_Destroy(HMD);
	ovr_Shutdown();

}