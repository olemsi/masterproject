#include "OculusGL_Interface.h"

static const char* VertexShaderModel =
"#version 150\n"

"in      vec4 Position;"
"in      vec4 Color;"
"in      vec2 TexCoord;"

"uniform mat4 projMatrix;"
"uniform mat4 viewMatrix;"
"uniform mat4 modelMatrix;"

"out     vec4 oColor;"
"out     vec2 oTexCoord;"

"void main() {"
"   oColor = Color;"
"   oTexCoord = TexCoord;"
"   gl_Position = (projMatrix * viewMatrix * modelMatrix) * Position;"
"};";

static const char* FragmentShaderModel =
"#version 150\n"

"in      vec4      oColor;"
"in      vec2      oTexCoord;"

"uniform sampler2D Texture0;"

"out     vec4      FragColor;"

"void main() {"
"   FragColor = texture2D(Texture0, oTexCoord) * oColor;"
"};";


GLuint shader_program_Model;
GLuint fboId_window; // Framebuffer Object

// Textureset variables
GLuint fboTexture;
ovrSwapTextureSet* Oculus_TextureSet_Left;
ovrSwapTextureSet* Oculus_TextureSet_Right;
GLuint textureDepth_Left;
GLuint textureDepth_Right;

// Mirroring variables
GLuint mirrorFBO = 0;
ovrGLTexture* mirrorTexture;

// Constructor
OpenGL_Handler::OpenGL_Handler() {

}

GLuint OpenGL_Handler::getVertexShader(const char* src) {

	
	GLuint vshader = glCreateShader(GL_VERTEX_SHADER);
	glShaderSource(vshader, 1, &src, NULL);
	glCompileShader(vshader);

	GLint r;
	glGetShaderiv(vshader, GL_COMPILE_STATUS, &r);
	if (!r)
	{
		GLchar msg[1024];
		glGetShaderInfoLog(vshader, sizeof(msg), 0, msg);
		if (msg[0]) {
			// Compiling shaders failed
		}
		return 0;
	}
	return vshader;
}

GLuint OpenGL_Handler::getFragmentShader(const char* src) {


	GLuint fshader = glCreateShader(GL_FRAGMENT_SHADER);

	glShaderSource(fshader, 1, &src, NULL);
	glCompileShader(fshader);

	GLint r;
	glGetShaderiv(fshader, GL_COMPILE_STATUS, &r);
	if (!r)
	{
		GLchar msg[1024];
		glGetShaderInfoLog(fshader, sizeof(msg), 0, msg);
		if (msg[0]) {
			// Compiling shaders failed
		}
		return 0;
	}

	return fshader;
}

GLuint OpenGL_Handler::getShaderProgram(GLuint vertex_shader, GLuint fragment_shader) {

	GLuint vshader = vertex_shader;
	GLuint fshader = fragment_shader;

	GLuint program = glCreateProgram();
	glAttachShader(program, vshader);
	glAttachShader(program, fshader);

	glLinkProgram(program);
	GLint response;

	glGetProgramiv(program, GL_LINK_STATUS, &response);
	if (!response) {
		GLchar msg[1024];
		glGetProgramInfoLog(program, sizeof(msg), 0, msg);
		// ERROR!
		return GL_FALSE;
	}

	glDetachShader(program, vshader);
	glDetachShader(program, fshader);

	glDeleteShader(vshader);
	glDeleteShader(fshader);

	return program;
}

void OpenGL_Handler::setupMirrorTexture(ovrHmd hmd, int windowWidth, int windowHeight) {

	ovr_CreateMirrorTextureGL(hmd, GL_RGBA, windowWidth, windowHeight, (ovrTexture**)&mirrorTexture);

	glGenFramebuffers(1, &mirrorFBO);
	glBindFramebuffer(GL_READ_FRAMEBUFFER, mirrorFBO);
	glFramebufferTexture2D(GL_READ_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, mirrorTexture->OGL.TexId, 0);
	glFramebufferRenderbuffer(GL_READ_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_RENDERBUFFER, 0);
	glBindFramebuffer(GL_READ_FRAMEBUFFER, 0);

}

bool OpenGL_Handler::initRendering(bool RiftIsPresent, OculusRift_Interface ovr,
	cv::Mat CamPicture_Left, cv::Mat CamPicture_Right, const char* fontDir){
	
	glewInit();
	if(!initFreetype(fontDir)) return false;
	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
	glEnable(GL_DEPTH_TEST);
	glEnable(GL_PROGRAM_POINT_SIZE);
	glDepthFunc(GL_LESS);
	glFrontFace(GL_CW);
	
	modelCollection = mainHud(RiftIsPresent, CamPicture_Left, CamPicture_Right);
	
	if (RiftIsPresent) {
		ovr_CreateSwapTextureSetGL(ovr.HMD, GL_RGBA, ovr.idealTextureSize_Left.w, ovr.idealTextureSize_Left.h, &Oculus_TextureSet_Left);

		for (int i = 0; i < Oculus_TextureSet_Left->TextureCount; i++) {
			ovrGLTexture* tex = (ovrGLTexture*)&Oculus_TextureSet_Left->Textures[i];
			glBindTexture(GL_TEXTURE_2D, tex->OGL.TexId);

			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
		}

		ovr_CreateSwapTextureSetGL(ovr.HMD, GL_RGBA, ovr.idealTextureSize_Right.w, ovr.idealTextureSize_Right.h, &Oculus_TextureSet_Right);

		for (int i = 0; i < Oculus_TextureSet_Right->TextureCount; i++) {
			ovrGLTexture* tex = (ovrGLTexture*)&Oculus_TextureSet_Right->Textures[i];
			glBindTexture(GL_TEXTURE_2D, tex->OGL.TexId);

			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
		}

		glGenTextures(1, &textureDepth_Left);
		glBindTexture(GL_TEXTURE_2D, textureDepth_Left);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
		
		glTexImage2D(GL_TEXTURE_2D, 0, GL_DEPTH_COMPONENT24, ovr.idealTextureSize_Left.w, ovr.idealTextureSize_Left.h, 0, GL_DEPTH_COMPONENT, GL_UNSIGNED_INT, NULL);

		glGenTextures(1, &textureDepth_Right);
		glBindTexture(GL_TEXTURE_2D, textureDepth_Right);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);

		glTexImage2D(GL_TEXTURE_2D, 0, GL_DEPTH_COMPONENT24, ovr.idealTextureSize_Right.w, ovr.idealTextureSize_Right.h, 0, GL_DEPTH_COMPONENT, GL_UNSIGNED_INT, NULL);
	}
		

	shader_program_Model = getShaderProgram(getVertexShader(VertexShaderModel), getFragmentShader(FragmentShaderModel));
	if (GL_FALSE == shader_program_Model) return false;


	if (RiftIsPresent)
	{
		glGenFramebuffers(1, &fboTexture);
		setupMirrorTexture(ovr.HMD, ovr.idealWindowWidth, ovr.idealWindowHeight);
	}
	
	return true;
}

void OpenGL_Handler::Render_RiftMissing(HDC hDC, windowSize windowRect) {


	GLuint posLoc, colorLoc, uvLoc;
	GLuint projMatLoc, viewMatLoc, modelMatLoc;

	glUseProgram(shader_program_Model);

	glm::mat4 view = glm::lookAt( // Simulates a camera
		glm::vec3(0.0f, 0.0f, 55.0f),  // Camera position - x, y, z
		glm::vec3(0.0f, 0.0f, 0.0f),  // Center point on screen
		glm::vec3(0.0f, 1.0f, 0.0f)); // Up axis (Z-axis in this case -> XY plane is the "ground")

	float aspect = ((float)(windowRect.width)) / ((float)windowRect.height);
	glm::mat4 projection = glm::perspective(glm::radians(80.0f), aspect, 0.2f, 1000.0f); // Vertical FOV, Aspect Ratio, Z-near, Z-far

	posLoc = glGetAttribLocation(shader_program_Model, "Position");
	colorLoc = glGetAttribLocation(shader_program_Model, "Color");
	uvLoc = glGetAttribLocation(shader_program_Model, "TexCoord");
	projMatLoc = glGetUniformLocation(shader_program_Model, "projMatrix");
	viewMatLoc = glGetUniformLocation(shader_program_Model, "viewMatrix");
	modelMatLoc = glGetUniformLocation(shader_program_Model, "modelMatrix");

	glUniform1i(glGetUniformLocation(shader_program_Model, "Texture0"), 0);

	glUniformMatrix4fv(projMatLoc, 1, GL_FALSE, glm::value_ptr(projection));
	glUniformMatrix4fv(viewMatLoc, 1, GL_FALSE, glm::value_ptr(view));
	glActiveTexture(GL_TEXTURE0);
	glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
	// glViewPort is default set to window size
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	for (std::vector<ModelCollection>::iterator collection = modelCollection.begin(); collection != modelCollection.end(); ++collection) {

		for (std::vector<Model>::iterator mdl = collection->modelList.begin(); mdl != collection->modelList.end(); ++mdl)
		{

			glBindTexture(GL_TEXTURE_2D, mdl->texture);

			glUniformMatrix4fv(modelMatLoc, 1, GL_FALSE, glm::value_ptr(mdl->modelMatrix));

			glBindBuffer(GL_ARRAY_BUFFER, mdl->vbo); // Mulig de ikke beh�ver � bindes her
			glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, mdl->ibo);

			glEnableVertexAttribArray(posLoc);
			glEnableVertexAttribArray(colorLoc);
			glEnableVertexAttribArray(uvLoc);

			glVertexAttribPointer(posLoc, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), 0);
			glVertexAttribPointer(colorLoc, 4, GL_FLOAT, GL_FALSE, sizeof(Vertex), (void*)(3 * sizeof(float)));
			glVertexAttribPointer(uvLoc, 2, GL_FLOAT, GL_FALSE, sizeof(Vertex), (void*)(7 * sizeof(float)));

			if (mdl->getName() == "mdlKinect") {
				glPointSize(1.0f);
				glDrawArrays(GL_POINTS, 0, mdl->vertices.size());
			}
			else {

				glDrawElements(GL_TRIANGLES, mdl->indices.size(), GL_UNSIGNED_INT, NULL);

			}

		}
	}
	SwapBuffers(hDC);
}

void OpenGL_Handler::Render_RiftPresent(OculusRift_Interface ovr) {

	Oculus_TextureSet_Left->CurrentIndex = (Oculus_TextureSet_Left->CurrentIndex + 1) % Oculus_TextureSet_Left->TextureCount;
	Oculus_TextureSet_Right->CurrentIndex = (Oculus_TextureSet_Right->CurrentIndex + 1) % Oculus_TextureSet_Right->TextureCount;

	GLuint posLoc, colorLoc, uvLoc;
	GLuint projMatLoc, viewMatLoc, modelMatLoc;
	
	ovrGLTexture *tex;
	
	glEnable(GL_FRAMEBUFFER_SRGB);
	for (int eye = 0; eye < 2; eye++) {

		OVR::Matrix4f view = OVR::Matrix4f::LookAtRH( // Simulates a camera
			OVR::Vector3f(ovr.EyeRenderDescription[eye].HmdToEyeViewOffset.x, 0.0f, 55.0f),  // Camera position - x, y, z
			OVR::Vector3f(ovr.EyeRenderDescription[eye].HmdToEyeViewOffset.x, 0.0f, 0.0f),  // Center point on screen
			OVR::Vector3f(0.0f, 1.0f, 0.0f)); // Up axis (Y-axis in this case -> XZ plane is the "ground")
				
		OVR::Matrix4f projection = ovrMatrix4f_Projection(ovr.EyeRenderDescription[eye].Fov, 0.2f, 1000.0f, ovrProjection_RightHanded);
		
		
		GLuint textureDepth;
		ovrSizei textureSize;
		if (eye == ovrEye_Left) {
			tex = (ovrGLTexture*)&Oculus_TextureSet_Left->Textures[Oculus_TextureSet_Left->CurrentIndex];
			textureSize = ovr.idealTextureSize_Left;
			textureDepth = textureDepth_Left;
		}
		else { //(eye == ovrEye_Right)
			tex = (ovrGLTexture*)&Oculus_TextureSet_Right->Textures[Oculus_TextureSet_Right->CurrentIndex];
			textureSize = ovr.idealTextureSize_Right;
			textureDepth = textureDepth_Right;
		}
		glUseProgram(shader_program_Model);

		glBindFramebuffer(GL_FRAMEBUFFER, fboTexture);
		glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, tex->OGL.TexId, 0);
		glFramebufferTexture2D(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_TEXTURE_2D, textureDepth, 0);

		posLoc = glGetAttribLocation(shader_program_Model, "Position");
		colorLoc = glGetAttribLocation(shader_program_Model, "Color");
		uvLoc = glGetAttribLocation(shader_program_Model, "TexCoord");
		projMatLoc = glGetUniformLocation(shader_program_Model, "projMatrix");
		viewMatLoc = glGetUniformLocation(shader_program_Model, "viewMatrix");
		modelMatLoc = glGetUniformLocation(shader_program_Model, "modelMatrix");

		glUniformMatrix4fv(projMatLoc, 1, GL_TRUE, (FLOAT*)&projection);
		glUniformMatrix4fv(viewMatLoc, 1, GL_TRUE, (FLOAT*)&view);
		
		glViewport(0, 0, textureSize.w, textureSize.h);
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

		glUniform1i(glGetUniformLocation(shader_program_Model, "Texture0"), 0);
		for (std::vector<ModelCollection>::iterator collection = modelCollection.begin(); collection != modelCollection.end(); ++collection) {

			for (std::vector<Model>::iterator mdl = collection->modelList.begin(); mdl != collection->modelList.end(); ++mdl)
			{
				if (eye == ovrEye_Left && mdl->getName() == "mdlRightSurface") continue;
				if (eye == ovrEye_Right && mdl->getName() == "mdlLeftSurface") continue;
				if (!mdl->isVisible) continue;
				
				glBindBuffer(GL_ARRAY_BUFFER, mdl->vbo);
				glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, mdl->ibo);
				glActiveTexture(GL_TEXTURE0);
				glBindTexture(GL_TEXTURE_2D, mdl->texture);

				glEnableVertexAttribArray(posLoc);
				glEnableVertexAttribArray(colorLoc);
				glEnableVertexAttribArray(uvLoc);

				glUniformMatrix4fv(modelMatLoc, 1, GL_FALSE, glm::value_ptr(mdl->modelMatrix));

				glVertexAttribPointer(posLoc, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), 0);
				glVertexAttribPointer(colorLoc, 4, GL_FLOAT, GL_FALSE, sizeof(Vertex), (void*)(3 * sizeof(float)));
				glVertexAttribPointer(uvLoc, 2, GL_FLOAT, GL_FALSE, sizeof(Vertex), (void*)(7 * sizeof(float)));

				if (mdl->getName() == "mdlKinect") {
					
					glPointSize(2.0f);
					glDrawArrays(GL_POINTS, 0, mdl->vertices.size());
					
				}
				else {
					
					glDrawElements(GL_TRIANGLES, mdl->indices.size(), GL_UNSIGNED_INT, NULL);

				}
			}
		}
		glBindFramebuffer(GL_FRAMEBUFFER, fboTexture);
		glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, 0, 0);
		glFramebufferTexture2D(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_TEXTURE_2D, 0, 0);
	}
}

void OpenGL_Handler::mirrorToScreen(HDC hDC) {

	glBindFramebuffer(GL_READ_FRAMEBUFFER, mirrorFBO);
	glBindFramebuffer(GL_DRAW_FRAMEBUFFER, 0);

	GLint w = mirrorTexture->OGL.Header.TextureSize.w;
	GLint h = mirrorTexture->OGL.Header.TextureSize.h;

	glBlitFramebuffer(0, h, w, 0,
		0, 0, w, h,
		GL_COLOR_BUFFER_BIT, GL_NEAREST);
	glBindFramebuffer(GL_READ_FRAMEBUFFER, 0);

	SwapBuffers(hDC);

}

void OpenGL_Handler::shutdown(bool RiftIsPresent, ovrHmd HMD) {
	if (RiftIsPresent) {
		glDeleteFramebuffers(1, &mirrorFBO);
		ovr_DestroyMirrorTexture(HMD, (ovrTexture*)mirrorTexture);
		ovr_DestroySwapTextureSet(HMD, Oculus_TextureSet_Left);
		ovr_DestroySwapTextureSet(HMD, Oculus_TextureSet_Right);
		ovr_DestroySwapTextureSet(HMD, Oculus_TextureSet_HUD);
	}
	glDeleteProgram(shader_program_Model);
	
}

bool OpenGL_Handler::initFreetype(const char* fontPath) {

	FT_Library ftLib;
	FT_Face face;
	if (FT_Init_FreeType(&ftLib)) return false; // "ERROR::FREETYPE: Could not init FreeType Library"
	if (FT_New_Face(ftLib, fontPath, 0, &face)) return false; // "ERROR::FREETYPE: Failed to load font"

	FT_Set_Pixel_Sizes(face, 0, 72);
	glPixelStorei(GL_UNPACK_ALIGNMENT, 1);
	
	// Generate texture
	GLuint texID[128];
	glGenTextures(128, texID);
	for (GLubyte c = 0; c < 128; c++) {

		if (FT_Load_Char(face, c, FT_LOAD_RENDER)) continue; // Failed to load glyph

		glBindTexture(GL_TEXTURE_2D, texID[c]);
		glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, face->glyph->bitmap.width, face->glyph->bitmap.rows, 0, GL_ALPHA, GL_UNSIGNED_BYTE, face->glyph->bitmap.buffer);
		GLint swizzleMask[] = { GL_ALPHA, GL_ALPHA, GL_ALPHA, GL_ALPHA }; 
		glTexParameteriv(GL_TEXTURE_2D, GL_TEXTURE_SWIZZLE_RGBA, swizzleMask); // Setting the RGB values equal to the alpha value, in order to color the text.
		// Set texture options
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
		
		// Storing character for later use
		Character character = {
			texID[c],
			glm::ivec2(face->glyph->bitmap.width, face->glyph->bitmap.rows),
			glm::ivec2(face->glyph->bitmap_left, face->glyph->bitmap_top),
			face->glyph->advance.x
		};

		Characters.insert(std::pair<GLchar, Character>(c, character));
	}
	glBindTexture(GL_TEXTURE_2D, 0);
	FT_Done_Face(face);
	FT_Done_FreeType(ftLib);
	return true;
}