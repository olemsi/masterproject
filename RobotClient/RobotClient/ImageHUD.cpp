#include "ImageHUD.h"
#include <chrono>
#include <thread>
using namespace cv;


static int horOffset = 13;
static int vertOffset = 5;

void incrementHorizontalOffset() {
	horOffset++;
}
void decrementHorizontalOffset() {
	horOffset--;
}
void incrementVerticalOffset() {
	vertOffset++;
}
void decrementVerticalOffset() {
	vertOffset--;
}



struct screenPosition {

	Point TopLeft;
	Point TopRight;
	Point MidLeft;
	Point MidRight;
	Point BottomLeft;
	Point BottomRight;
	Point Center;

	int offset = 0;
	int verticalOffset = 0;

	screenPosition(int Eye, int width, int height) {
		if (Eye == 0) {
			offset = horOffset;
			verticalOffset = vertOffset;
		}
		if (Eye == 1) {
			offset = -horOffset;
			verticalOffset = -vertOffset;
		}


		TopLeft = { (width / 10) * 2 + offset, (height / 10) * 2 + verticalOffset };
		MidLeft = { (width / 10) * 2 + offset, (height / 10) * 5 + verticalOffset };
		BottomLeft = { (width / 10) * 2 + offset, (height / 10) * 8 + verticalOffset };

		TopRight = { (width / 10) * 8 + offset, (height / 10) * 2 + verticalOffset };
		MidRight = { (width / 10) * 8 + offset, (height / 10) * 5 + verticalOffset };
		BottomRight = { (width / 10) * 8 + offset, (height / 10) * 8 + verticalOffset };

		Center = { (width / 2) + offset, (height / 2) + verticalOffset };

	}

};

tm getTime() {

	time_t now = time(0);
	struct tm tstruct;
	localtime_s(&tstruct, &now);

	return tstruct;
}

void drawText(Mat img, Point displayLocation, String sText, bool centerText = true, bool useBorder = false, Scalar borderColor = { 0, 128, 255 }, Scalar textColor = { 0, 128, 255 }) {

	Point drawLoc = displayLocation;

	if (centerText) {
		Size txtSize = cv::getTextSize(sText, CV_FONT_NORMAL, 1, 2, NULL);
		drawLoc = { displayLocation.x - (txtSize.width / 2), displayLocation.y - (txtSize.height / 2) };
	}

	putText(img, sText, drawLoc, CV_FONT_NORMAL, 1, Scalar(0, 128, 255), 2, cv::LineTypes::LINE_AA); // Scalar( B, G, R)

	if (useBorder) {
		int borderSize = 20;
		Size txtSize = cv::getTextSize(sText, CV_FONT_NORMAL, 1, 2, NULL);

		Rect border = Rect((drawLoc.x - borderSize), (drawLoc.y - txtSize.height - borderSize), (txtSize.width + borderSize * 2), (txtSize.height + borderSize * 2));

		rectangle(img, border, borderColor, 2, LINE_AA);
	}

}

void drawAngleIndicator(Mat img, Point displayLocation, float angle, float radius = 100.0f, bool invert = false) {

	int invFlag = 1;
	if (invert) invFlag = -1;

	circle(img, displayLocation, 100, Scalar(0, 255, 64), 1, LINE_AA);

	arrowedLine(img, displayLocation, { displayLocation.x + invFlag * (int)(radius * sin(angle)), displayLocation.y + (int)(-radius * cos(angle)) }, Scalar(255, 0, 0), 1, LINE_AA);

}

void drawCross(Mat img, Point displayLocation, int size) {


	line(img, { displayLocation.x - size, displayLocation.y - size }, { displayLocation.x + size, displayLocation.y + size }, Scalar(0, 128, 255), 2, LINE_AA);
	line(img, { displayLocation.x - size, displayLocation.y + size }, { displayLocation.x + size, displayLocation.y - size }, Scalar(0, 128, 255), 2, LINE_AA);
}

void drawFFT(Mat img, Point displayLocation) {

	int barWidth = 20;
	int numBars = 10;
	int offset = barWidth * (numBars / 2);
	int random;
	srand(time(NULL));
	for (int i = 0; i < numBars; i++) {

		random = rand() % 50 + 1;

		rectangle(img, { displayLocation.x - offset + (barWidth * i), displayLocation.y - random }, { displayLocation.x - offset - barWidth + (barWidth * i), displayLocation.y }, Scalar(0, 128, 255), CV_FILLED);
		drawText(img, { displayLocation.x, displayLocation.y + 40 }, "20Hz - 20kHz");
	}



}

void paintMain(Mat &img, int eye, int fpsLeft, int fpsRight, float angle, bool menuActiveL, bool menuActiveR) {

	Mat localImg = img.clone();
	struct tm tstruct = getTime();
	screenPosition *HUDPos = new screenPosition(eye, localImg.cols, localImg.rows);

	String minutes = (tstruct.tm_min < 10) ? ("0" + to_string(tstruct.tm_min)) : to_string(tstruct.tm_min);

	String sClock = to_string(tstruct.tm_hour) + ":" + minutes;
	drawText(localImg, HUDPos->TopLeft, sClock);

	String sFPS = "FPS: " + to_string(fpsLeft) + "/" + to_string(fpsRight);
	drawText(localImg, HUDPos->TopRight, sFPS);

	String sBattery = "Battery: 100%";
	drawText(localImg, HUDPos->MidLeft, sBattery);

	String sSignal = "Signal: 70%";
	drawText(localImg, HUDPos->MidRight, sSignal);

	String sMenuLeft = "< Environmental";
	if (menuActiveL) { drawText(localImg, HUDPos->BottomLeft, sMenuLeft, true, true, Scalar(0, 255, 0)); }
	else { drawText(localImg, HUDPos->BottomLeft, sMenuLeft, true, true); }


	String sMenuRight = "Robot Ctrl >";
	if (menuActiveR) { drawText(localImg, HUDPos->BottomRight, sMenuRight, true, true, Scalar(0, 255, 0)); }
	else { drawText(localImg, HUDPos->BottomRight, sMenuRight, true, true); }


	// Centered Cross
	drawCross(localImg, HUDPos->Center, 10);

	// Angle Indicator
	drawAngleIndicator(localImg, { HUDPos->Center.x, HUDPos->BottomLeft.y }, angle);

	img = localImg;

}

void paintEnvironmental(Mat &img, int eye, float angle, bool menuActiveL, bool menuActiveR) {

	Mat localImg = img.clone();
	screenPosition *HUDPos = new screenPosition(eye, img.cols, img.rows);

	String sTemp = "Temp: 20.0 C";
	drawText(localImg, HUDPos->TopLeft, sTemp);

	String sHumid = "Humidity: 50%";
	drawText(localImg, HUDPos->TopRight, sHumid);

	// FFT
	drawFFT(localImg, HUDPos->MidRight);

	String sBar = "1.25 bar";
	drawText(localImg, HUDPos->MidLeft, sBar);


	String sMenuRight = "Main >";
	if (menuActiveR) { drawText(localImg, HUDPos->BottomRight, sMenuRight, true, true, Scalar(0, 255, 0)); }
	else { drawText(localImg, HUDPos->BottomRight, sMenuRight, true, true); }


	drawCross(localImg, HUDPos->Center, 10);
	drawAngleIndicator(localImg, { HUDPos->Center.x, HUDPos->BottomLeft.y }, angle);

	img = localImg;
}


void paintRobotic(Mat &img, int eye, float angle, bool menuActiveL, bool menuActiveR) {

	Mat localImg = img.clone();

	screenPosition *HUDPos = new screenPosition(eye, img.cols, img.rows);

	String x_dir = "X: 358.85mm";
	drawText(localImg, HUDPos->TopLeft, x_dir);
	String y_dir = "Y: 26.30mm";
	drawText(localImg, { HUDPos->Center.x, HUDPos->TopLeft.y }, y_dir);
	String z_dir = "Z: 504.13mm";
	drawText(localImg, HUDPos->TopRight, z_dir);
	String p_rot = "Pitch: 35.03 deg";
	drawText(localImg, HUDPos->MidLeft, p_rot);
	String r_rot = "Roll: 7.38 deg";
	drawText(localImg, HUDPos->MidRight, r_rot);

	String sMenuLeft = "< Main";
	if (menuActiveL) { drawText(localImg, HUDPos->BottomLeft, sMenuLeft, true, true, Scalar(0, 255, 0)); }
	else { drawText(localImg, HUDPos->BottomLeft, sMenuLeft, true, true); }

	drawCross(localImg, HUDPos->Center, 10);
	drawAngleIndicator(localImg, { HUDPos->Center.x, HUDPos->BottomLeft.y }, angle);

	img = localImg;

}
