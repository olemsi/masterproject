#include "OculusGL_Interface.h"

// Searches the global model collection for a particular model list, returning a pointer to its location in memory.
std::vector<Model>* OpenGL_Handler::findModelList(std::vector<ModelCollection> &mdlCollection, std::string name) {

	for (std::vector<ModelCollection>::iterator list = mdlCollection.begin(); list != mdlCollection.end(); ++list)
	{
		if (list->getName() == name) {
			return &list->modelList;
		}
	}
	return NULL;
}

// Searches a model list for a particular model, returning a pointer to its location in memory.
Model* OpenGL_Handler::findModel(std::vector<Model> &mdlList, std::string name) {

	for (std::vector<Model>::iterator mdl = mdlList.begin(); mdl != mdlList.end(); ++mdl)
	{
		if (mdl->getName() == name) {
			return &(*mdl);
		}
	}
	return NULL;

}

// Deletes a particular model list from the global model collection, freeing up memory in order to avoid leaks.
bool OpenGL_Handler::removeModelCollection(std::string listName) {

	for (std::vector<ModelCollection>::iterator mdlColl = modelCollection.begin(); mdlColl != modelCollection.end(); mdlColl++) {

		if (mdlColl->getName() == listName) {
			for (std::vector<Model>::iterator mdl = mdlColl->modelList.begin(); mdl != mdlColl->modelList.end(); mdl++) {
				if (mdl->texture != 0) mdl->freeModel(true);
				else (mdl->freeModel());
			}
			modelCollection.erase(mdlColl);
			return true;
		}
	}

	return false;
}

// Deletes a model from a model list, freeing up memory in order to avoid leaks.
bool OpenGL_Handler::removeModel(std::string listName, std::string mdlName) {
	std::vector<Model> *mdlList = findModelList(modelCollection, listName);
	if (!(mdlList == NULL)) {

		for (std::vector<Model>::iterator mdl = mdlList->begin(); mdl != mdlList->end(); mdl++) {
			if (mdl->getName() == mdlName) {
				if (mdl->texture != 0) mdl->freeModel(true);
				else (mdl->freeModel());
				mdlList->erase(mdl);
			}

		}
	}

	return false;
}

// First rendering of most 3D models which are to be rendered at a static location in the virtual world. Adding static representations, such as background circles for the clock and speedometer.
std::vector<ModelCollection> OpenGL_Handler::mainHud(bool RiftIsPresent, cv::Mat &CamPicture_Left, cv::Mat &CamPicture_Right) {

	std::vector<ModelCollection> mdlCollection;
	glm::mat4 modelMatrix;
		
	// Adding 3D models
	std::vector<Model> mdlList;
	cv::Mat texRobot = cv::imread("robotTexture.jpg");
	cv::Mat texClock = cv::imread("clockTexture.jpg");

	modelMatrix = glm::translate(glm::mat4(), glm::vec3(15.0f, -22.0f, 20.0f)) * glm::rotate(glm::mat4(), glm::radians(90.0f), glm::vec3(0.0f, 0.0f, 1.0f)) *glm::rotate(glm::mat4(), glm::radians(90.0f), glm::vec3(0.0f, 1.0f, 0.0f));
	addBatteryModel(&mdlList, "mdlBattery", 0.2f, modelMatrix, texRobot);
	
	modelMatrix = glm::translate(glm::mat4(), glm::vec3(0.0f, 0.0f, 54.5f)) * glm::rotate(glm::mat4(), glm::radians(90.0f), glm::vec3(0.0f, -1.0f, 0.0f)) * glm::scale(glm::mat4(), glm::vec3(0.001f, 0.001f, 0.001f));
	addRobotModel(&mdlList, "mdlRobot", 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, modelMatrix, texRobot);

	modelMatrix = glm::translate(glm::mat4(), glm::vec3(0.0f, 20.0f, 25.0f)) * glm::rotate(glm::mat4(), glm::radians(90.0f), glm::vec3(-1.0f, 0.0f, 0.0f));
	addArrowModel(&mdlList, "mdlCompassArrow", 0.0f, modelMatrix, texRobot);
	
	{ // Adding a circular plane for the compass
		std::vector<Vertex> vb_CompassPlane;
		std::vector<GLuint> ib_CompassPlane;
		modelMatrix = glm::translate(glm::mat4(), glm::vec3(0.0, 21.0f, 25.0f)) * glm::rotate(glm::mat4(), glm::radians(90.0f), glm::vec3(-1.0f, 0.0f, 0.0f));
		
		ShapeAttribs shape;
		shape.radius = 5.0f;
		shape.depth = 0.1f;
		shape.color = glm::vec3{ 0.1f, 0.1f, 1.0f };

		addShape_Circle(vb_CompassPlane, ib_CompassPlane, shape);

		mdlList.push_back(Model(vb_CompassPlane, ib_CompassPlane, modelMatrix, cv::Mat(), "mdlCompassPlane"));
	}

	modelMatrix = glm::translate(glm::mat4(), glm::vec3(-15.0f, -22.0f, 20.0f)) * glm::rotate(glm::mat4(), glm::radians(90.0f), glm::vec3(-1.0f, 0.0f, 0.0f)) *glm::scale(glm::mat4(), glm::vec3(0.5f, 0.5f, 0.5f));
	
	time_t rawTime = time(0);
	tm timeInfo;
	localtime_s(&timeInfo, &rawTime);
	addClockModel(&mdlList, "mdlClockPointers", timeInfo, modelMatrix);

	{ // Adding a circular plane for the clock
		std::vector<Vertex> vb_Clock;
		std::vector<GLuint> ib_Clock;
		ShapeAttribs shape;
		shape.radius = 11.0f;
		shape.depth = 0.1f;

		addShape_Circle(vb_Clock, ib_Clock, shape);

		mdlList.push_back(Model(vb_Clock, ib_Clock, modelMatrix, texClock, "mdlClock"));
	}


	{ // Adding a circular plane for the speedometer
		std::vector<Vertex> vb_SpeedoPlane;
		std::vector<GLuint> ib_SpeedoPlane;
		modelMatrix = glm::translate(glm::mat4(), glm::vec3(0.0, -80.0f, 10.0f)) * glm::rotate(glm::mat4(), glm::radians(90.0f), glm::vec3(-1.0f, 0.0f, 0.0f));

		ShapeAttribs shape;
		shape.radius = 11.0f;
		shape.depth = 100.1f;
		shape.color = glm::vec3(0.2f, 0.2f, 0.2f);
		addShape_Cylinder(vb_SpeedoPlane, ib_SpeedoPlane, shape);

		mdlList.push_back(Model(vb_SpeedoPlane, ib_SpeedoPlane, modelMatrix, texSpeedo, "mdlSpeedometerBkg"));
	}
	modelMatrix = glm::translate(glm::mat4(), glm::vec3(0.0, -30.0f, 10.0f)) * glm::rotate(glm::mat4(), glm::radians(90.0f), glm::vec3(-1.0f, 0.0f, 0.0f));
	mdlCollection.push_back(ModelCollection(mdlList, "3DModels"));

	addSpeedoMeter(mdlCollection, "mdlSpeedArrow", 50.0f, 0.0f, 0.0f, modelMatrix); // Must be located after 3DModels list have been pushed onto collection

	mdlCollection.push_back(ModelCollection(addSurfaceModels(RiftIsPresent, CamPicture_Left, CamPicture_Right), "Surfaces"));

	return mdlCollection;
}